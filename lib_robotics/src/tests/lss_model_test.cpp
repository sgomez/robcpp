
#include "robotics/lss_model.hpp"
#include <armadillo>
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE LSS_Test
#include <boost/test/unit_test.hpp>
#include <json.hpp>
#include <fstream>
#include <string>
#include <unordered_map>
#include <cmath>
#include <memory>
#include "robotics/utils.hpp"

#define EPS 1e-8

using namespace robotics;
using namespace arma;
using namespace std;
using json = nlohmann::json;

ConstLSSModel uniformVel1D(double deltaT, double noise_std, const mat& Gamma) {
  mat mA(2,2), mC(1,2);
  mA << 1.0 << deltaT << endr << 0.0 << 1.0 << endr;
  mC << 1.0 << 0.0 << endr;
  mat bias_state(2,1,fill::zeros);
  mat bias_obs(1,1,fill::zeros);
  mat Sigma(1,1);
  Sigma(0,0) = noise_std*noise_std;
  ConstLSSModel dyn(mA, bias_state, mC, bias_obs, Sigma, Gamma);
  return dyn;
}

BOOST_AUTO_TEST_CASE( SimpleKFTest1 ) {
  mat Gamma = eye<mat>(2,2)*1e-8;
  ConstLSSModel dyn = uniformVel1D(1.0, 0.1, Gamma);
  vec mu0 {0,0};
  vec u{0};
  mat P0 = eye<mat>(2,2)*1e6;
  LSSGaussState st0(mu0, P0);

  BOOST_CHECK( norm(st0.mean() - mu0) < EPS );
  BOOST_CHECK( norm(st0.cov() - P0) < EPS );

  //I am originally at in 0, but observe a noisy version of the position
  auto st1 = st0.observe(dyn, vec{-0.05}, u);
  mat P1 = st1.cov();
  //So my uncertainty over the position should decrease a lot
  BOOST_CHECK( (P1(0,0) < 0.05) && (P1(0,0)>0) );
  //But my uncertainty over the velocity should still be high
  BOOST_CHECK( P1(1,1) > 0.1*P0(1,1) );

  //Now I try to predict what will happen 1 second later
  auto st2 = st1.next( dyn, u );
  mat P2 = st2.cov();
  //Now the uncertanty on the position should be big again, since the velocity can be about anything
  BOOST_CHECK( P2(0,0) > P1(0,0) );
  //But my uncertainty over the velocity should not really change much
  BOOST_CHECK( fabs(P2(1,1) - P1(1,1)) < 0.01 );

  //Now I am at 1, because my velocity was 1. I observe a noise version of the position
  vector<double> next_positions{1.09, 2.01, 2.96, 4.01, 4.99, 5.97, 7.01, 8.0, 9.01, 10.0};
  for (const double& obs : next_positions) {
    //The current observation should be close to the estimated location
    BOOST_CHECK( st2.obs_mah_dist(dyn, vec{obs}, u) < 4.0 );
    auto st3 = st2.observe(dyn, vec{obs}, u);
    mat P3 = st3.cov();
    vec mu3 = st3.mean();
    //cout << mu3 << endl << P3 << endl;
    //Now the uncertainty on both the position and velocity should be low
    BOOST_CHECK( (P3(0,0) < 0.05) && (P3(0,0)>0) );
    BOOST_CHECK( P3(1,1) < 1.0 );
    //And my position estimate should be close to the real one x=1, v=1
    BOOST_CHECK( fabs(mu3[0]-round(obs)) < 5*sqrt(P3(0,0)) );
    BOOST_CHECK( fabs(mu3[1] - 1.0) < 5*sqrt(P3(1,1)) );
    st2 = st3.next(dyn, u);
  }
}
