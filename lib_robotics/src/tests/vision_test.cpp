
#include "robotics/utils.hpp"
#include <armadillo>
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Utils_Test
#include <boost/test/unit_test.hpp>
#include <json.hpp>
#include <fstream>
#include <string>
#include <unordered_map>
#include <cmath>
#include <memory>

#define EPS 1e-8

using namespace robotics;
using namespace arma;
using namespace std;
using json = nlohmann::json;

vector<pt3d> some_rand_3d_pts() {
  vector<pt3d> obs3d {
    {0.0,-2.12,-0.99},{0.1,-2,-0.8},{-0.76,-3,-0.5},{0.76,-0.76,-0.9},{-0.3,-2.1,0},
    {0,-1,-0.3},{0.32,-2,-0.5},{0.71,-1.32,-0.82}
  };
  return obs3d;
}

BOOST_AUTO_TEST_CASE( CalibrationTest ) {
  mat C;
  C << 289.3719 << 7.6836 << 3.6974 << 275.2658 << endr <<
    1.4087 << -103.4491 << -256.4231 << -160.0444 << endr <<
    0.0575 << 0.2362 << -0.1019 << 1.0000 << endr;
  vector<pt2d> obs2d;
  vector<pt3d> obs3d = some_rand_3d_pts();
  for (unsigned int i=0; i < obs3d.size(); i++) {
    obs2d.push_back(stereo_project(obs3d[i], C));
  }

  mat calib = calibrate_camera(obs2d, obs3d);
  BOOST_CHECK( norm(calib - C) < 1e-6 );
}

BOOST_AUTO_TEST_CASE( StereoTest ) {
  mat C1, C2;
  C1 << 289.3719 << 7.6836 << 3.6974 << 275.2658 << endr <<
    1.4087 << -103.4491 << -256.4231 << -160.0444 << endr <<
    0.0575 << 0.2362 << -0.1019 << 1.0000 << endr;
  C2 << 2.3593137e+02 << 1.6816250e+02 << -1.2069500e+02 << 3.5435570e+02 << endr
    << -2.6973308e+01 << -1.2039595e+02 << -2.8477892e+02 << -1.2252819e+02 << endr
    << -9.2945166e-02 << 1.8486168e-01 << -1.4139138e-01 << 1.0000000e+00 << endr;
  unordered_map<unsigned int, mat> calib{ {1,C1}, {2,C2} };
  vector<pt2d> obs2d;
  vector<pt3d> obs3d = some_rand_3d_pts();
  for (unsigned int i=0; i < obs3d.size(); i++) {
    vec tmp{obs3d[i][0], obs3d[i][1], obs3d[i][2], 1.0};
    vector<pair<unsigned int, pt2d>> cam_obs;
    for (int cam_id=1; cam_id<=2; cam_id++) {
      vec proj = calib[cam_id]*tmp;
      pt2d pix;
      pix[0] = proj[0]/proj[2];
      pix[1] = proj[1]/proj[2];
      cam_obs.push_back( make_pair(cam_id,pix) );
    }
    pt3d estimation = stereo_vision(calib, cam_obs);
    for (int j=0; j<3; j++) {
      BOOST_CHECK(fabs(estimation[j] - obs3d[i][j]) < 1e-2);
    }
  }
}

BOOST_AUTO_TEST_CASE( StereoMaxInlierSetTest ) {
  mat C1, C2, C3, C4;
  C1 << -1567 << -33 << 47 << 1343 << endr <<
    -93 << 554 << -1399 << 1156 << endr <<
    -0.29 << -0.991 << -0.6338 << 1.0000 << endr;
  C2 << -1161 << -638 << -424 << -800 << endr
    << -79.6 << 489.4 << -1261 << 1032 << endr
    << 0.39 << -0.892 << -0.551 << 1.0 << endr;
  C3 << 328 << -14 << 12 << 282 << endr
    << 18.5 << -100 << -310 << -120 << endr
    << 0.06 << 0.18 << -0.094 << 1.0 << endr;
  C4 << 260 << 144 << -75 << 375 << endr
    << -23.6 << -90 << -300 << -83 << endr
    << -0.095 << 0.176 << -0.088 << 1.0 << endr;
  unordered_map<unsigned int, mat> calib{ {0,C1}, {1,C2}, {2,C3}, {3,C4} };
  for (unsigned int mask=0; mask<16; mask++) {
    vector<pt3d> obs3d = some_rand_3d_pts();
    for (unsigned int i=0; i < obs3d.size(); i++) {
      vector<pair<unsigned int, pt2d>> cam_obs;
      int num_inliers = 0;
      for (int cam_id=0; cam_id<4; cam_id++) {
        pt2d proj = stereo_project(obs3d[i], calib.at(cam_id));
        if (mask & (1<<cam_id)) {
          num_inliers++;
        } else {
          proj[0] += 11 + cam_id*13; proj[1] -= 17 + cam_id*19;
        }
        cam_obs.push_back( make_pair(cam_id,proj) );
      }
      auto inlier_set = stereo_max_inlier_set(calib, cam_obs, 5.0);
      if (num_inliers >= 2) BOOST_CHECK_EQUAL(inlier_set.size(), num_inliers);
      //else BOOST_CHECK(inlier_set.size() < 2);
      if (num_inliers >= 2) {
        pt3d estimation = stereo_vision(calib, inlier_set);
        for (int j=0; j<3; j++) {
          BOOST_CHECK(fabs(estimation[j] - obs3d[i][j]) < 1e-2);
        }
      }
    }
  }
}


