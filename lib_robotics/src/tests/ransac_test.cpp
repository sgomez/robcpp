
#include <robotics/utils/ransac.hpp>
#include <robotics/utils/random.hpp>
#include <armadillo>
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TestRansac
#include <boost/test/unit_test.hpp>
#include <random>

using namespace std;
using namespace arma;
using namespace robotics;

/**
 * This test case uses a PRGN with fixed seeds to produce reproducible results. The seeds
 * where arbitrarily chosen by the tester.
 */
BOOST_AUTO_TEST_CASE(prng_ransac) {
  cout << "hello" << endl;
  arma::vec w {1.2, -2.1, 0.5}; //arbitrary vector of parameters of a 2nd order polynomial
  double prob_inlier = 0.8;
  unsigned int n_points = 100;
  double noise = 0.1;
  vec t = arma::linspace<vec>(-5, 5, n_points);
  cout << "world" << endl;
  for (unsigned int seed = 0; seed < 100; seed += 10) {
    mt19937 gen(seed);
    vector<bool> inliers = random::sample_bernoulli(gen, prob_inlier, n_points);
    mat x(n_points, 3);
    for (unsigned int i=0; i<n_points; i++) {
      double tmp=1.0;
      for (int j=0; j<3; j++) {
        x(i,j) = tmp;
        tmp *= t[i];
      }
    }
    mat y = x*w + random::sample_normal(gen, 0, noise, n_points);
    normal_distribution<> outnoise(0, 100);
    for (unsigned int i=0; i<n_points; i++) {
      if (!inliers[i]) {
        y(i,0) += outnoise(gen);
      }
    }
    cout << "Generating finished with seed " << seed << endl;

    //Now actually test ransac
    RansacConf conf{5*noise,10,100};
    RansacLinAns r_ans = linear_ransac(x, y, conf);
    cout << "w = " << r_ans.x << endl;

    for (auto i : r_ans.inliers) {
      BOOST_CHECK(r_ans.is_inlier[i]);
    }
    BOOST_CHECK( arma::norm(w - r_ans.x) < 0.1 );
    for (unsigned int i=0; i<n_points; i++) {
      if (inliers[i]) BOOST_CHECK(r_ans.is_inlier[i]);
    }
    cout << "Ransac test finised with seed " << seed << endl;
  }
}
