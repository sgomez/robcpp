
#include "robotics.hpp"
#include <armadillo>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <json.hpp>
#include <fstream>
#include <string>
#include <unordered_map>
#include <cmath>
#include <memory>

#define EPS 1e-8

using namespace robotics;
using namespace arma;
using namespace std;
using json = nlohmann::json;

BOOST_AUTO_TEST_CASE( TestUncPoly ) {
  FullProMP unconstrained = create_promp_poly(7,3);
  vec zeros(7, fill::zeros);
  vec ones(7, fill::ones);
  FullProMP constrained = unconstrained.condition_current_state(0,1,zeros,
      zeros).condition_current_state(1,1,ones,zeros);
  auto first = constrained.mean_traj_step(0,1);
  auto last = constrained.mean_traj_step(1,1);
  BOOST_CHECK( norm(first.q - zeros) < 1e-6 );
  BOOST_CHECK( norm(first.qd - zeros) < 1e-6 );
  BOOST_CHECK( norm(last.q - ones) < 1e-6 );
  BOOST_CHECK( norm(last.qd - zeros) < 1e-6 );
}

BOOST_AUTO_TEST_CASE( TestGoToLimits ) {
  vec zeros(7, fill::zeros);
  vec ones(7, fill::ones);
  unsigned int steps = 100;
  double max_qd = 10;
  double max_qdd = 100;
  double qd_eps = 0.5, qdd_eps=5;
  double T = find_goto_duration(zeros, zeros, ones, zeros, max_qd, max_qdd);
  cout << T << endl;
  FullProMP p = go_to_promp(ones, zeros, T);
  p = p.condition_current_state(0, T, zeros, zeros);
  bool once_close = false;
  auto q0 = p.mean_traj_step(0,T), qf = p.mean_traj_step(T,T);
  BOOST_CHECK( norm(q0.q - zeros) < 1e-6 && norm(q0.qd - zeros) < 1e-6);
  BOOST_CHECK( norm(qf.q - ones) < 1e-6 && norm(qf.qd - zeros) < 1e-6 );
  for (unsigned int i=0; i<steps; i++) {
    double t = i*T/steps;
    auto step = p.mean_traj_step(t, T);
    cout << step.q[0] << " " << step.qd[0] << " " << step.qdd[0] << endl;
    for (auto qd : step.qd) {
      BOOST_CHECK((fabs(qd)-qd_eps) < max_qd);
      if (fabs(fabs(qd)-max_qd) < 1e-3) once_close = true;
    }
    for (auto qdd : step.qdd) {
      BOOST_CHECK((fabs(qdd)-qdd_eps) < max_qdd);
      if (fabs(fabs(qdd)-max_qdd) < 1e-3) once_close = true;
    }
  }
  BOOST_CHECK(once_close);
}

BOOST_AUTO_TEST_CASE( TestFreezeProMP ) {
  FullProMP freeze = freeze_promp(7);
  vec q{0.1,0.2,0.3,0.4,0.5,0.6,0.7};
  auto constrained = freeze.condition_current_position(0,1,q);
  for (double t=0; t<=1.0; t+=0.1) {
    auto tmp = constrained.mean_traj_step(t,1.0);
    BOOST_CHECK( norm(tmp.q - q) < 1e-6 );
    BOOST_CHECK( norm(tmp.qd) < 1e-6 );
    BOOST_CHECK( norm(tmp.qdd) < 1e-6 );
  }
}
