
#include <robotics/utils.hpp>
#include <robotics/kinematics.hpp>
#include <json.hpp>
#include <armadillo>
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TestRansac
#include <boost/test/unit_test.hpp>
#include <random>

using namespace std;
using namespace arma;
using namespace robotics;
using json = nlohmann::json;

/**
 * This test code only tests that the implementation of C++ returns the same
 * answers as the Python implementation. It doesn't provide full test that
 * everything else is fine
 */
BOOST_AUTO_TEST_CASE( fwd_kin_equiv_python ) {
  shared_ptr<FwdKin> pfwd_kin = load_barrett_wam_kin("examples/barrett_kin.json");
  FwdKin& fwd_kin = *pfwd_kin;
  ifstream in_file("test/barrett_fwd_kin.json");
  json jtest;
  in_file >> jtest;
  for (auto test_case : jtest) {
    vec q = json2vec(test_case["q"]);
    vec pos = json2vec(test_case["x"]);
    mat jac = json2mat(test_case["jac"]);
    vec ori = json2vec(test_case["ori"]);
    FwdKin::Answer ans = fwd_kin(q);
    BOOST_CHECK_MESSAGE(arma::norm(ans.position - pos)  < 1e-6,
        "error in returned position: for q=" << q << " I get " << 
        ans.position << " and Python said " << pos);
    BOOST_CHECK_MESSAGE(arma::norm(ans.jacobian - jac) < 1e-6,
        "error in returned Jacobian: for q=" << q << " I get " << 
        ans.jacobian << " and Python said " << jac);
    BOOST_CHECK_MESSAGE(arma::norm(ans.orientation - ori) < 1e-3,
        "error in returned orientation: for q=" << q << " I get " << 
        ans.orientation << " and Python said " << ori << ". Diff=" << ans.orientation-ori);
  }
}

/**
 * This test code only tests that the implementation of C++ returns the same
 * answers as the Python implementation. It doesn't provide full test that
 * everything else is fine
 */
BOOST_AUTO_TEST_CASE( inv_kin_equiv_python ) {
  shared_ptr<FwdKin> pfwd_kin = load_barrett_wam_kin("examples/barrett_kin.json");
  FwdKin& fwd_kin = *pfwd_kin;
  ifstream in_file("test/inv_kin_test.json");
  json jtest;
  in_file >> jtest;
  for (auto test_case : jtest) {
    vec mu_q_prior = json2vec(test_case["mu_th_prior"]);
    mat Sig_q_prior = json2mat(test_case["Sigma_th_prior"]);
    vec mu_x = json2vec(test_case["mu_x"]);
    mat Sig_x = json2mat(test_case["Sigma_x"]);
    vec mu_q_post = json2vec(test_case["mu_th_post"]);
    mat Sig_q_post = json2mat(test_case["Sigma_th_post"]);

    auto ans = prob_inv_kin_pos(fwd_kin, {mu_q_prior, Sig_q_prior}, {mu_x, Sig_x});
    BOOST_CHECK_MESSAGE(arma::norm(ans.mean() - mu_q_post)  < 1e-4,
        "error in returned mean: I get " << ans.mean() << " and Python said " << mu_q_post);
    BOOST_CHECK_MESSAGE(arma::norm(ans.cov() - Sig_q_post) < 1e-2,
        "error in returned Covariance: I get " << ans.cov() << " and Python said " << Sig_q_post);
  }
}

