
#include <boost/test/included/unit_test.hpp>
#include <boost/test/parameterized_test.hpp>
#include <robotics/controllers/mov_prim.hpp>
#include <robotics/controllers/inv_dyn.hpp>
#include <robotics/utils/promp_utils.hpp>
#include <armadillo>
#include <vector>
#include <cmath>

using namespace boost::unit_test;
using namespace robotics;
using namespace std;
using namespace arma;

void test_promp_inv_dyn_control(const vector<InvDynJointObs>& desired_traj) {
  auto controller = MovPrimController::create("inv_dyn");
  double tot_time = 0.0;
  //create the desired trajectory in the controller
  for (unsigned int i=1; i<desired_traj.size(); i++) {
    double t0 = desired_traj[i-1].time;
    double T = desired_traj[i].time - t0;
    tot_time += T;
    auto promp = make_shared<FullProMP>( go_to_promp(desired_traj[i].q, desired_traj[i].qd, T) ); 
    controller->addProMP(promp, t0, T);
  }

  double deltaT = 0.002;
  InvDynSimulation::Controller control_method = [controller](void* a, const void* b, double c) -> void {
    controller->control(a,b,c);
  };
  InvDynSimulation sim(control_method, desired_traj[0]);
  sim.set_sim_noise(1e-5, 1e-5);
  vector<InvDynJointObs> v = sim(deltaT, ceil(tot_time/deltaT) + 1);
  
  unsigned int i, j;
  for (i=0, j=0; i<desired_traj.size() && j<v.size(); j++) {
    if ( fabs(desired_traj[i].time - v[j].time) < 1e-3 ) {
      BOOST_CHECK( arma::norm(desired_traj[i].q  - v[j].q) < 0.1 );
      BOOST_CHECK( arma::norm(desired_traj[i].qd  - v[j].qd) < 0.1 );
      i++;
    }
  }
  BOOST_CHECK(i == desired_traj.size());
}

test_suite* init_unit_test_suite( int argc, char* argv[] ) {
  vector< vector<InvDynJointObs> > tests {
    { {vec{0.0}, vec{0.0}, 0.0}, {vec{1.0}, vec{0.0}, 1.0}, {vec{0.5}, vec{0.0}, 2.0} },
    { {vec{0.0,0.0,0.0}, vec{0.0,0.0,0.0}, 0.0}, {vec{1.0, 1.4, -1.5}, vec{0.0,0.0,0.1}, 1.0} },
    { {vec{1.0,-1.0},vec{0.1,-0.1}, 1.0}, {vec{-1.1, 0.8},vec{0.0,0.0},2.0}, {vec{0,0},vec{0,0},3.0} }
  };

  framework::master_test_suite().add( 
      BOOST_PARAM_TEST_CASE(test_promp_inv_dyn_control, tests.begin(), tests.end()) 
      );

  return 0;
}
