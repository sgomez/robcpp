
#include <robotics/controllers/mov_prim.hpp>
#include <robotics/controllers/inv_dyn.hpp>
#include <thread>
#include <mutex>
#include <unordered_map>
#include <queue>

using namespace std;

namespace robotics {

  namespace {

    /**
     * Internal enum representing what should be done at the start of a movement primitive
     */
    enum MovPrimStart {
      COND_Q0 = 1<<0,
      COND_QD0 = 1<<1
    };

    /**
     * Internal representation of the movement primitive requires the starting time, the duration
     * and the controller function.
     */
    struct MovPrim {
      double t0; //<! Staring time of the movement primitive
      double T; //<! Duration of the movement primitive
      MovPrimController::Controller mov; //<! Function that returns the control signal for the movement

      MovPrim(double t0, double T, MovPrimController::Controller mov) : t0(t0), T(T), mov(mov) {
      }

      MovPrim() {
      }
    };

    /**
     * A Datatype for a minheap based on priority queues
     */
    template <class T>
    using minheap = priority_queue<T, vector<T>, greater<T>>;


    class ProMPInvDynController {
      private:
        std::shared_ptr<FullProMP> original_promp;
        std::shared_ptr<FullProMP> cond_promp;
        double t0, T;
        MovPrimStart start_flags;
      public:
        ProMPInvDynController(std::shared_ptr<FullProMP> promp, double t0, double T, 
            MovPrimStart start_flags) : t0(t0), T(T), start_flags(start_flags) {
          original_promp = promp;
        }

        ProMPInvDynController(std::shared_ptr<FullProMP> promp, double t0, double T) : t0(t0), T(T) {
          start_flags = static_cast<MovPrimStart>(COND_Q0 | COND_QD0);
          original_promp = promp;
        }

        ~ProMPInvDynController() = default;

        void operator()(void* control_signal, const void* curr_state, double time) {
          if (original_promp) {
            const InvDynJointObs& last_obs = *(static_cast<const InvDynJointObs*>(curr_state));
            if ( (start_flags & (COND_Q0 | COND_QD0)) == (COND_Q0 | COND_QD0) ) {
              cond_promp = make_shared<FullProMP>(original_promp->condition_current_state(last_obs.time - t0, T, 
                  last_obs.q, last_obs.qd));
            } else if ( (start_flags & (COND_Q0)) == COND_Q0 ) {
              cond_promp = make_shared<FullProMP>(original_promp->condition_current_position(
                    last_obs.time-t0, T, last_obs.q));
            } else {
              //this is dangerous and should not happen
              cond_promp = original_promp;
            }
            original_promp.reset();
          }
          InvDynSignal& inv_dyn_control = *(static_cast<InvDynSignal*>(control_signal));
          TrajectoryStep curr_step = cond_promp->mean_traj_step(time - t0, T);
          swap(inv_dyn_control.q, curr_step.q);
          swap(inv_dyn_control.qd, curr_step.qd);
          swap(inv_dyn_control.qdd, curr_step.qdd);
        }
    };

  };


  /**
   * Factory method for a function that return inverse dynamics control signals for a Probabilistic
   * movement primitive.
   */
  MovPrimController::Controller promp_invdyn_control(std::shared_ptr<FullProMP> promp, double t0, double T) {
    ProMPInvDynController ans(promp, t0, T);
    return ans;
  }

  /**
   * This method can be used as default behaviour for an inverse dynamics based controller.
   */
  void invdyn_gravety_comp(void* control_signal, const void* curr_state, double time) {
    (void)time;
    const InvDynJointObs& last_obs = *(static_cast<const InvDynJointObs*>(curr_state));
    InvDynSignal& inv_dyn_control = *(static_cast<InvDynSignal*>(control_signal));
    inv_dyn_control.q = last_obs.q;
    inv_dyn_control.qd = inv_dyn_control.qdd = arma::zeros<arma::vec>(last_obs.q.n_elem);
  }

  /**
   * Private methods and data for the movement primitive controller class. This methods are not
   * thread safe (as are private only), the public methods should use the mutex variable in this
   * class to ensure thread safety.
   */
  class MovPrimController::Impl {

    public:
      ProMPControllerFactory promp_control_factory; //<! Creates a controller method for ProMPs
      Controller default_controller; //<! Default controller to be called if there is no ProMP in execution
      mutex object_mutex; //<! Mutex that all public methods should use to make the object thread safe
      unordered_map<unsigned int,MovPrim> movements;
      minheap<pair<double,unsigned int>> mov_order;
      unsigned int id_counter;

      void clear() {
        movements.clear();
        mov_order = minheap<pair<double,unsigned int>>();
        id_counter = 0;
      }

      Impl() {
        id_counter = 0;
      }

      unsigned int addMov(const MovPrim& mov) {
        movements[id_counter] = mov;
        mov_order.push(make_pair(mov.t0,id_counter));
        return id_counter++;
      }

      void delMov(unsigned int key) {
        movements.erase( key );
      }

      unsigned int editMov(unsigned int key, const MovPrim& mov) {
        delMov(key);
        return addMov(mov);
      }

      void delete_passed_mov(double time) {
        while (!mov_order.empty()) {
          auto& x = mov_order.top();
          if (movements.count(x.second) == 0) {
            mov_order.pop(); //the move was previously deleted
          } else {
            const MovPrim& m = movements[x.second];
            if ((time-1e-6) > (m.t0 + m.T)) {
              movements.erase(x.second);
              mov_order.pop();
            } else return;
          }
        }
      }

      int control_primitive_id(double time) {
        delete_passed_mov(time);
        if (!mov_order.empty())
          return mov_order.top().second;
        else return -1;
      }

      void control(void* control_signal, const void* curr_state, double time) {
        delete_passed_mov(time);
        if (!mov_order.empty()) {
          unsigned int mov_key = mov_order.top().second;
          const MovPrim& m = movements[mov_key];
          if (time >= m.t0) {
            //Movement has started. Execute current ProMP
            m.mov(control_signal, curr_state, time);;
            return;
          }
        }
        //If no primitive is in execution, call the default controller
        default_controller(control_signal, curr_state, time);
      }
  };

  /**
   * Default constructor for the movement primitive controller class. Instead of using this method
   * you should use whenever possible the static method create(), because attemping to use an
   * object built with this constructor for actual control before setting the required controller methods
   * (see for instance the set_default_controller() method) would result in undefined behaviour.
   */
  MovPrimController::MovPrimController() {
    _impl = unique_ptr<Impl>( new Impl() );
  }

  MovPrimController::~MovPrimController() = default;
      
  void MovPrimController::removeMP(unsigned int key) {
    lock_guard<mutex> lock(_impl->object_mutex);
    _impl->delMov(key);
  }

  unsigned int MovPrimController::addProMP(std::shared_ptr<FullProMP> promp, double t0, double T) {
    lock_guard<mutex> lock(_impl->object_mutex);
    auto promp_controller =  _impl->promp_control_factory(promp, t0, T);
    return _impl->addMov( MovPrim(t0, T, promp_controller) );
  }

  unsigned int MovPrimController::addProMP(std::shared_ptr<FullProMP> promp, double t0, double T,
      unsigned int flags) {
    lock_guard<mutex> lock(_impl->object_mutex);
    MovPrimStart start_flags = static_cast<MovPrimStart>(flags);
    auto promp_controller = ProMPInvDynController(promp, t0, T, start_flags);//_impl->promp_control_factory(promp, t0, T);
    return _impl->addMov( MovPrim(t0, T, promp_controller) );
  }

  unsigned int MovPrimController::editProMP(unsigned int prev_key, std::shared_ptr<FullProMP> promp, 
      double t0, double T) {
    lock_guard<mutex> lock(_impl->object_mutex);
    return _impl->editMov(prev_key, {t0, T, _impl->promp_control_factory(promp, t0, T)});
  }
  
  unsigned int MovPrimController::editProMP(unsigned int prev_key, std::shared_ptr<FullProMP> promp, 
      double t0, double T, unsigned int flags) {
    lock_guard<mutex> lock(_impl->object_mutex);
    MovPrimStart start_flags = static_cast<MovPrimStart>(flags);
    auto promp_controller = ProMPInvDynController(promp, t0, T, start_flags);
    return _impl->editMov(prev_key, {t0, T, promp_controller});
  }

  void MovPrimController::proMP_controller_factory(const ProMPControllerFactory& factory) {
    lock_guard<mutex> lock(_impl->object_mutex);
    _impl->promp_control_factory = factory;
  }

  void MovPrimController::set_default_controller(const Controller& default_controller) {
    lock_guard<mutex> lock(_impl->object_mutex);
    _impl->default_controller = default_controller;
  }
     
  void MovPrimController::control(void* control_signal, const void* curr_state, double time) {
    lock_guard<mutex> lock(_impl->object_mutex);
    _impl->control(control_signal, curr_state, time);
  }

  void MovPrimController::clear() {
    lock_guard<mutex> lock(_impl->object_mutex);
    _impl->clear();
  }

  unsigned int MovPrimController::num_primitives() const {
    lock_guard<mutex> lock(_impl->object_mutex);
    return _impl->movements.size();
  }

  int MovPrimController::control_primitive_id(double time) {
    lock_guard<mutex> lock(_impl->object_mutex);
    return _impl->control_primitive_id(time);
  }

  std::shared_ptr<MovPrimController> MovPrimController::create(const std::string& control_type) {
    unordered_map<string, Controller> defaults { 
      {"inv_dyn", invdyn_gravety_comp}
    };
    unordered_map<string, ProMPControllerFactory> promps {
      {"inv_dyn", promp_invdyn_control}
    };
    if (defaults.count(control_type)==0 || promps.count(control_type)==0) {
      throw std::logic_error("Undefined control type was passed as parameter");
    }
    shared_ptr<MovPrimController> ans { new MovPrimController() };
    ans->set_default_controller( defaults[control_type] );
    ans->proMP_controller_factory( promps[control_type] );
    return ans;
  }
  
};
