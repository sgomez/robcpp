
#include <thread>
#include <mutex>
#include <deque>
#include <vector>
#include <unordered_map>
#include <queue>
#include <random>
#include <robotics/controllers/inv_dyn.hpp>
#include <robotics/utils/random.hpp>

using namespace std;
using namespace arma;

namespace robotics {

  class InvDynSimulation::Impl {
    public:
      default_random_engine gen;
      double noise_q, noise_qd;
      InvDynJointObs state;
      Controller controller;

      Impl(const Controller& controller, const InvDynJointObs& init_cond) {
        this->controller = controller;
        this->state = init_cond;
        noise_q = noise_qd = 1e-3;
      }

      InvDynJointObs step(double deltaT) {
        InvDynSignal uff; //control signal
        normal_distribution<double> rq(0, noise_q), rqd(0, noise_qd);
        controller(&uff, &state, state.time + deltaT);
        state.time += deltaT;
        for (unsigned int j=0; j<state.q.n_elem; j++) {
          state.qd[j] += deltaT*uff.qdd[j] + rqd(gen);
          state.q[j] += deltaT*state.qd[j] + 0.5*deltaT*deltaT*uff.qdd[j] + rq(gen);
        }
        return state;
      }

      vector<InvDynJointObs> sim(double deltaT, unsigned int num_steps) {
        vector<InvDynJointObs> ans;
        InvDynSignal uff; //control signal
        normal_distribution<double> rq(0, noise_q), rqd(0, noise_qd);
        ans.push_back(state);
        for (unsigned int i=0; i<num_steps; i++) {
          controller(&uff, &state, state.time + deltaT);
          state.time += deltaT;
          for (unsigned int j=0; j<state.q.n_elem; j++) {
            state.qd[j] += deltaT*uff.qdd[j] + rqd(gen);
            state.q[j] += deltaT*state.qd[j] + 0.5*deltaT*deltaT*uff.qdd[j] + rq(gen);
          }
          ans.push_back(state);
        }
        return ans;
      }
  };

  InvDynSimulation::InvDynSimulation(const Controller& controller, const InvDynJointObs& init_cond) {
    _impl = unique_ptr<Impl>{ new Impl(controller, init_cond) };
  }

  InvDynSimulation::~InvDynSimulation() = default;

  /**
   * Assuming the noise is Gaussian with zero mean, this method sets the noise standard deviation
   * to the given parameters for both the position and velocity.
   */
  void InvDynSimulation::set_sim_noise(double noise_q, double noise_qd) {
    _impl->noise_q = noise_q;
    _impl->noise_qd = noise_qd;
  }

  /**
   * Sets the initial conditions for the simulation
   */
  void InvDynSimulation::set_init_cond(const InvDynJointObs& init_state) {
    _impl->state = init_state;
  }

  /**
   * Sets the controller function that will set the desired joint positions, velocities and accelerations
   * given the current state.
   */
  void InvDynSimulation::set_controller(const Controller& controller) {
    _impl->controller = controller;
  }

  /**
   * Runs the simulation with the current internal state for the desired number of steps and deltaT value
   */
  std::vector<InvDynJointObs> InvDynSimulation::operator()(double deltaT, unsigned int num_steps) {
    return _impl->sim(deltaT, num_steps);
  }

  /**
   * Returns the simulator internal time
   */
  double InvDynSimulation::time() const {
    return _impl->state.time;
  }

  /**
   * Runs one single simulation step of size deltaT in time and return the new start measurement.
   */
  InvDynJointObs InvDynSimulation::operator()(double deltaT) {
    return _impl->step(deltaT);
  }
  
};
