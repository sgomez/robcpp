
#include <robotics/utils/ransac.hpp>
#include <robotics/utils/random.hpp>
#include <chrono>
#include <random>
#include <vector>

using namespace arma;
using namespace std;

namespace robotics {

  RansacLinAns linear_ransac_iter(const arma::mat& A, const arma::mat& b, const RansacConf& conf,
      const arma::uvec& indices) {
    RansacLinAns ans;
    mat A_iter = A.rows(indices);
    mat b_iter = b.rows(indices);
    ans.x = arma::solve(A_iter, b_iter);
    mat pred = A*ans.x;
    ans.is_inlier.resize(A.n_rows);
    for (unsigned int i=0; i<A.n_rows; i++) {
      double distance = arma::norm(pred.row(i) - b.row(i));
      ans.is_inlier[i] = (distance < conf.tol);
      if (ans.is_inlier[i]) ans.inliers.push_back(i);
    }
    return ans;
  }

  RansacLinAns linear_ransac_iter(const arma::mat& A, const arma::mat& b, const RansacConf& conf,
      const std::vector<unsigned int>& perm) {
    uvec arma_perm(perm.size());
    std::copy(perm.begin(), perm.end(), arma_perm.begin());
    return linear_ransac_iter(A, b, conf, arma_perm);
  }

  RansacLinAns linear_ransac(const arma::mat& A, const arma::mat& b, const RansacConf& conf) {
    RansacLinAns ans;
    unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
    mt19937 uniform_gen(seed);
    mat A_iter(conf.group_size, A.n_cols);
    vec b_iter(conf.group_size);
    for (unsigned int iter=0; iter<conf.num_iter; iter++) {
      vector<unsigned int> perm = random::sample_permutation(uniform_gen, A.n_rows, conf.group_size);
      RansacLinAns tmp = linear_ransac_iter(A, b, conf, perm);
      if (iter==0 || tmp.inliers.size()>ans.inliers.size()) {
        std::swap(ans.inliers, tmp.inliers);
        std::swap(ans.is_inlier, tmp.is_inlier);
        std::swap(ans.x, tmp.x);
      }
    }
    return linear_ransac_iter(A, b, conf, ans.inliers);
  }
};
