
#include <robotics/callback.hpp>
#include <deque>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

namespace robotics {

  class AsyncCallback::Impl {
    public:
      mutex obj_mtx;
      condition_variable obj_cv;
      bool running;
      deque<function<void(void)>> callbacks;
      thread exec;

      Impl() {
        running = false;
      }

      void run() {
        while (true) {
          //1) Gather next callback or exit
          unique_lock<mutex> l{obj_mtx};
          while (running && callbacks.empty()) {
            obj_cv.wait(l);
          }
          if (!running) break;
          auto cb = callbacks.front();
          callbacks.pop_front();
          l.unlock();
          //2) Actually call the callback
          cb();
        }
      }

      void start() {
        unique_lock<mutex> l(obj_mtx);
        if (!running) {
          running = true;
          exec = thread { [this]() -> void {this->run();} };
        }
      }

      void stop() {
        //1) Lock and set state so thread finished safely
        unique_lock<mutex> l(obj_mtx);
        running = false;
        obj_cv.notify_all();
        l.unlock();
        //2) Unlock to let thread run and wait until it finishes
        if (exec.joinable()) exec.join();
      }
  };

  AsyncCallback::AsyncCallback() {
    _impl = unique_ptr<Impl>( new Impl );
    _impl->start();
  }

  AsyncCallback::~AsyncCallback() {
    _impl->stop();
  }

  void AsyncCallback::start() {
    _impl->start();
  }

  void AsyncCallback::stop() {
    _impl->stop();
  }

  void AsyncCallback::add_callback(const function<void(void)>& callback) {
    unique_lock<mutex> l(_impl->obj_mtx);
    _impl->callbacks.push_back(callback);
    _impl->obj_cv.notify_all();
  }
};
