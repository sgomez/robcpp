
#include "robotics/lss_model.hpp"

using namespace std;
using namespace arma;

namespace robotics {

  class LSSGaussState::Impl {
    private:

      /*pair<vec, mat> obs_dist(const mat& C, const mat& D, const mat& Sigma, const vec& bias) const {
        vec obs_mean = C*mean + D*bias;
        mat obs_cov = Sigma + C*cov*C.t();
        return make_pair(obs_mean, obs_cov);
      }*/
    public:
      random::NormalDist dist;
      //arma::vec mean;
      //arma::mat cov;

      Impl(const random::NormalDist& dist) : dist(dist) {
      }

      Impl(const arma::vec& mean, const arma::mat& cov) : dist(mean,cov) {
      }

      Impl(const Impl& b) = default;

      random::NormalDist obs_dist(const LSSGaussState& self, const AbstractLSSModel& dynamics, 
          const vec& bias, const void* params) const {
        mat C = dynamics.obs_mat(self, params);
        mat D = dynamics.obs_bias(self, params);
        mat Sigma = dynamics.obs_noise(self, params);
        random::NormalDist ans { C*dist.mean() + D*bias, Sigma + C*dist.cov()*C.t() };
        return ans;
      }

      LSSGaussState obs(const LSSGaussState& self, const AbstractLSSModel& dynamics, 
          const vec& obs, const vec& bias, const void* params) const {
        mat C = dynamics.obs_mat(self, params);
        auto obs_d = obs_dist(self, dynamics, bias, params);
        mat K = dist.cov()*C.t()*obs_d.inv_cov();
        mat ncov = dist.cov() - K*C*dist.cov();
        return LSSGaussState(dist.mean() + K*(obs - obs_d.mean()), 0.5*(ncov + ncov.t()));
      }

      double obs_mah_dist(const LSSGaussState& self, const AbstractLSSModel& dynamics, 
          const vec& obs, const vec& bias, const void* params) const {
        auto obs_d = obs_dist(self, dynamics, bias, params);
        mat diff = obs - obs_d.mean();
        return sqrt(dot(diff, obs_d.inv_cov()*diff));
      }

      LSSGaussState next(const LSSGaussState& self, const AbstractLSSModel& dynamics,
          const vec& action, const void* params) const {
        mat A = dynamics.transition(self, params);
        mat B = dynamics.action_mat(self, params);
        mat Gamma = dynamics.trans_noise(self, params);
        vec nmean = A*dist.mean() + B*action;
        mat ncov = Gamma + A*dist.cov()*A.t();
        return LSSGaussState(nmean, 0.5*(ncov + ncov.t()));
      }
  };

  LSSGaussState::LSSGaussState() {
    _impl = nullptr;
  }
      
  LSSGaussState::LSSGaussState(const arma::vec init_mean, const arma::mat init_cov) {
    _impl = unique_ptr<Impl>( new Impl(init_mean, init_cov) );
  }

  LSSGaussState::LSSGaussState(const random::NormalDist& dist) {
    _impl = unique_ptr<Impl>( new Impl(dist) );
  }

  LSSGaussState::LSSGaussState(const LSSGaussState& b) {
    if (b._impl) {
      _impl = unique_ptr<Impl>( new Impl(*b._impl) );
    }
  }
      
  LSSGaussState& LSSGaussState::operator=(const LSSGaussState& b) {
    if (&b != this && b._impl) {
      _impl = unique_ptr<Impl>( new Impl(*b._impl) );
    }
    return *this;
  }

  LSSGaussState::LSSGaussState(LSSGaussState&& b) = default;
  LSSGaussState& LSSGaussState::operator=(LSSGaussState&& b) = default;
  LSSGaussState::~LSSGaussState() = default;

  LSSGaussState LSSGaussState::observe(const AbstractLSSModel& dynamics, const arma::vec& obs, 
          const arma::vec& bias, const void* params) const {
    return _impl->obs(*this, dynamics, obs, bias, params);
  }

  LSSGaussState LSSGaussState::next(const AbstractLSSModel& dynamics, const arma::vec& action, 
          const void* params) const {
    return _impl->next(*this, dynamics, action, params);
  }


  /**
   * @brief Returns the distribution of the observation variable
   */
  random::NormalDist LSSGaussState::obs_dist(const AbstractLSSModel& dynamics, const vec& bias, 
      const void* params) const {
    return _impl->obs_dist(*this, dynamics, bias, params);
  }

  /**
   * @brief Returns the Mahalanobis distance between the given observation and the predicted observation.
   */
  double LSSGaussState::obs_mah_dist(const AbstractLSSModel& dynamics, const arma::vec& obs, 
          const arma::vec& bias, const void* params) const {
    return _impl->obs_mah_dist(*this, dynamics, obs, bias, params);
  }

  const arma::vec& LSSGaussState::mean() const {
    return _impl->dist.mean();
  }

  const arma::mat& LSSGaussState::cov() const {
    return _impl->dist.cov();
  }

  /**
   * @brief Returns the state distribution
   */
  const random::NormalDist& LSSGaussState::dist() const {
    return _impl->dist;
  }


  class ConstLSSModel::Impl {
    public:
      arma::mat A, B, C, D, Sigma, Gamma;

  };

  ConstLSSModel::ConstLSSModel(const arma::mat& A, const arma::mat& B, const arma::mat& C, const arma::mat& D,
                      const arma::mat& Sigma, const arma::mat& Gamma) {
    _impl = unique_ptr<Impl>( new Impl{A, B, C, D, Sigma, Gamma} );
  }

  ConstLSSModel::ConstLSSModel(const ConstLSSModel& b) {
    if (b._impl) {
      _impl = unique_ptr<Impl>( new Impl(*b._impl) );
    }
  }

  ConstLSSModel::ConstLSSModel(ConstLSSModel&& b) = default;
  ConstLSSModel::~ConstLSSModel() = default;

  ConstLSSModel& ConstLSSModel::operator=(const ConstLSSModel& b) {
    if (&b != this && b._impl) {
      _impl = unique_ptr<Impl>( new Impl(*b._impl) );
    }
    return *this;
  }

  ConstLSSModel& ConstLSSModel::operator=(ConstLSSModel&& b) = default;

  arma::mat ConstLSSModel::transition(const LSSGaussState& state, const void* params) const {
    (void)state; (void)params; //to get rid of unused warning
    return _impl->A;
  }

  arma::mat ConstLSSModel::action_mat(const LSSGaussState& state, const void* params) const {
    (void)state; (void)params; //to get rid of unused warning
    return _impl->B;
  }

  arma::mat ConstLSSModel::obs_mat(const LSSGaussState& state, const void* params) const {
    (void)state; (void)params; //to get rid of unused warning
    return _impl->C;
  }

  arma::mat ConstLSSModel::obs_bias(const LSSGaussState& state, const void* params) const {
    (void)state; (void)params; //to get rid of unused warning
    return _impl->D;
  }

  arma::mat ConstLSSModel::obs_noise(const LSSGaussState& state, const void* params) const {
    (void)state; (void)params; //to get rid of unused warning
    return _impl->Sigma;
  }

  arma::mat ConstLSSModel::trans_noise(const LSSGaussState& state, const void* params) const {
    (void)state; (void)params; //to get rid of unused warning
    return _impl->Gamma;
  }

  unsigned int ConstLSSModel::dim_hidden() const {
    return _impl->A.n_cols;
  }
      
  unsigned int ConstLSSModel::dim_obs() const {
    return _impl->C.n_rows;
  }

  std::vector<arma::vec> sample_lss_hidden_states(const LSSGaussState& init, const AbstractLSSModel& dyn,
      const std::vector<arma::vec>& actions, const void* params) {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed);
    std::vector<arma::vec> ans = random::sample_multivariate_normal(gen, init.dist(), 1);
    random::NormalDist model_noise(arma::zeros(ans.front().n_elem), dyn.trans_noise(init, params));
    for (auto action : actions) {
      LSSGaussState tmp{ans.back(), model_noise.cov()};
      mat A = dyn.transition(tmp, params);
      mat B = dyn.action_mat(tmp, params);
      mat Gamma = dyn.trans_noise(tmp, params);
      vec pred = A*ans.back() + B*action;
      //Add model noise to prediction
      pred += random::sample_multivariate_normal(gen, model_noise, 1).front();
      ans.push_back(pred);
      if (arma::norm(Gamma - model_noise.cov()) > 1e-6) {
        model_noise.set_cov(Gamma);
      }
    }
    return ans;
  }

  std::vector<arma::vec> sample_lss_observations(const std::vector<arma::vec>& hidden_states, 
      const AbstractLSSModel& dyn, const std::vector<arma::vec>& actions, const void* params) {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed);
    vector<vec> ans;
    unsigned int dim = dyn.dim_hidden();
    mat hid_zeros = arma::zeros(dim,dim);
    LSSGaussState tmp{hidden_states.front(), hid_zeros};
    random::NormalDist obs_noise{arma::zeros(dyn.dim_obs()), dyn.obs_noise(tmp, params)};
    for (unsigned int i=0; i<hidden_states.size(); i++) {
      mat C = dyn.obs_mat(tmp, params);
      mat D = dyn.obs_bias(tmp, params);
      mat Sigma = dyn.obs_noise(tmp, params);
      vec obs = C*hidden_states[i] + D*actions[i];
      obs += random::sample_multivariate_normal(gen, obs_noise, 1).front();
      ans.push_back(obs);
      if (arma::norm(Sigma - obs_noise.cov()) > 1e-6) {
        obs_noise.set_cov(Sigma);
      }
    }
    return ans;
  }
};
