
#include <robotics/kinematics.hpp>
#include "json.hpp"
#include "robotics/utils.hpp"
#include <streambuf>
#include <nlopt.hpp>

using namespace std;
using namespace arma;
using json = nlohmann::json;

namespace robotics {

  std::shared_ptr<FwdKin> create_barrett_wam_kin(const nlohmann::json& conf) {
    auto ans = [conf](const vec& q) -> std::vector<arma::mat> {
      vec sq = arma::sin(q), cq = arma::cos(q);
      json params = conf.at("config");
      const vector<vector<double>> tmp {
        {cq[0],-sq[0],0,0, sq[0],cq[0],0,0, 0,0,1,params.at("ZSFE"), 0,0,0,1},
        {0,0,-1,0, sq[1],cq[1],0,0, cq[1],-sq[1],0,0, 0,0,0,1},
        {0,0,1,params.at("ZHR"), sq[2],cq[2],0,0, -cq[2],sq[2],0,0, 0,0,0,1},
        {0,0,-1,0,sq[3],cq[3],0,params.at("YEB"),cq[3],-sq[3],0,params.at("ZEB"),0,0,0,1},
        {0,0,1,params.at("ZWR"),sq[4],cq[4],0,params.at("YWR"),-cq[4],sq[4],0,0,0,0,0,1},
        {0,0,-1,0, sq[5],cq[5],0,0, cq[5],-sq[5],0,params.at("ZWFE"),0,0,0,1},
        {0,0,1,0, sq[6],cq[6],0,0, -cq[6],sq[6],0,0, 0,0,0,1}
      };
      vector<mat> ans;
      for (auto m : tmp) {
        mat A(4,4);
        std::copy(m.begin(), m.end(), A.begin());        
        ans.push_back(A.t());
      }
      return ans;
    };
    return make_shared<FwdKin>(ans, json2mat(conf.at("base")), json2vec(conf.at("endeff")));
  }

  std::shared_ptr<FwdKin> create_barrett_wam_kin(const std::string& json_str) {
    json conf = json::parse(json_str);
    return create_barrett_wam_kin(conf);
  }

  std::shared_ptr<FwdKin> load_barrett_wam_kin(const std::string& file_name) {
    ifstream in(file_name);
    string ans {std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>()};
    return create_barrett_wam_kin(ans);
  }

  /**
   * Implementation class of the generic forward kinematics class
   */
  class FwdKin::Impl {
    public:
      std::function<std::vector<arma::mat>(const arma::vec&)> joints; 
      arma::mat base;
      arma::mat endeff_mat;

      void update_endeff(const arma::vec& endeff) {
        double sa = sin(endeff[3]);
        double ca = cos(endeff[3]);
        double sb = sin(endeff[4]);
        double cb = cos(endeff[4]);
        double sg = sin(endeff[5]);
        double cg = cos(endeff[5]);
        const vector<double> tmp {
          cb*cg, -(cb*sg), sb, endeff[0],
          cg*sa*sb + ca*sg, ca*cg - sa*sb*sg, -(cb*sa), endeff[1], 
          -(ca*cg*sb) + sa*sg, cg*sa + ca*sb*sg, ca*cb, endeff[2],
          0,0,0,1
        };
        mat ef(4,4);
        std::copy(tmp.begin(), tmp.end(), ef.begin());
        endeff_mat = ef.t();
      }

      /**
       * Returns a vector of matrices where the first one is the base matrix and
       * the rest are a product of the kinematic matrices from the start to the
       * end effector matrix. The first matrix is not returned in the equivalent
       * python code, so add one to the indexes with respect to the python version
       */
      vector<mat> trans_matrices(const arma::vec& q) const {
        auto matrices = joints(q);
        vector<mat> ans;
        ans.push_back(base);
        matrices.push_back(endeff_mat);
        for (auto M : matrices) {
          ans.push_back(ans.back()*M);
        }
        return ans;
      }

      /**
       * Computes the analytic jacobian assuming always a rotation over the same axis, given
       * a set of transformation matrices with respect to the same origin.
       */
      mat analytic_jac(const vec& q, const vector<mat>& tmat) const {
        mat ans(6, q.n_elem, fill::zeros);
        vec pe = tmat.back().submat(0,3,2,3);
        for (unsigned int i=0; i<q.n_elem; i++) {
          vec zprev = tmat[i+1].submat(0,2,2,2);
          vec pprev = tmat[i+1].submat(0,3,2,3);
          ans.submat(0,i,2,i) = arma::cross(zprev, pe - pprev);
          ans.submat(3,i,5,i) = zprev;
        }
        return ans;
      }

      vec rot_mat_to_euler(const arma::mat& rot_mat) const {
        vec eul(3);
        double a = rot_mat(2,1), b = rot_mat(2,2);
        eul[0] = atan2(-a,b);
        eul[1] = atan2(rot_mat(2,0),sqrt(a*a + b*b));
        eul[2] = atan2(-rot_mat(1,0), rot_mat(0,0));
        return eul;
      }

      Impl(const std::function<std::vector<arma::mat>(const arma::vec&)>& joints, 
          const arma::mat& base, const arma::vec& endeff_vec) : joints(joints), base(base) {
        update_endeff(endeff_vec);
      }
  };

  FwdKin::FwdKin(const std::function<std::vector<arma::mat>(const arma::vec&)>& joints, 
          const arma::mat& base, const arma::vec& endeff) {
    _impl = unique_ptr<Impl>( new Impl(joints, base, endeff) );
  }

  FwdKin::FwdKin(const FwdKin& b) {
    _impl = unique_ptr<Impl>(new Impl(*b._impl));
  }

  FwdKin::FwdKin(FwdKin&& b) {
    _impl = std::move(b._impl);
  }

  FwdKin& FwdKin::operator=(const FwdKin& b) {
    if (&b != this) {
      _impl = unique_ptr<Impl>(new Impl(*b._impl));
    }
    return *this;
  }

  FwdKin& FwdKin::operator=(FwdKin&& b) {
    if (&b != this) {
      _impl = std::move(b._impl);
    }
    return *this;
  }

  std::vector<arma::mat> FwdKin::trans_matrices(const arma::vec& q) {
    return _impl->trans_matrices(q);
  }

  FwdKin::Answer FwdKin::operator()(const arma::vec& q) const {
    vector<mat> As = _impl->trans_matrices(q);
    Answer ans;
    ans.position = As.back().submat(0,3,2,3);
    ans.orientation = _impl->rot_mat_to_euler(As.back().submat(0,0,2,2).t());
    ans.jacobian = _impl->analytic_jac(q, As);
    return ans;
  }

  namespace {

    struct ProbInvKinState {
      FwdKin fwd_kin;
      random::NormalDist q_prior;
      random::NormalDist x_desired;

      ProbInvKinState(const FwdKin& fwd_kin, const random::NormalDist& q_prior, 
          const random::NormalDist& x_desired) : fwd_kin(fwd_kin), q_prior(q_prior), 
          x_desired(x_desired) {
      }
    };

    double prob_inv_kin_pos_laplace_obj(const vector<double>& _q, vector<double>& _grad, 
        void* f_data) {
      const ProbInvKinState& state = *static_cast<ProbInvKinState*>(f_data);
      vec q = _q;
      FwdKin::Answer ka = state.fwd_kin(q);
      mat jac_th = ka.jacobian.submat(0,0,2,6);
      vec diff1 = q - state.q_prior.mean();
      vec diff2 = ka.position - state.x_desired.mean();
      mat tmp1 = state.q_prior.inv_cov()*diff1, tmp2 = state.x_desired.inv_cov()*diff2;
      double nll = 0.5*(arma::dot(diff1, tmp1) + arma::dot(diff2, tmp2));
      vec grad_nll = tmp1 + jac_th.t()*tmp2;
      std::copy(grad_nll.begin(), grad_nll.end(), _grad.begin());
      return nll;
    }
  };

  /**
   * Using forward kinematics, computes a Gaussian posterior in Joint Space given
   * a Gaussian Prior in task space and a Gaussian desired task space distribution
   * for the position only. This method does not work with the orientation and
   * velocity.
   * @brief Probabilistic inverse kinematics for the position of the end effector.
   * @param[in] fwd_kin Forward kinematics of the robot
   * @param[in] q_prior Prior probability distribution over the joint angles
   * @param[in] x_desired Probability distribution of the desired position of the end effector
   */
  random::NormalDist prob_inv_kin_pos(const FwdKin& fwd_kin, const random::NormalDist& q_prior,
      const random::NormalDist& x_desired) {
    unsigned int D = q_prior.mean().n_elem;
    nlopt::opt my_opt(nlopt::LD_LBFGS, D);
    ProbInvKinState inner_st{fwd_kin, q_prior, x_desired};
    my_opt.set_lower_bounds(-acos(-1));
    my_opt.set_upper_bounds(acos(-1));
    my_opt.set_ftol_rel(1e-6);
    my_opt.set_xtol_abs(1e-4);
    //my_opt.set_initial_step(1e-3);
    my_opt.set_min_objective(prob_inv_kin_pos_laplace_obj, &inner_st);
    vector<double> post_q_mean(q_prior.mean().begin(), q_prior.mean().end());
    double opt_nll;
    my_opt.optimize(post_q_mean, opt_nll);
        
    //Now, numerically approximate the Hessian
    mat hessian(D,D);
    const double eps = 1e-5;
    for (unsigned int i=0; i<D; i++) {
      vector<double> g1(D), g2(D);
      post_q_mean[i] += eps;
      prob_inv_kin_pos_laplace_obj(post_q_mean, g1, &inner_st);
      post_q_mean[i] -= 2*eps;
      prob_inv_kin_pos_laplace_obj(post_q_mean, g2, &inner_st);
      post_q_mean[i] += eps;
      for (unsigned int j=0; j<D; j++) {
        hessian(i,j) = (g1[j] - g2[j]) / (2*eps);
      }
    }

    random::NormalDist ans {vec(post_q_mean), hessian.i()};
    return ans;
  }

  /**
   * Computes a probability distribution in task space for the given distribution in joint space. The
   * current implementation simply linearizes the forward kinematics function for such a purpose.
   * @brief Computes a probability distribution in task space for the given distribution in joint space
   * @param[in] fwd_kin Forward kinematics object
   * @param[in] q_dist Probability distribution in joint space
   */
  random::NormalDist prob_fwd_kin_pos(const FwdKin& fwd_kin, const random::NormalDist& q_dist) {
    auto fw_ans = fwd_kin(q_dist.mean());
    mat jac = fw_ans.jacobian.submat(0,0,2,6);
    return random::NormalDist{ fw_ans.position, jac*q_dist.cov()*jac.t() };
  }
};
