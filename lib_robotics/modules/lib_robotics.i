
%module lib_robotics
%{
#define SWIG_FILE_WITH_INIT
#include <robotics.hpp>
#include <vector>
#include <array>
#include <unordered_map>

typedef std::array<double,2> pt2d;
typedef std::array<double,3> pt3d;

arma::mat mult_test(const arma::mat& A, const arma::mat& B) {
  return A*B;
}

%}

%include "armanpy.i"
%include "std_vector.i"
%include "std_array.i"
%include "std_string.i"

typedef std::array<double,2> pt2d;
typedef std::array<double,3> pt3d;

namespace std {
  %template(pt2d) array<double,2>;
  %template(pt3d) array<double,3>;
  %template(list_pt2d) vector<pt2d>;
  %template(list_pt3d) vector<pt3d>;
  %template(list_mat) vector<arma::mat>;
};

namespace robotics {

  struct RansacConf {
    double tol; //!< Tolerance value. Distances greater than this will be considered outliers
    unsigned int group_size; //!< Size of the groups picked in every iteration
    unsigned int num_iter; //!< Number of iterations the algorithm will be runned
  };

  struct RansacLinAns {
    arma::mat x; //!< Solution to the linear equation
    std::vector<unsigned int> inliers; //!< The indices of the inliers
    std::vector<bool> is_inlier; //!< Which instances are inliers (true) or outliers (false).
  };

  RansacLinAns linear_ransac(const arma::mat& A, const arma::mat& b, const RansacConf& conf);

  arma::mat calibrate_camera(const std::vector<pt2d>& obs2d, const std::vector<pt3d>& obs3d);
  pt3d stereo_vision(const std::vector<arma::mat>& proj_mat, const std::vector<pt2d>& pts2d);
};
