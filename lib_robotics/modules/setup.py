from setuptools import setup, find_packages

setup(
    name = "lib_robotics",
    version = "0.0.1",
    author = "Sebastian Gomez",
    author_email = "sgomez@tue.mpg.de",
    description = ("A subset of Python bindings for the lib_robotics c++ library"),
    license = "Not sure, ask the Max Plank Institute",
    keywords = "lib_robotics C++ binding robotics",
    url = "https://gitlab.tuebingen.mpg.de/sgomez/robcpp",
    packages=find_packages(),
    #long_description=read('README'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
