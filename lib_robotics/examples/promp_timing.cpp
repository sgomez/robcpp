
#include <fstream>
#include <armadillo>
#include <memory>
#include <vector>
#include <unordered_map>
#include <chrono>
#include <random>
#include <robotics.hpp>
#include <boost/program_options.hpp>

using namespace std;
using namespace arma;
using namespace robotics;
using json = nlohmann::json;

json time_experiment(const FullProMP& promp, const FwdKin& fwd_kin,
    unsigned int rep) {
  json ans;
  default_random_engine gen;
  for (unsigned int i=0; i<rep; i++) {
    //1) Time marginalizing w
    auto t1 = std::chrono::high_resolution_clock::now();
    auto q_dist = promp.joint_dist(0.5);
    auto t2 = std::chrono::high_resolution_clock::now();
    ans["marg_w"].push_back(chrono::duration<double>(t2 - t1).count());

    //2) Time prob fwd kinematics
    t1 = std::chrono::high_resolution_clock::now();
    auto x_dist = prob_fwd_kin_pos(fwd_kin, q_dist);
    t2 = std::chrono::high_resolution_clock::now();
    ans["fwd_kin"].push_back(chrono::duration<double>(t2 - t1).count());

    //3) Time prob inv kinematics
    auto x_cond = sample_multivariate_normal(gen, x_dist, 1);
    robotics::random::NormalDist x_cond_dist{x_cond.front(), 1e-3*arma::eye<mat>(3,3)};
    t1 = std::chrono::high_resolution_clock::now();
    auto q_dist_cond = prob_inv_kin_pos(fwd_kin, q_dist, x_cond_dist);
    t2 = std::chrono::high_resolution_clock::now();
    ans["inv_kin"].push_back(chrono::duration<double>(t2 - t1).count());

    //4) Time condition in joint space
    t1 = std::chrono::high_resolution_clock::now();
    auto promp_cond = promp.condition_pos(0.5, q_dist_cond);
    t2 = std::chrono::high_resolution_clock::now();
    ans["promp_cond"].push_back(chrono::duration<double>(t2 - t1).count());
  }
  return ans;
}

int main(int argc, char** argv) {
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help", "Produce help message")
      ("input,i", po::value<string>(), "path to the ProMP input json file")
      ("fwdkin", po::value<string>(), "path to the Barrett Forward Kinematics config file")
      ("output,o", po::value<string>(), "path to an "
       "output file where the timing-data is stored")
      ("rep,r", po::value<int>(), "number of times each experiment should be run")
      ("conf,c", po::value<string>(), "path to JSON "
       "configuration file");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help") || !vm.count("input") || !vm.count("fwdkin")) {
      cout << desc << endl;
      return 0;
    }
    
    unsigned int rep = 100;
    if (vm.count("rep")) rep = vm["rep"].as<int>();

    ifstream in(vm["input"].as<string>());
    json promp_pars;
    in >> promp_pars;
    FullProMP promp = json2full_promp(promp_pars);

    auto fwd_kin = load_barrett_wam_kin(vm["fwdkin"].as<string>());

    auto ans = time_experiment(promp, *fwd_kin, rep);
    if (vm.count("output")) {
      ofstream out(vm["output"].as<string>());
      out << ans;
      out.close();
    } else {
      cout << ans << endl;
    }
  } catch (std::exception& ex) {
    cerr << "Error: " << ex.what() << endl;
    return 1;
  }
}
