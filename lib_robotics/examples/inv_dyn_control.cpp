
#include <fstream>
#include <armadillo>
#include <memory>
#include <random>
#include <chrono>
#include <robotics.hpp>

using namespace std;
using namespace std::chrono;
using namespace arma;
using namespace robotics;
using json = nlohmann::json;

int main() {
  //InvDynControl controller;
  auto controller_ptr = MovPrimController::create("inv_dyn");
  MovPrimController& controller = *controller_ptr;

  //Create the required ProMP that takes you from q=1 at time t=1 to q=0.5 in time t=2
  shared_ptr<FullProMP> mov { new FullProMP(go_to_promp(vec{0.5},vec{0},1.0)) };
  unsigned int k1 = controller.addProMP(mov, 1.0, 1.0);

  //Create a predecesor ProMP (with T=1) that take you from where you are to the starting position and vel
  auto pre_mov = make_shared<FullProMP>( go_to_promp(vec{1.0},vec{0.0},1.0) );
  controller.addProMP(pre_mov, 0.0, 1.0);

  //Freeze for a second at the end
  auto post_mov = make_shared<FullProMP>( freeze_promp(1) );
  controller.addProMP(post_mov, 2.0, 1.0, 0x1);

  //Now simulate a system that work reasonably well
  default_random_engine gen;
  normal_distribution<double> noise(0,1e-3);

  double deltaT = 0.01;
  double q=2.0, qd=0.0;
  for (double t=0.0; t<2.5; t+=deltaT) {
    if (fabs(t-1.5)<1e-6) {
      //Simulate a change of goal at time 1.5
      double new_goal = 0.3; //instead of 0.5 that was before
      auto new_move = make_shared<FullProMP>( go_to_promp(vec{new_goal},vec{0},1.0) );
      k1 = controller.editProMP(k1, new_move, 1.0, 1.0);
    }
    cout << "Current state: t=" << t << " q=" << q << " qd=" << qd << endl;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    InvDynJointObs curr_obs{vec{q}, vec{qd}, t};
    InvDynSignal u;
    controller.control(&u, &curr_obs, t+deltaT);
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>( t2 - t1 ).count();
    cout << "Action: u=(" << u.q[0] << "," << u.qd[0] << "," << u.qdd[0] << ") comp_time=" << 
      duration << "us" << endl;
    q = q + deltaT*qd + 0.5*u.qdd[0]*deltaT*deltaT + noise(gen);
    qd = qd + deltaT*u.qdd[0] + noise(gen);
  }
 
}
