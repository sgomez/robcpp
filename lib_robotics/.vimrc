set colorcolumn=110
highlight ColorColumn ctermbg=darkgray

set path=.,./lib_robotics/include,/usr/include
let g:ycm_auto_trigger = 1
let g:ycm_extra_conf_globlist = ['/is/ei/sgomez/Documents/repos/robcpp/lib_robotics/*']
