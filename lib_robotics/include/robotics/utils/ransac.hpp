#ifndef ROBOTICS_UTILS_RANSAC_HPP
#define ROBOTICS_UTILS_RANSAC_HPP

#include <memory>
#include <armadillo>
#include <vector>

namespace robotics {

  /**
   * @brief Ransac configuration parameters
   */
  struct RansacConf {
    double tol; //!< Tolerance value. Distances greater than this will be considered outliers
    unsigned int group_size; //!< Size of the groups picked in every iteration
    unsigned int num_iter; //!< Number of iterations the algorithm will be runned
  };

  /**
   * @brief Represents the answer of the RANSAC algorithm
   */
  struct RansacLinAns {
    arma::mat x; //!< Solution to the linear equation
    std::vector<unsigned int> inliers; //!< The indices of the inliers
    std::vector<bool> is_inlier; //!< Which instances are inliers (true) or outliers (false).
  };

  /**
   * Runs RANSAC algorithm to find the solution to the equation Ax = b in a robust to outliers
   * way.
   */
  RansacLinAns linear_ransac(const arma::mat& A, const arma::mat& b, const RansacConf& conf);

};

#endif
