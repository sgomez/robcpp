#ifndef ROBOTICS_CALLBACK_H
#define ROBOTICS_CALLBACK_H

#include <armadillo>
#include <memory>
#include <functional>

namespace robotics {

  /**
   * @brief Asynchronous callback method handler
   * @since version 0.0.1
   * This class collect callback functions to be called, and calls each
   * of the received functions in an asynchronous way. It also ensures that
   * the callback methods are called in the same order as they are added
   * to the object.
   */ 
  class AsyncCallback {
    public:
      AsyncCallback();
      ~AsyncCallback();

      void add_callback(const std::function<void(void)>& callback);
      void start();
      void stop();
    private:
      class Impl;
      std::unique_ptr<Impl> _impl;
  };

};

#endif
