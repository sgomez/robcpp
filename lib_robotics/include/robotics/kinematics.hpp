#ifndef ROBOTICS_KINEMATICS_H
#define ROBOTICS_KINEMATICS_H

#include <armadillo>
#include <memory>
#include <vector>
#include <string>
#include <functional>
#include <robotics/utils/random.hpp>
#include <json.hpp>

namespace robotics {

  /**
   * @brief Generic forward kinematics. Computes the configuration of the end effector
   * given the joint configuration.
   */
  class FwdKin {
    private:
      class Impl;
      std::unique_ptr<Impl> _impl;
    public:
      struct Answer {
        arma::vec position; //!< Position of the end effector
        arma::vec orientation; //<! Orientation of the end effector
        arma::mat jacobian; //!< Jacobian of the end effector forward kinematics
      };

      /**
       * @brief Construct a forward kinematics object
       * @param[in] joints Function that returns a vector of transformation matrices relative to
       * the previous joint given the joint state configuration
       * @param[in] base Transformation matrix for the base of the robot
       * @param[in] endeff Translation and rotation of the end effector relative to the last joint
       */
      FwdKin(const std::function<std::vector<arma::mat>(const arma::vec&)>& joints, 
          const arma::mat& base, const arma::vec& endeff);

      FwdKin(const FwdKin& b);
      FwdKin(FwdKin&& b);
      FwdKin& operator=(const FwdKin& b);
      FwdKin& operator=(FwdKin&& b);

      /**
       * Receives a joint configuration vector with equal number of dimensions as degrees of freedom
       * in the robot and returns a vector of transformation matrices, the first corresponding to the
       * robot base and the last to the end effector.
       */
      std::vector<arma::mat> trans_matrices(const arma::vec& q);

      /**
       * @brief Computes the forward kinematics for the given joint configuration
       */
      Answer operator()(const arma::vec& q) const;
  };

  random::NormalDist prob_inv_kin_pos(const FwdKin& fwd_kin, const random::NormalDist& q_prior,
      const random::NormalDist& x_desired);

  random::NormalDist prob_fwd_kin_pos(const FwdKin& fwd_kin, const random::NormalDist& q_dist);

  std::shared_ptr<FwdKin> load_barrett_wam_kin(const std::string& file_name);
  std::shared_ptr<FwdKin> create_barrett_wam_kin(const nlohmann::json& conf);
  std::shared_ptr<FwdKin> create_barrett_wam_kin(const std::string& json_str);

};

#endif
