#ifndef ROBOTICS_CONTROLLER_INVDYN_HPP
#define ROBOTICS_CONTROLLER_INVDYN_HPP

#include <armadillo>
#include <memory>
#include <functional>
#include <deque>
#include "robotics/full_promp.hpp"
#include "robotics/trajectory.hpp"

/**
 * @file
 * This file contains the declarations of classes, structs, functions and data types required for
 * a simple controller based on inverse dynamics
 */

namespace robotics {

  /**
   * This structure represents the control signal of a Inverse Dynamics based controller, that
   * basically consists on a desired joint state configuration
   * @brief Inverse Dynamics Control Signal
   */
  struct InvDynSignal {
    arma::vec q; //!< Joint desired angles
    arma::vec qd; //!< Joint desired angular velocities
    arma::vec qdd; //!< Joint desired angular acceleration
  };

  /**
   * This data type represents the joint measurements that are required to observe for an inverse
   * dynamics based controller.
   * @brief Joint observation representation
   */
  struct InvDynJointObs {
    arma::vec q; //!< Observed joint angles
    arma::vec qd; //!< Observed angular velocity
    double time; //!< Time of the obtained observation
  };

  /**
   * @brief This class provides a simple simulator for an inverse dynamics based controller.
   */
  class InvDynSimulation {
    public:
      /**
       * The controller function get a void pointer to the control signal of type InvDynSignal, a
       * const void pointer to the current state of type InvDynJointObs and the current time.
       * @brief Controller function type
       */
      using Controller = std::function<void(void*, const void*, double)>;

      InvDynSimulation(const Controller& controller, const InvDynJointObs& init_cond);
      ~InvDynSimulation();

      void set_sim_noise(double noise_q, double noise_qd);
      void set_init_cond(const InvDynJointObs& init_state);
      void set_controller(const Controller& controller);

      double time() const;

      std::vector<InvDynJointObs> operator()(double deltaT, unsigned int num_steps);
      InvDynJointObs operator()(double deltaT);
    private:
      class Impl;
      std::unique_ptr<Impl> _impl;
  };
};

#endif
