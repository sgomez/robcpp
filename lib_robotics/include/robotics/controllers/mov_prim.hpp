#ifndef ROBOTICS_CONTROLLER_MOV_PRIM_HPP
#define ROBOTICS_CONTROLLER_MOV_PRIM_HPP

#include <functional>
#include <memory>
#include <robotics/full_promp.hpp>

namespace robotics {

  /**
   * This class mantains a set of movement primitives to be executed. Each movement primitive
   * consists of a time interval range (Where it is active) and a pointer to the primitive 
   * representation. This class also provides control signals that can be for instance called
   * from SL, but before using it make sure you set all the factory methods for your primitive
   * representations.
   * @brief A controller to track movement primitives
   */
  class MovPrimController {
    public:
      /**
       * Type of the controller function. Receives a pointer to the output control signal, a pointer
       * to the current state and the current time.
       */
      using Controller = std::function<void(void*,const void*, double)>;

      /**
       * Type for a ProMP controller factory method. The function receives a pointer to the ProMP 
       * representation, the initial time of the ProMP and its duration.
       */
      using ProMPControllerFactory = std::function<Controller(std::shared_ptr<FullProMP>, 
          double, double)>;

      MovPrimController();
      ~MovPrimController();
      
      void removeMP(unsigned int key);
      unsigned int addProMP(std::shared_ptr<FullProMP> promp, double t0, double T);
      unsigned int addProMP(std::shared_ptr<FullProMP> promp, double t0, double T, unsigned int flags);
      unsigned int editProMP(unsigned int prev_key, std::shared_ptr<FullProMP> promp, double t0, double T);
      unsigned int editProMP(unsigned int prev_key, std::shared_ptr<FullProMP> promp, double t0, double T,
          unsigned int flags);
      void proMP_controller_factory(const ProMPControllerFactory& factory);
      void set_default_controller(const Controller& default_controller);
      unsigned int num_primitives() const;
      void clear();
      int control_primitive_id(double time);
      
      void control(void* control_signal, const void* curr_state, double time);

      static std::shared_ptr<MovPrimController> create(const std::string& control_type);
    private:
      class Impl;
      std::unique_ptr<Impl> _impl;
  };

  MovPrimController::Controller promp_invdyn_control(std::shared_ptr<FullProMP> promp, double t0, double T);

  void invdyn_gravety_comp(void* control_signal, const void* curr_state, double time);
};

#endif
