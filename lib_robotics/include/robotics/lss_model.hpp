#ifndef ROBOTICS_LSS_MODEL
#define ROBOTICS_LSS_MODEL

#include <armadillo>
#include <memory>
#include <robotics/utils/random.hpp>

namespace robotics {

  class LSSGaussState;

  /**
   * This class models a Generic linear state space model that can be used for any application. Assume
   * that there is a hidden state \f$z_t\f$ an observation \f$x_t\f$, and possibly a action command 
   * \f$u_t\f$ at time t, the model dynamics is given by
   * \f[ z_{t+1} = A_t z_t + B_t u_t + w_t, \f]
   * \f[ x_{t} = C_t z_t + D_t u_t + v_t, \f]
   * where \f$w_t\f$ and \f$v_t\f$ are Gaussian distributed random variables with zero mean and
   * covariances \f$\Gamma\f$ and \f$\Sigma\f$ respectively. We refer to the matrix \f$\Gamma\f$ as the
   * transition covariance matrix and the matrix \f$\Sigma\f$ as the observation noise. For the model
   * dynamics matrices we call A the transition matrix, B the action matrix, C the observation matrix
   * and D the observation bias matrix.
   * @brief Linear state space model dynamics
   * @since version 0.0.1
   */ 
  class AbstractLSSModel {
    public:
      virtual arma::mat transition(const LSSGaussState& state, const void* params) const = 0;
      virtual arma::mat action_mat(const LSSGaussState& state, const void* params) const = 0;
      virtual arma::mat obs_mat(const LSSGaussState& state, const void* params) const = 0;
      virtual arma::mat obs_bias(const LSSGaussState& state, const void* params) const = 0;
      virtual arma::mat obs_noise(const LSSGaussState& state, const void* params) const = 0;
      virtual arma::mat trans_noise(const LSSGaussState& state, const void* params) const = 0;
      virtual unsigned int dim_hidden() const = 0;
      virtual unsigned int dim_obs() const = 0;
      virtual ~AbstractLSSModel() = default;
  };

  /**
   * @brief Linear state space model that considers constant dynamic matrices
   */
  class ConstLSSModel : public AbstractLSSModel {
    public:
      ConstLSSModel(const arma::mat& A, const arma::mat& B, const arma::mat& C, const arma::mat& D,
          const arma::mat& Sigma, const arma::mat& Gamma);
      ConstLSSModel(const ConstLSSModel& b);
      ConstLSSModel(ConstLSSModel&& b);
      ~ConstLSSModel();
      ConstLSSModel& operator=(const ConstLSSModel& b);
      ConstLSSModel& operator=(ConstLSSModel&& b);
      arma::mat transition(const LSSGaussState& state, const void* params) const;
      arma::mat action_mat(const LSSGaussState& state, const void* params) const;
      arma::mat obs_mat(const LSSGaussState& state, const void* params) const;
      arma::mat obs_bias(const LSSGaussState& state, const void* params) const;
      arma::mat obs_noise(const LSSGaussState& state, const void* params) const;
      arma::mat trans_noise(const LSSGaussState& state, const void* params) const;
      unsigned int dim_hidden() const;
      unsigned int dim_obs() const;
    private:
      class Impl;
      std::unique_ptr<Impl> _impl;
  };

  /**
   * State space model representation as a Gaussian distribution. Construct this class with an
   * initial distribution for the state (mean and covariance).
   * Then call observe to incorporate observations to the state distribution or next to obtain
   * a distribution after updating it according to the model dynamics.
   * @brief Gaussian state for state space models
   * @since version 0.0.1
   */ 
  class LSSGaussState {
    public:
      LSSGaussState();
      LSSGaussState(const random::NormalDist& dist);
      LSSGaussState(const arma::vec init_mean, const arma::mat init_cov);
      LSSGaussState(const LSSGaussState& b);
      LSSGaussState(LSSGaussState&& b);
      LSSGaussState& operator=(const LSSGaussState& b);
      LSSGaussState& operator=(LSSGaussState&& b);
      ~LSSGaussState();

      LSSGaussState observe(const AbstractLSSModel& dynamics, const arma::vec& obs, 
          const arma::vec& bias, const void* params=nullptr) const;

      LSSGaussState next(const AbstractLSSModel& dynamics, const arma::vec& action, 
          const void* params=nullptr) const;

      random::NormalDist obs_dist(const AbstractLSSModel& dynamics, const arma::vec& bias, 
          const void* params=nullptr) const;

      double obs_mah_dist(const AbstractLSSModel& dynamics, const arma::vec& obs, 
          const arma::vec& bias, const void* params=nullptr) const;

      const arma::vec& mean() const;
      const arma::mat& cov() const;
      const random::NormalDist& dist() const;
    private:
      class Impl;
      std::unique_ptr<Impl> _impl;
  };

  std::vector<arma::vec> sample_lss_hidden_states(const LSSGaussState& init, const AbstractLSSModel& dyn,
      const std::vector<arma::vec>& actions, const void* params=nullptr);

  std::vector<arma::vec> sample_lss_observations(const std::vector<arma::vec>& hidden_states, 
      const AbstractLSSModel& dyn, const std::vector<arma::vec>& actions, const void* params=nullptr);
};

#endif
