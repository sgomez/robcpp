#ifndef ROB_SLPP_SL_HPP
#define ROB_SLPP_SL_HPP

#include <robotics/controller.hpp>
#include <robotics/slpp/net/net_task.hpp>
#include <memory>
#include <functional>
#include <string>
#include <json.hpp>

namespace robotics {

  namespace slpp {

    /**
     * This object contains the pointers to the functionality required by the low level API
     */
    struct SLObject {
      std::function<void(void* signal, const void* state, double time)> control;
      std::function<void(const double* q, const double* qd, double time)> add_joint_obs;
      std::function<double()> get_time;
      std::function<void(unsigned int sensor_id, double time, const double *x)> add_obs;
      std::function<bool(unsigned int sensor_id, const double* x)> is_obs_new;
    };

    /**
     * Use this object as an owner of the objects required by the low level API for the net task
     */
    struct SLNetTask {
      std::unique_ptr<net::MVCServer> server; //!< Pointer to the network server object
      std::unique_ptr<SLObject> sl_object; //<! Pointer to the low level API functionality
    };

    std::unique_ptr<SLNetTask> load_net_task_json(const nlohmann::json& config);
    std::unique_ptr<SLNetTask> load_net_task_file(const std::string& config_file);

  };

};

#endif
