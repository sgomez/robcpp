#ifndef ROB_SLPP_NET_TAB_TENNIS_TASK_HPP
#define ROB_SLPP_NET_TAB_TENNIS_TASK_HPP

#include <robotics/slpp/net/net_task.hpp>
#include <robotics/controllers/mov_prim.hpp>
#include <robotics/slpp/net/collect_obs_task.hpp>
#include <robotics/slpp/net/mov_prim_task.hpp>
#include <robotics/slpp/net/joint_obs_controller.hpp>
#include <robotics/slpp/net/chrono.hpp>
#include <memory>
#include <vector>

namespace robotics {
  namespace slpp {
    namespace net {

      /**
       * @brief ProMP based task for table tennis playing
       */
      class TableTennisProMP : public MVCController {
        public:
          TableTennisProMP(std::shared_ptr<MovPrimTask> mov_prim, 
              std::shared_ptr<JointObsController> joint_obs, std::shared_ptr<CollectObs> sensor_obs,
              std::shared_ptr<TimeSyncController> time_cntr);
          ~TableTennisProMP();

          json start_task(const json& body);
          json stop_task(const json& body);
          json load_ball_filter(const json& body);
          json load_ball_model(const json& body);
          json load_prior_promp(const json& body);
          json load_barrett_kin(const json& body);
          json get_log(const json& body);
          json set_config(const json& body);
        private:
          class Impl;
          std::unique_ptr<Impl> _impl;
      };

    };
  };
};

#endif
