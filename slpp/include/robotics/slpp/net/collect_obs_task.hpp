#ifndef ROB_SLPP_NET_COLLECT_OBS_TASK_HPP
#define ROB_SLPP_NET_COLLECT_OBS_TASK_HPP

#include <robotics/slpp/net/net_task.hpp>
#include <robotics/utils/chrono.hpp>
#include <robotics/callback.hpp>
#include <string>
#include <memory>

namespace robotics {
  namespace slpp {
    namespace net {

      /**
       * This class implements a MVCController to collect the sensor observations of the robot and
       * distributes it to the net clients that request it.
       */
      class CollectObs : public MVCController {
        public:
          CollectObs();
          CollectObs(std::shared_ptr<AsyncCallback> call_back_handler);
          ~CollectObs();

          static std::shared_ptr<CollectObs> load(const json& sensors, std::shared_ptr<Chronometer> timer);

          void add_sensor(unsigned int id, unsigned int dim, unsigned int buff_size, const std::string& desc);
          void add_obs(unsigned int id, double time, const double* obs);
          bool is_obs_new(unsigned int id, const double* obs);

          json flush_sensor_info(const json& body);
          json clear_sensor_info(const json& body);
          json add_sim_obs(const json& body);

          using obs_listener = std::function<void(unsigned int id, double time, 
              const std::vector<double>& obs)>;
          void add_obs_listener(const std::string& name, const obs_listener& call_back);
          void rm_obs_listener(const std::string& name);
          void set_timer(std::shared_ptr<Chronometer> timer);
          double time() const;
        private:
          class Impl;
          std::unique_ptr<Impl> _impl;
      };
    };
  };
};

#endif
