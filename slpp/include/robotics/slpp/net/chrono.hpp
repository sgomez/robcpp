#ifndef ROB_SLPP_NET_CHRONO_HPP
#define ROB_SLPP_NET_CHRONO_HPP

#include <robotics/slpp/net/net_task.hpp>
#include <robotics/utils/chrono.hpp>
#include <memory>
#include <vector>

namespace robotics {
  namespace slpp {
    namespace net {

      /**
       * This class implements a MVCController for the time synchronization between different components
       */
      class TimeSyncController : public MVCController {
        public:
          TimeSyncController(std::shared_ptr<robotics::Chronometer> chronometer);
          ~TimeSyncController();

          json getTime(const json& body) const;
          std::shared_ptr<robotics::Chronometer> get_chrono() const;
        private:
          class Impl;
          std::unique_ptr<Impl> _impl;
      };
    };
  };
};

#endif
