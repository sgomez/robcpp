#ifndef ROB_SLPP_NET_TASK_HPP
#define ROB_SLPP_NET_TASK_HPP

#include <robotics/table_tennis/env_state.hpp>
#include <robotics/controllers/mov_prim.hpp>
#include <functional>
#include <string>
#include <vector>
#include <memory>
#include <json.hpp>

namespace robotics {
  namespace slpp {
    namespace net {

      /**
       * In a Model-View-Controller arquitecture, this class represents a controller. In general
       * it will receive a method, a body in JSON format and a socket it should process the query
       * (send the answer through the socket) or route the query to other controller.
       */
      class MVCController {
        public:
          using json = nlohmann::json;

          /**
           * Each net controller method receives the body of the query in JSON format and returns
           * an answer in JSON format.
           */
          using method = std::function<json(const json&)>;

          MVCController();
          ~MVCController();
          MVCController(const MVCController& b) = delete;
          MVCController& operator=(const MVCController& b) = delete;

          void register_method(const std::string& name, const method& m);
          json operator()(const std::string& name, const json& body);
        private:
          class Impl;
          std::unique_ptr<Impl> _impl;
      };

      /**
       * This class creates a server in a desired port and forwards the petitions to the main controller.
       * Each petition consists of a JSON object with two fields called method and body. The method is a
       * string containing the URI of the controller method to call and the body is in general another JSON
       * object that is passed as parameter to the controller method.
       */
      class MVCServer {
        public:
          MVCServer(const std::string& host);
          void register_controller(const std::string& name, std::shared_ptr<MVCController> controller);
          ~MVCServer();
        private:
          class Impl;
          std::unique_ptr<Impl> _impl;
      };

    };
  };
};

#endif
