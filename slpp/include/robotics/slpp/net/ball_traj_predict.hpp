#ifndef ROB_SLPP_NET_BALL_TRAJ_PRED_HPP
#define ROB_SLPP_NET_BALL_TRAJ_PRED_HPP

#include <json.hpp>
#include <robotics/utils/random.hpp>
#include <memory>
#include <deque>

namespace robotics {
  namespace slpp {
    namespace net {

      class BallTrajPredict {
        public:
          virtual double traj_llh(const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs) = 0;
          virtual std::vector<std::pair<double,random::NormalDist>> traj_dist(
                const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs,
                const std::vector<double>& pred_times) = 0;
          virtual ~BallTrajPredict() = default;
      };

      /**
       * @brief Loads a generic trajectory prediction model with a given configuration
       */
      std::shared_ptr<BallTrajPredict> load_traj_pred(const nlohmann::json& conf);

      class BallProMP : public BallTrajPredict {
        public:
          BallProMP(const nlohmann::json& conf);
          ~BallProMP();
          double traj_llh(const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs);
          std::vector<std::pair<double,random::NormalDist>> traj_dist(
                const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs,
                const std::vector<double>& pred_times);
        private:
          class Impl;
          std::unique_ptr<Impl> _impl;
      };

      /**
       * @brief Trajectory prediction over a socket
       */
      class RemoteTrajPred : public BallTrajPredict {
        public:
          RemoteTrajPred(const nlohmann::json& conf);
          ~RemoteTrajPred();
          double traj_llh(const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs);
          std::vector<std::pair<double,random::NormalDist>> traj_dist(
                const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs,
                const std::vector<double>& pred_times);
        private:
          class Impl;
          std::unique_ptr<Impl> _impl;
      };

    };
  };
};

#endif
