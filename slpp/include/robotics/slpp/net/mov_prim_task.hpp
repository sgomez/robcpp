#ifndef ROB_SLPP_NET_MOV_PRIM_TASK_HPP
#define ROB_SLPP_NET_MOV_PRIM_TASK_HPP

#include <robotics/slpp/net/net_task.hpp>
#include <robotics/controllers/mov_prim.hpp>
#include <robotics/callback.hpp>
#include <memory>
#include <vector>

namespace robotics {
  namespace slpp {
    namespace net {

      /**
       * This class implements a MVCController for a movement primitive based robot controller.
       */
      class MovPrimTask : public MVCController {
        public:
          MovPrimTask(std::shared_ptr<robotics::MovPrimController> controller);
          ~MovPrimTask();

          json addProMP(const json& body);
          json editProMP(const json& body);
          json removeMP(const json& body);
          json add_go_to(const json& body);
          json add_freeze(const json& body);

          std::shared_ptr<robotics::MovPrimController> robot_controller() const;
        private:
          class Impl;
          std::unique_ptr<Impl> _impl;
      };

    };
  };
};

#endif
