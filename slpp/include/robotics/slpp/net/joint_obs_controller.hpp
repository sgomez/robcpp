#ifndef ROB_SLPP_NET_JOINT_OBS_CTRL_HPP
#define ROB_SLPP_NET_JOINT_OBS_CTRL_HPP

#include <robotics/slpp/net/net_task.hpp>
#include <robotics/callback.hpp>
#include <memory>
#include <vector>

namespace robotics {
  namespace slpp {
    namespace net {

      /**
       * This class implements a MVCController for the joint observations of a robot.
       */
      class JointObsController : public MVCController {
        public:
          JointObsController(unsigned int buffer_size);
          JointObsController(unsigned int buffer_size, std::shared_ptr<AsyncCallback> cb_handler);
          ~JointObsController();

          void addJointMeasure(const std::vector<double>& q, const std::vector<double>& qd, double time);
          json flush(const json& body);
          json clear(const json& body);

          using joint_listener = std::function<void(const std::vector<double>& q, 
              const std::vector<double>& q_dot, double time)>;
          void add_joint_listener(const std::string& name, const joint_listener& call_back);
          void rm_joint_listener(const std::string& name);
        private:
          class Impl;
          std::unique_ptr<Impl> _impl;
      };

      /**
       * @brief A callback object to publish joint observations on a socket
       */
      class JointObsPub {
        public:
          JointObsPub(const nlohmann::json& conf);
          ~JointObsPub();
          void operator()(const std::vector<double>& q, const std::vector<double>& q_dot, double time);
        private:
          class Impl;
          std::shared_ptr<Impl> _impl;
      };
    };
  };
};

#endif
