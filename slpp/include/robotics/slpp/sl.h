#ifndef ROB_SLPP_SL_H
#define ROB_SLPP_SL_H

/**
 * @file 
 * This file contains the basic functionality for a generic task to interact with SL. The create
 * net task function receives a configuration file in JSON format and load a server with a certain
 * number of MVCController objects. It also returns to SL an object that internally contains pointers
 * to the objects that support the functionality of the rest of the API. If the user forgets for instance
 * load the joint controller on the network task and attemps to call rob_slpp_add_joint_obs it will result
 * in undefined behaviour.
 */

#ifdef __cplusplus
extern "C" {  
#endif


  void* rob_slpp_create_net_task(const char* config_file);
  void rob_slpp_delete_net_task(void* task);
  void* rob_slpp_sl_object_net_task(void* task);
  //void rob_slpp_restart_task(void* task);
  /*void rob_slpp_join_task(void* task);*/
  double rob_slpp_get_task_time(const void* sl_obj);
  void rob_slpp_add_joint_obs(void* sl_obj, const double* q, const double* qd, double time);
  void rob_slpp_control(void* sl_obj, void* control_signal, const void* prev_state, double time);
  void rob_slpp_add_sensor_obs(void* sl_obj, unsigned int sensor_id, double time, const double* obs);
  int rob_slpp_is_obs_new(void* sl_obj, unsigned int sensor_id, const double* obs);

  void* rob_slpp_create_inv_dyn_state(unsigned int num_dof);
  void rob_slpp_delete_inv_dyn_state(void* inv_dyn_state);
  void rob_slpp_set_inv_dyn_state(void* inv_dyn_state, const double* q, const double* qd, double time);

  void* rob_slpp_create_inv_dyn_signal(unsigned int num_dof);
  void rob_slpp_delete_inv_dyn_signal(void* inv_dyn_signal);
  void rob_slpp_get_inv_dyn_signal(const void* inv_dyn_signal, double* q, double* qd, double* qdd);

#ifdef __cplusplus  
} // extern "C"  
#endif

#endif
