#ifndef ROB_SLPP_H
#define ROB_SLPP_H

/**
 * @file 
 * Include this file from a C program to access the C API of SL++.
 */

#include <robotics/slpp/sl.h>

#endif
