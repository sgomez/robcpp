#ifndef ROB_SLPP_HPP
#define ROB_SLPP_HPP

/**
 * @file
 * Include this file to access the C++ API of the SL++ library.
 */

#include <robotics/slpp/sl.hpp>
#include <robotics/slpp/net/net_task.hpp>
#include <robotics/slpp/net/chrono.hpp>
#include <robotics/slpp/net/mov_prim_task.hpp>
#include <robotics/slpp/net/collect_obs_task.hpp>

#endif
