
#include <robotics/slpp/sl.hpp>
#include <robotics/slpp/net/mov_prim_task.hpp>
#include <robotics/slpp/net/joint_obs_controller.hpp>
#include <robotics/slpp/net/chrono.hpp>
#include <robotics/slpp/net/collect_obs_task.hpp>
#include <robotics/controllers/inv_dyn.hpp>
#include <robotics/slpp/net/table_tennis.hpp>
#include <robotics/slpp/net/tt_single_promp.hpp>
#include <unordered_map>
#include <string>
#include <fstream>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

using namespace std;
namespace logging = boost::log;

namespace robotics {
  namespace slpp {

    /**
     * Use this method to load a SL network task object from a configuration JSON object
     */
    std::unique_ptr<SLNetTask> load_net_task_json(const nlohmann::json& config) {
      unsigned int num_dof = config.at("num_dof");
      string server_url = config.at("server_url");
      auto& controllers = config.at("controllers");
      unique_ptr<SLNetTask> ans { new SLNetTask };
      ans->server = unique_ptr<net::MVCServer>( new net::MVCServer(server_url) );
      ans->sl_object = unique_ptr<SLObject>( new SLObject );
      unordered_map<string, shared_ptr<net::MVCController>> mvc_ctrl_dict;
      if (config.count("log")) {
        auto log_conf = config.at("log");
        bool auto_flush = false;
        if (log_conf.count("auto_flush")) auto_flush = log_conf.at("auto_flush");
        logging::add_common_attributes();
        logging::register_simple_formatter_factory< boost::log::trivial::severity_level, char >("Severity");
        if (log_conf.count("file")) {
          logging::add_file_log( 
              logging::keywords::file_name = log_conf.at("file").get<string>(), 
              logging::keywords::format = "[%TimeStamp%] (%LineID%) <%Severity%>: %Message%",
              logging::keywords::auto_flush = auto_flush);
        } else {
          logging::add_console_log(std::cout, 
              logging::keywords::format="%Message%",
              logging::keywords::auto_flush = auto_flush);
        }
      } else {
        logging::add_console_log(std::cout, logging::keywords::format="%Message%");
      }
      logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::trace);
      auto cb_handler = make_shared<AsyncCallback>();
      for (auto& controller : controllers) {
        string id = controller.at("id");
        string uri = controller.at("uri");
        if (id == "mov_prim") {
          string type = controller.at("type");
          auto rob_control = MovPrimController::create(type);
          auto mvc_ctrl = make_shared<slpp::net::MovPrimTask>(rob_control);
          ans->server->register_controller(uri, mvc_ctrl);
          ans->sl_object->control = [rob_control](void* signal, const void* state, double time) -> void {
            rob_control->control(signal, state, time);
          };
          mvc_ctrl_dict["mov_prim"] = mvc_ctrl;
        } else if (id == "joints") {
          unsigned int buff_size = controller.at("buff_size");
          auto joint_obs_controller = make_shared<slpp::net::JointObsController>( buff_size, cb_handler );
          ans->server->register_controller(uri, joint_obs_controller);
          ans->sl_object->add_joint_obs = [joint_obs_controller, num_dof](const double* q, 
              const double* qd, double time) -> void {
            vector<double> vq(q, q+num_dof), vqd(qd, qd+num_dof);
            joint_obs_controller->addJointMeasure(vq, vqd, time);
          };
          if (controller.count("pub")) {
            joint_obs_controller->add_joint_listener("pub", net::JointObsPub(controller.at("pub")));
          }
          mvc_ctrl_dict["joints"] = joint_obs_controller;
        } else if (id == "time") {
          auto p_chrono = make_shared<robotics::Chronometer>();
          auto time_controller = make_shared<slpp::net::TimeSyncController>(p_chrono);
          ans->server->register_controller(uri, time_controller);
          ans->sl_object->get_time = [p_chrono]() { return (*p_chrono)(); };
          mvc_ctrl_dict["time"] = time_controller;
        } else if (id == "obs") {
          nlohmann::json sensors = controller.at("sensors");
          if (mvc_ctrl_dict.count("time") == 0) {
            throw std::logic_error("The time controller has to be defined before the "
                "observation controller in the JSON configuration.");
          }
          auto time_controller = static_pointer_cast<net::TimeSyncController>( mvc_ctrl_dict.at("time") );
          auto p_obs = net::CollectObs::load(sensors, time_controller->get_chrono());
          ans->sl_object->add_obs = [p_obs](unsigned int id, double time, const double* x) -> void {
            p_obs->add_obs(id, time, x);
          };
          ans->sl_object->is_obs_new = [p_obs](unsigned int id, const double* x) -> bool {
            return p_obs->is_obs_new(id, x);
          };
          //p_obs->set_timer( time_controller->get_chrono() ); (Already called by load)
          ans->server->register_controller(uri, p_obs);
          mvc_ctrl_dict["obs"] = p_obs;
        } else if (id == "table_tennis_promp") {
          unsigned int pre_req = mvc_ctrl_dict.count("mov_prim") + mvc_ctrl_dict.count("joints") +
            mvc_ctrl_dict.count("time") + mvc_ctrl_dict.count("obs");
          if (pre_req != 4) {
            throw std::logic_error("The table_tennis_promp controller has to be defined in the"
                " configuration JSON after the controllers mov_prim, joints, time and obs.");
          }
          auto promp_task = static_pointer_cast<net::MovPrimTask>( mvc_ctrl_dict.at("mov_prim") );
          auto joint_obs = static_pointer_cast<net::JointObsController>( mvc_ctrl_dict.at("joints") );
          auto time_controller = static_pointer_cast<net::TimeSyncController>( mvc_ctrl_dict.at("time") );
          auto sensor_cntr = static_pointer_cast<net::CollectObs>( mvc_ctrl_dict.at("obs") );
          auto tt_task = make_shared<net::TableTennisProMP>(promp_task, joint_obs, sensor_cntr, time_controller);
          ans->server->register_controller(uri, tt_task);
        } else if (id == "tt_single_promp") {
          unsigned int pre_req = mvc_ctrl_dict.count("mov_prim") + mvc_ctrl_dict.count("joints") +
            mvc_ctrl_dict.count("time") + mvc_ctrl_dict.count("obs");
          if (pre_req != 4) {
            throw std::logic_error("The table_tennis_promp controller has to be defined in the"
                " configuration JSON after the controllers mov_prim, joints, time and obs.");
          }
          auto promp_task = static_pointer_cast<net::MovPrimTask>( mvc_ctrl_dict.at("mov_prim") );
          auto joint_obs = static_pointer_cast<net::JointObsController>( mvc_ctrl_dict.at("joints") );
          auto time_controller = static_pointer_cast<net::TimeSyncController>( mvc_ctrl_dict.at("time") );
          auto sensor_cntr = static_pointer_cast<net::CollectObs>( mvc_ctrl_dict.at("obs") );
          auto tt_task = make_shared<net::TTSingleProMP>(promp_task, joint_obs, sensor_cntr, time_controller);
          ans->server->register_controller(uri, tt_task);
        } else {
          throw std::logic_error("Unrecognized 'id' property on the load_net_task JSON object");
        }
      }
      return ans;
    }

    /**
     * Use this method to load a SL network task object from a configuration file in JSON
     * format.
     */
    std::unique_ptr<SLNetTask> load_net_task_file(const std::string& config_file) {
      ifstream conf(config_file);
      string str((std::istreambuf_iterator<char>(conf)), std::istreambuf_iterator<char>());
      auto json_obj = nlohmann::json::parse(str);
      return load_net_task_json(json_obj);
    }
  };
};

using namespace robotics::slpp;

extern "C" {

  /**
   * Creates a networking task from a configuration file with JSON format.
   */
  void* rob_slpp_create_net_task(const char* config_file) {
    string fname(config_file);
    try {
      auto ptr = load_net_task_file(config_file);
      return ptr.release();
    } catch (const std::exception& ex) {
      BOOST_LOG_TRIVIAL(error) << "An exception ocurred while creating the task from "
        "a configuration file. Message:\n" << ex.what() << endl;
      return nullptr;
    }
  }

  /**
   * Releases the resources of the net task object
   */
  void rob_slpp_delete_net_task(void* task) {
    SLNetTask* net_task = static_cast<SLNetTask*>(task);
    delete net_task;
  }

  /**
   * Returns a SLObject object from a networking SL task
   */
  void* rob_slpp_sl_object_net_task(void* task) {
    SLNetTask* net_task = static_cast<SLNetTask*>(task);
    return net_task->sl_object.get();
  }

  /**
   * Returns the time given the SLObject object
   */
  double rob_slpp_get_task_time(const void* sl_obj) {
    const SLObject* obj = static_cast<const SLObject*>(sl_obj);    
    return obj->get_time();
  }

  /**
   * Adds a joint observation to an SLObject object
   */
  void rob_slpp_add_joint_obs(void* sl_obj, const double* q, const double* qd, double time) {
    SLObject* obj = static_cast<SLObject*>(sl_obj);
    obj->add_joint_obs(q, qd, time);
  }

  /**
   * Returns the control singal in the control_signal variable
   */
  void rob_slpp_control(void* sl_obj, void* control_signal, const void* prev_obs, double time) {
    SLObject* obj = static_cast<SLObject*>(sl_obj);
    obj->control(control_signal, prev_obs, time);
  }

  /**
   * Creates an object to store the inverse dynamics controller current state.
   */
  void* rob_slpp_create_inv_dyn_state(unsigned int num_dof) {
    auto ans = new robotics::InvDynJointObs;
    ans->q = arma::zeros<arma::vec>(num_dof);
    ans->qd = arma::zeros<arma::vec>(num_dof);
    ans->time = 0.0;
    return ans;
  }

  /**
   * Deletes the inverse dynamics state controller object
   */
  void rob_slpp_delete_inv_dyn_state(void* inv_dyn_state) {
    auto tmp = static_cast<robotics::InvDynJointObs*>(inv_dyn_state);
    delete tmp;
  }

  /**
   * Sets the state of the inverse dynamics controller to the desired values
   */
  void rob_slpp_set_inv_dyn_state(void* inv_dyn_state, const double* q, const double* qd, double time) {
    auto tmp = static_cast<robotics::InvDynJointObs*>(inv_dyn_state);
    unsigned int ndof = tmp->q.n_elem;
    std::copy(q, q+ndof, tmp->q.begin());
    std::copy(qd, qd+ndof, tmp->qd.begin());
    tmp->time = time;
  }

  /**
   * Creates an object that stores the inverse dynamics control signal.
   */
  void* rob_slpp_create_inv_dyn_signal(unsigned int num_dof) {
    auto tmp = new robotics::InvDynSignal;
    tmp->q = tmp->qd = tmp->qdd = arma::zeros<arma::vec>(num_dof);
    return tmp;
  }

  /**
   * Deletes an inverse dynamics control signal object
   */
  void rob_slpp_delete_inv_dyn_signal(void* inv_dyn_signal) {
    auto tmp = static_cast<robotics::InvDynSignal*>(inv_dyn_signal);
    delete tmp;
  }

  /**
   * Returns the inverse dynamics control signal in the given output parameter pointers
   */
  void rob_slpp_get_inv_dyn_signal(const void* inv_dyn_signal, double* q, double* qd, double* qdd) {
    auto tmp = static_cast<const robotics::InvDynSignal*>(inv_dyn_signal);
    std::copy(tmp->q.begin(), tmp->q.end(), q);
    std::copy(tmp->qd.begin(), tmp->qd.end(), qd);
    std::copy(tmp->qdd.begin(), tmp->qdd.end(), qdd);
  }

  /**
   * Adds a sensor measurement for a particular sensor identified with sensor_id.
   */
  void rob_slpp_add_sensor_obs(void* sl_obj, unsigned int sensor_id, double time, const double* obs) {
    SLObject* obj = static_cast<SLObject*>(sl_obj);
    obj->add_obs(sensor_id, time, obs);
  }

  /**
   * Returns true if the currently given observation is different from the last given observation
   */
  int rob_slpp_is_obs_new(void* sl_obj, unsigned int sensor_id, const double* obs) {
    SLObject* obj = static_cast<SLObject*>(sl_obj);
    return (int)obj->is_obs_new(sensor_id, obs);
  }

};
