
#include <robotics/slpp/net/net_task.hpp>
#include <unordered_map>
#include <string>
#include <thread>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <boost/log/trivial.hpp>
#include <zmq.h>

using namespace std;
using json = nlohmann::json;

namespace robotics {
  namespace slpp {
    namespace net {

      class MVCController::Impl {
        public:
          unordered_map<string, method> methods;
      };

      MVCController::MVCController() {
        _impl = unique_ptr<Impl>( new Impl );
      }

      MVCController::~MVCController() = default;

      void MVCController::register_method(const std::string& name, const method& m) {
        _impl->methods[name] = m;
      }

      MVCController::json MVCController::operator()(const std::string& name, const json& body) {
        if (_impl->methods.count(name) == 0) {
          ostringstream err;
          err << "Requested method " << name << " not found in the controller";
          return {{"error", err.str()}};
        }
        return _impl->methods[name](body);
      }

      class MVCServer::Impl {
        public:
          bool running;
          unordered_map<string,shared_ptr<MVCController>> router_table;
          
          Impl() {
            running = false;
          }

          void start(const string& uri) {
            thread t(&MVCServer::Impl::run, this, uri);
            t.detach();
          }

          void run(const string& uri) {
#define SOCK_BUFF_SIZE (1 << 20)
            unique_ptr<void, function<int(void*)>> context { zmq_ctx_new(), zmq_ctx_destroy };
            unique_ptr<void, function<int(void*)>> srv { zmq_socket(context.get(), ZMQ_REP), zmq_close };
            if ( zmq_bind(srv.get(), uri.c_str()) != 0 ) {
              throw std::logic_error("The ZMQ socket could not be opened.");
            }
            unique_ptr<char[]> buff{ new char[SOCK_BUFF_SIZE] };
          
            string message;
            running = true;
            while (running) {
              int num_bytes = zmq_recv(srv.get(), buff.get(), SOCK_BUFF_SIZE, 0);
              buff[num_bytes] = '\0';
              message = buff.get();
              string debug_msg = (message.size() > 80) ? message.substr(0,80) : message;
              BOOST_LOG_TRIVIAL(debug) << "Received TCP message: " << debug_msg;
              json query = json::parse(message);
              json answer;
              auto method = query.find("method");
              auto body = query.find("body");
              if (method != query.end() && body != query.end()) {
                vector<string> parsed;
                string s_method = *method;
                json j_body = *body;
                boost::split(parsed, s_method, boost::is_any_of("/"));
                if (parsed.size()==1 && parsed[0]=="quit") {
                  running = false;
                } else if (parsed.size() != 2 || router_table.count(parsed[0])==0) {
                  answer["error"] = "The given method was not found";
                } else {
                  MVCController& controller = *router_table[parsed[0]];
                  answer = controller(parsed[1], j_body);
                }
              } else {
                answer["error"] = "Malformed query. Both method and body properties are expected";
              }
              message = answer.dump();
              debug_msg = (message.size() > 80) ? message.substr(0,80) : message;
              BOOST_LOG_TRIVIAL(debug) << "Answer: " << debug_msg;
              zmq_send(srv.get(), message.c_str(), message.size(), 0);
            }
#undef SOCK_BUFF_SIZE
          }

      };

      MVCServer::MVCServer(const std::string& host) {
        _impl = unique_ptr<Impl>(new Impl);
        _impl->start(host);
      }

      void MVCServer::register_controller(const string& name, shared_ptr<MVCController> controller) {
        _impl->router_table[name] = controller;
      }

      MVCServer::~MVCServer() = default;
    };
  };
};
