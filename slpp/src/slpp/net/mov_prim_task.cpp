
#include <robotics/slpp/net/mov_prim_task.hpp>
#include <robotics/json_factories.hpp>
#include <robotics/utils/promp_utils.hpp>
#include <deque>
#include <mutex>
#include <unordered_map>

using namespace std;

namespace robotics {
  namespace slpp {
    namespace net {

      using json = nlohmann::json;

      class MovPrimTask::Impl {
        public:
          shared_ptr<robotics::MovPrimController> controller;

          Impl(shared_ptr<robotics::MovPrimController> controller) : controller(controller) {
          }

          void register_methods(MovPrimTask& self) {
            auto add_promp = [&self](const json& body) -> json { return self.addProMP(body); };
            auto edit_promp = [&self](const json& body) -> json { return self.editProMP(body); };
            auto rem_mp = [&self](const json& body) -> json { return self.removeMP(body); };
            auto add_go_to = [&self](const json& body) -> json { return self.add_go_to(body); };
            auto add_freeze = [&self](const json& body) -> json { return self.add_freeze(body); };
            self.register_method("addProMP", add_promp);
            self.register_method("editProMP", edit_promp);
            self.register_method("removeMP", rem_mp);
            self.register_method("add_go_to", add_go_to);
            self.register_method("add_freeze", add_freeze);
          }

          json addProMP(const json& body) {
            double t0 = body["t0"];
            double T = body["T"];
            auto promp = make_shared<FullProMP>( robotics::json2full_promp(body["promp"]) );
            unsigned int id;
            if (!body.count("flags")) {
              id = controller->addProMP(promp, t0, T);
            } else {
              unsigned int flags = body.at("flags");
              id = controller->addProMP(promp, t0, T, flags);
            }
            return {"id", id};
          }

          json editProMP(const json& body) {
            unsigned int id = body["id"];
            double t0 = body["t0"];
            double T = body["T"];
            auto promp = make_shared<FullProMP>( robotics::json2full_promp(body["promp"]) );
            unsigned int new_id; 
            if (!body.count("flags")) {
              new_id = controller->editProMP(id, promp, t0, T);
            } else {
              unsigned int flags = body.at("flags");
              new_id = controller->editProMP(id, promp, t0, T, flags);
            }
            return {"id", new_id};
          }

          json removeMP(const json& body) {
            unsigned int id = body["id"];
            controller->removeMP(id);
            //for the moment return the echo
            return {"id", id};
          }

          json add_go_to(const json& body) {
            double t0 = body["t0"];
            double T = body["T"];
            vector<double> qdes = body["qdes"], qddes = body["qddes"];
            auto promp = make_shared<FullProMP>( go_to_promp(arma::vec{qdes}, arma::vec{qddes}, T) );
            unsigned int id = controller->addProMP(promp, t0, T);
            return {{"id", id}};
          }

          json add_freeze(const json& body ) {
            double t0 = body["t0"];
            double T = body["T"];
            unsigned int ndof = body.at("ndof");
            auto promp = make_shared<FullProMP>( freeze_promp(ndof) );
            unsigned int id = controller->addProMP(promp, t0, T, 0x1);
            return {{"id", id}};

          }
      };

      MovPrimTask::MovPrimTask(std::shared_ptr<robotics::MovPrimController> controller) {
        _impl = unique_ptr<Impl>( new Impl(controller) );
        _impl->register_methods( *this );
      }

      MovPrimTask::~MovPrimTask() = default;

      /**
       * Receives a JSON object with properties "promp", "t0", "T" corresponding to a FullProMP object,
       * the initial time of the movement and its duration. The received ProMP is added to the controller
       * and a JSON object with a property "id" (the identifier of the movement) is returned.
       */
      json MovPrimTask::addProMP(const json& body) {
        return _impl->addProMP(body);
      }

      /**
       * This method receives a JSON object with all the same properties of the addProMP method (see the
       * addProMP method of this class) plus the identifier (property "id") of the movement primitive that 
       * is going to be replaced by the recevied ProMP. The method replaces the movement primitive with the
       * given id with the new received ProMP and returns a new identifier for the modified ProMP with the
       * same format as the method addProMP.
       */
      json MovPrimTask::editProMP(const json& body) {
        return _impl->editProMP(body);
      }

      /**
       * This method receives a JSON object with a single property called "id" corresponding to the
       * identifier of the movement primitive that should be erased.
       */
      json MovPrimTask::removeMP(const json& body) {
        return _impl->removeMP(body);
      }

      /**
       * This method receives a JSON object with properties "t0", "T", "qdes", "qddes" and adds a go_to
       * trajectory to the executed task and with the desired parameters. Returns the id of the created
       * movement primitive just like the addProMP method.
       */
      json MovPrimTask::add_go_to(const json& body) {
        return _impl->add_go_to(body);
      }

      /**
       * This method receives a JSON object with properties "t0", "T", "ndof" and adds a freeze
       * trajectory to the executed task and with the desired parameters. Returns the id of the created
       * movement primitive just like the addProMP method.
       */
      json MovPrimTask::add_freeze(const json& body) {
        return _impl->add_freeze(body);
      }

      /**
       * This method can be used to retrieve the same robot controller used by this task, so that
       * it can also be used by other SL++ task.
       * @brief Returns a pointer to the robot controller.
       */
      std::shared_ptr<robotics::MovPrimController> MovPrimTask::robot_controller() const {
        return _impl->controller;
      }
    };
  };
};

