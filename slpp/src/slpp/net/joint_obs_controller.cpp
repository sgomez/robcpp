#include <robotics/slpp/net/joint_obs_controller.hpp>
#include <robotics/json_factories.hpp>
#include <deque>
#include <mutex>
#include <unordered_map>
#include <zmqpp/zmqpp.hpp>
#include <rob_proto/slpp.pb.h>
#include <chrono>

using namespace std;

namespace robotics {
  namespace slpp {
    namespace net {

      using json = nlohmann::json;

      class JointObsController::Impl {
        public:
          using JointObs = tuple<vector<double>,vector<double>,double>;
          deque<JointObs> obs; //<! Collection of observations
          unsigned int buff_size;
          unordered_map<string, JointObsController::joint_listener> listeners;
          mutex lock;
          shared_ptr<AsyncCallback> cb_handler;

          Impl(unsigned int buff_size) {
            this->buff_size = buff_size;
          }

          void register_methods(JointObsController& self) {
            auto flush = [&self](const json& body) -> json { return self.flush(body); };
            auto clear = [&self](const json& body) -> json { return self.clear(body); };
            self.register_method("flush", flush);
            self.register_method("clear", clear);
          }

          void addJointMeasure(const vector<double>& q, const vector<double>& qd, double time) {
            obs.push_back( make_tuple(q, qd, time) );
            if (obs.size() > buff_size) obs.pop_front();
            for (const auto& p : listeners) {
              cb_handler->add_callback([p, q, qd, time]() -> void { p.second(q, qd, time); });
            }
          }

          json json_measures(deque<JointObs>::iterator begin, deque<JointObs>::iterator end) {
            json ans;
            for (auto it=begin; it!=end; it++) {
              json elem;
              elem["q"] = get<0>(*it);
              elem["qd"] = get<1>(*it);
              elem["t"] = get<2>(*it);
              ans.push_back(elem);
            }
            return ans;
          }

          json flush_all() {
            json ans = json_measures(obs.begin(), obs.end());
            obs.clear();
            return ans;
          }
      };
                
      JointObsController::JointObsController(unsigned int buffer_size) {
        _impl = unique_ptr<Impl>( new Impl(buffer_size) );
        _impl->register_methods(*this);
        _impl->cb_handler = make_shared<AsyncCallback>();
      }

      JointObsController::JointObsController(unsigned int buffer_size, shared_ptr<AsyncCallback> cb_handler) {
        _impl = unique_ptr<Impl>( new Impl(buffer_size) );
        _impl->register_methods(*this);
        _impl->cb_handler = cb_handler;
      }


      JointObsController::~JointObsController() = default;

      /**
       * Adds a joint measurement to the internal buffer. These joint measurements can be accessed through
       * the networking task later on.
       */
      void JointObsController::addJointMeasure(const std::vector<double>& q, 
          const std::vector<double>& qd, double time) {
        std::lock_guard<mutex> lock(_impl->lock);
        _impl->addJointMeasure(q, qd, time);
      }
          
      /**
       * Returns all the measurement joint angles and cleans the internal buffers, that means
       * if the method is called again only new joint observations will be returned.
       */
      json JointObsController::flush(const json& body) {
        (void)body;
        std::lock_guard<mutex> lock(_impl->lock);
        return _impl->flush_all();
      }

      /**
       * Cleans the buffer of joint observations and returns an empty json object.
       */
      json JointObsController::clear(const json& body) {
        (void)body;
        std::lock_guard<mutex> lock(_impl->lock);
        _impl->obs.clear();
        return _impl->flush_all();
      }

      /**
       * @brief Register a callback function with some name to be called with every joint measurement
       */
      void JointObsController::add_joint_listener(const std::string& name, const joint_listener& call_back) {
        std::lock_guard<mutex> lock(_impl->lock);
        _impl->listeners[name] = call_back;
      }

      /**
       * @brief Remove a callback function registered with the given name
       */
      void JointObsController::rm_joint_listener(const std::string& name) {
        std::lock_guard<mutex> lock(_impl->lock);
        _impl->listeners.erase(name);
      }

      class JointObsPub::Impl {
        public:
          unique_ptr<zmqpp::context> ctx;
          unique_ptr<zmqpp::socket> q_send;

          void start_server(const string& url) {
            ctx = unique_ptr<zmqpp::context>(new zmqpp::context);
            q_send = unique_ptr<zmqpp::socket>(new zmqpp::socket(*ctx, zmqpp::socket_type::pub));
            q_send->bind( url );
          }

          void send(const std::vector<double>& q, const std::vector<double>& q_dot, double time) {
            auto sys_time = std::chrono::system_clock::now();
            rob_proto::JointObs jobs;
            jobs.set_slpp_time(time);
            jobs.set_abs_time(sys_time.time_since_epoch().count());
            for (auto x: q) jobs.add_q(x);
            for (auto x: q_dot) jobs.add_qd(x);

            string ans = jobs.SerializeAsString();
            zmqpp::message msg;
            msg << ans;
            q_send->send(msg);
          }

          Impl(const json& conf) {
            start_server(conf);
          }
      };

      JointObsPub::JointObsPub(const nlohmann::json& conf) {
        _impl = unique_ptr<Impl>( new Impl(conf) );
      }

      JointObsPub::~JointObsPub() = default;
          
      void JointObsPub::operator()(const std::vector<double>& q, const std::vector<double>& q_dot, double time) {
        _impl->send(q, q_dot, time);
      }
    };
  };
};
