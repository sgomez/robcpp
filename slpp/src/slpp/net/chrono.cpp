
#include <robotics/slpp/net/chrono.hpp>
#include <json.hpp>

using namespace std;

namespace robotics {
  namespace slpp {
    namespace net {
      using json = nlohmann::json;

      class TimeSyncController::Impl {
        public:
          shared_ptr<robotics::Chronometer> chrono;

          void register_methods(TimeSyncController& self) {
            self.register_method("get_time", [&self](const json& body) { return self.getTime(body); });
          } 
      };

      TimeSyncController::TimeSyncController(std::shared_ptr<robotics::Chronometer> chronometer) {
        _impl = unique_ptr<Impl>( new Impl{chronometer} );
        _impl->register_methods(*this);
      }
      
      TimeSyncController::~TimeSyncController() = default;

      json TimeSyncController::getTime(const json& body) const {
        (void)body;
        json ans;
        ans["time"] = (*_impl->chrono)();
        return ans;
      }

      std::shared_ptr<robotics::Chronometer> TimeSyncController::get_chrono() const {
        return _impl->chrono;
      }

    }
  }
};
