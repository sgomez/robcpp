#include <robotics/slpp/net/ball_traj_predict.hpp>
#include <robotics/table_tennis.hpp>
#include <robotics.hpp>
#include <deque>
#include <mutex>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <armadillo>

#include <zmqpp/zmqpp.hpp>
#include <rob_proto/slpp.pb.h>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>

using namespace std;
using namespace arma;
namespace logging = boost::log;

namespace robotics {

  using namespace table_tennis;

  namespace slpp {
    namespace net {
      using json = nlohmann::json;

      class BallProMP::Impl {
        public:
          shared_ptr<FullProMP> ball_promp;
          double duration = 1.0;
          double min_z;

          void set_conf(const json& conf) {
            ball_promp = make_shared<FullProMP>(json2full_promp(conf.at("promp")));
            duration = conf.at("duration");
            min_z = conf.at("min_z");
          }

          deque<pair<double,vector<double>>> filter_obs(const deque<pair<double,vector<double>>>& obs) {
            deque<pair<double,vector<double>>> ans;
            for (unsigned int i=0; i<obs.size(); i++) {
              if (obs[i].second[2] > min_z) {
                ans.push_back(obs[i]);
              }
            }
            return ans;
          }

          vec get_z(const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs) {
            unsigned int N = prev_ball_obs.size();
            vec z(N);
            double t0 = prev_ball_obs[0].first;
            for (unsigned int i=0; i<N; i++) {
              z[i] = (prev_ball_obs[i].first - t0) / duration;
            }
            return z;
          }

          mat get_obs(const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs) {
            unsigned int N = prev_ball_obs.size();
            mat obs(3,N);
            for (unsigned int i=0; i<N; i++) {
              obs.col(i) = conv_to<vec>::from(prev_ball_obs[i].second);
            }
            return obs;
          }

          double traj_llh(const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs) {
            vec z = get_z(prev_ball_obs);
            mat obs = get_obs(prev_ball_obs);
            return ball_promp->log_lh(z, obs);
          }

          std::vector<std::pair<double,random::NormalDist>> traj_dist(
              const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs,
              const std::vector<double>& pred_times) {
            //double T = pred_times.back() - pred_times.front();
            double t0 = prev_ball_obs[0].first;
            //duration = T;
            vec z = get_z(prev_ball_obs);
            mat obs = get_obs(prev_ball_obs);
            FullProMP nmodel = ball_promp->condition_multiple_obs(z, obs);
            std::vector<std::pair<double,random::NormalDist>> ans;
            for (auto t : pred_times) {
              double curr_z = (t-t0)/duration;
              auto ball_dist = nmodel.joint_dist(curr_z, true, false, false);
	      if (curr_z < 0.0 || curr_z > 1.0) ball_dist.set_cov(arma::eye<mat>(3,3));
              ans.push_back(make_pair(t, ball_dist));
            }
            return ans;
          }
      };

      BallProMP::BallProMP(const nlohmann::json& conf) {
        _impl = unique_ptr<Impl>(new Impl);
        _impl->set_conf(conf);
      }

      BallProMP::~BallProMP() = default;

      double BallProMP::traj_llh(const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs) {
        return _impl->traj_llh(prev_ball_obs);
      }

      std::vector<std::pair<double,random::NormalDist>> BallProMP::traj_dist(
              const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs,
              const std::vector<double>& pred_times) {
        return _impl->traj_dist(prev_ball_obs, pred_times);
      }

      class RemoteTrajPred::Impl {
        public:
          unique_ptr<zmqpp::context> context;
          unique_ptr<zmqpp::socket> socket;
          double noise_y=0.0;

          void start(const string& url) {
            context = unique_ptr<zmqpp::context>(new zmqpp::context());
            socket = unique_ptr<zmqpp::socket>(new zmqpp::socket(*context, 
                  zmqpp::socket_type::req));
            socket->connect(url);
          }

          std::vector<std::pair<double,random::NormalDist>> traj_dist(
              const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs,
              const std::vector<double>& pred_times) {
            using namespace rob_proto;
            BallPredictRequest req; 
            Tensor& t_prev_times = *(req.mutable_prev_times());
            Tensor& t_prev_obs = *(req.mutable_prev_obs());
            Tensor& t_pred_times = *(req.mutable_pred_times());

            t_prev_times.add_shape(prev_ball_obs.size());
            t_prev_obs.add_shape(prev_ball_obs.size());
            t_prev_obs.add_shape(3);
            for (const auto& p : prev_ball_obs) {
              t_prev_times.add_data(p.first);
              for (unsigned int j=0; j<3; j++) {
                t_prev_obs.add_data(p.second[j]);
              }
            }

            t_pred_times.add_shape(pred_times.size());
            for (double t : pred_times) {
              t_pred_times.add_data(t);
            }

            zmqpp::message msg;
	    string smsg = req.SerializeAsString();
            msg << smsg;
            BOOST_LOG_TRIVIAL(trace) << "Sending ball trajectory to ball prediction server";
            socket->send(msg);
            socket->receive(msg);
            BOOST_LOG_TRIVIAL(trace) << "Answer received from the ball prediction server";
            msg >> smsg;

            BallPredictResponse res;
            res.ParseFromString(smsg);
	    BOOST_LOG_TRIVIAL(trace) << "Ball prediction response parsed";
            unsigned int res_size = pred_times.size();
            //check tensors have the following shapes means -> (N,D). covs -> (N,D,D)
            if (res.means().shape_size() != 2 || res.covs().shape_size() != 3 || 
                res.means().shape(0) != res_size || res.covs().shape(0) != res_size) {
              BOOST_LOG_TRIVIAL(error) << "The predicted trajectory tensors do not have the required size.";
              throw std::logic_error("The predicted trajectory tensors do not have the required size");
            }
            unsigned int D = res.means().shape(1);
            if (res.covs().shape(1) != D || res.covs().shape(2) != D) {
              BOOST_LOG_TRIVIAL(error) << "Shape error in RemoteTrajPred. mean of " << D << 
                " dimensions, but covariance of (" << res.covs().shape(1) << 
                "," << res.covs().shape(2) << ") dimensions";
              throw std::logic_error("Shape error in RemoteTrajPred");
            }

            vector<pair<double,random::NormalDist>> ans;
	    BOOST_LOG_TRIVIAL(trace) << "Decoding response of "  << res_size << 
		    " time steps and " <<  D << " dimensions";
            mat Sigma_y(D,D,fill::eye);
            Sigma_y *= noise_y*noise_y;
            for (unsigned int n=0; n<res_size; n++) {
              vec mean(D);
              mat cov(D,D);

              std::copy(res.means().data().begin() + n*D, 
                  res.means().data().begin() + (n+1)*D, 
                  mean.begin());
              std::copy(res.covs().data().begin() + n*D*D,
                  res.covs().data().begin() + (n+1)*D*D,
                  cov.begin());

              /*for (unsigned int i=0; i<D; i++) {
                mean[i] = res.means().data(n*D + i);
                for (unsigned int j=0; j<D; j++) {
                  cov(i,j) = res.covs().data(n*D*D + D*i + j);
                }
              }*/
              ans.push_back(std::make_pair(pred_times[n], 
                    random::NormalDist{mean, cov + Sigma_y}));
            }
            BOOST_LOG_TRIVIAL(trace) << "Predicted ball distribution decoded successfully";

            return ans;
          }

      };

      RemoteTrajPred::RemoteTrajPred(const nlohmann::json& conf) {
        _impl = unique_ptr<Impl>(new Impl);
        _impl->start(conf.at("url"));
        if (conf.count("noise")) _impl->noise_y = conf.at("noise");
      }

      RemoteTrajPred::~RemoteTrajPred() = default;

      std::vector<std::pair<double,random::NormalDist>> RemoteTrajPred::traj_dist(
          const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs,
          const std::vector<double>& pred_times) {
        return _impl->traj_dist(prev_ball_obs, pred_times);
      }

      double RemoteTrajPred::traj_llh(const std::deque<std::pair<double,std::vector<double>>>& prev_ball_obs) {
        // Note: Not yet implemented
        (void)prev_ball_obs;
        return 0.0;
      }

      std::shared_ptr<BallTrajPredict> load_traj_pred(const nlohmann::json& conf) {
        shared_ptr<BallTrajPredict> ans;
        BOOST_LOG_TRIVIAL(trace) << "Loading a ball model";
        if (conf.count("model") == 0) {
	  //Default behaviour: when only the ball promp model was supported
          ans = make_shared<BallProMP>(conf);
	} else {
	  string model_type = conf.at("model");
	  if (model_type == "net") ans = make_shared<RemoteTrajPred>(conf);
	  else if (model_type == "promp") ans = make_shared<BallProMP>(conf);
	  else {
            BOOST_LOG_TRIVIAL(error) << "Unrecognized ball model type " << model_type;
	    throw std::logic_error("Error loading the ball model");
	  }
	}
        BOOST_LOG_TRIVIAL(trace) << "Ball model successfully loaded";
        return ans;
      }

    };
  };
};
