
#include <robotics/slpp/net/collect_obs_task.hpp>
#include <deque>
#include <unordered_map>
#include <vector>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <zmq.h>

#include <boost/log/trivial.hpp>

using namespace std;

namespace robotics {
  namespace slpp {
    namespace net {

      /**
       * @brief Represents a sensor on the CollectObs class
       */
      struct Sensor {
        unsigned int dims; //<! Dimension of the observation vector of the sensor
        string desc; //<! A string describing the sensor
        unsigned int buff_size; //<! The observation buffer size for the current vector
      };

      /**
       * @brief Represents a sensor observation
       */
      struct Obs {
        double time; //<! Time of the observation
        vector<double> obs; //<! Observation vector
      };

      /**
       * @brief Represents a simulated observation
       */
      struct SimObs {
        unsigned int sensor_id; //!< Id of the sensor that produced the observation
        Obs obs; //!< The observation itself
      };

      bool operator>(const SimObs& a, const SimObs& b) {
        return a.obs.time > b.obs.time;
      }

      class CollectObs::Impl {
        public:
          unordered_map<unsigned int, Sensor> sensors;
          unordered_map<unsigned int, deque<Obs>> observations;
          unordered_map<string, CollectObs::obs_listener> obs_listeners;
          priority_queue<SimObs, vector<SimObs>, greater<SimObs>> sim_obs;
          std::shared_ptr<Chronometer> p_timer;
          std::mutex obj_mutex, sim_mutex;
          std::condition_variable sim_cv;
          std::thread sim_thread;
          shared_ptr<AsyncCallback> cb;

          void _add_obs(unsigned int id, double time, vector<double>&& v_obs) {
            //First call all the callbacks
            double t1 = (*p_timer)();
            for (const auto& p : obs_listeners) {
              cb->add_callback([p, id, time, v_obs]() -> void { p.second(id, time, v_obs); });
            }
            double t2 = (*p_timer)();
            if ((t2 - t1)>0.002) {
              BOOST_LOG_TRIVIAL(warning) << "Calling the ball observation callbacks took longer than expected: " 
                << (t2-t1) << "s";
            }

            //Then use std::move to add the current observation
            Obs obs{time, v_obs};
            observations[id].push_back(std::move(obs));
            if (observations[id].size() > sensors[id].buff_size) {
              observations[id].pop_front();
            }
          }

          bool _is_obs_new(unsigned int id, const double *x) {
            if (observations[id].empty()) return true;
            const Obs& last = observations[id].back();
            for (unsigned int i=0; i<last.obs.size(); i++) {
              if ( fabs(last.obs[i] - x[i]) > 1e-3 ) return true;
            }
            return false;
          }

          unordered_map<unsigned int, deque<Obs>> flush_sensor_info(const vector<unsigned int>& sensor_ids) {
            unique_lock<mutex> lock(obj_mutex);
            unordered_map<unsigned int, deque<Obs>> ans;
            if (sensor_ids.size() == 0) swap(ans, observations);
            else {
              for (unsigned int id : sensor_ids) {
                swap(ans[id], observations[id]);
              }
            }
            return ans;
          }

          void clear_sensor_info(const vector<unsigned int>& ids) {
            unique_lock<mutex> lock(obj_mutex);
            if (ids.size() == 0) observations.clear();
            else {
              for (auto id : ids) {
                observations[id].clear();
              }
            }
          }

          /**
           * This method is thread safe even if it does not use the lock, because instead
           * of directly accessing the object properties it access the values that it needs
           * from the overloaded version flush_sensor_info method that does use a mutex lock.
           */
          json jflush_sensor_info(const json& query) {
            vector<unsigned int> ids; 
            if (query.count("ids") == 1) {
              ids = query["ids"].get<vector<unsigned int>>();
            }
            auto flushed = flush_sensor_info(ids);
            json ans;
            for (auto p : flushed) {
              json sensor;
              sensor["id"] = p.first;
              for (auto elem : p.second) {
                json value;
                value["time"] = elem.time;
                value["obs"] = elem.obs;
                sensor["values"].push_back(value);
              }
              ans.push_back(sensor);
            }
            return ans;
          }

          json jclear_sensor_info(const json& query) {
            vector<unsigned int> ids;
            if (query.count("ids") != 0) {
              ids = query["ids"].get<vector<unsigned int>>();
            }
            clear_sensor_info(ids);
            json ans;
            return ans;
          }

          void run_sim_obs() {
            const Chronometer& timer = *p_timer;
            while (true) {
              //1) Wait until there is a simulated observation to add
              std::unique_lock<mutex> lock(sim_mutex);
              while (sim_obs.empty()) sim_cv.wait(lock);
              SimObs tmp = sim_obs.top(); sim_obs.pop();
              lock.unlock();
              //2) Wait until it is valid
              std::chrono::duration<double> dt {tmp.obs.time - timer()};
              if (dt.count() > 0) std::this_thread::sleep_for(dt);
              //3) Add the observation
              std::unique_lock<mutex> obj_lock(obj_mutex);
              _add_obs(tmp.sensor_id, tmp.obs.time, std::move(tmp.obs.obs));
              obj_lock.unlock();
            }
          }

          json jadd_sim_obs(const json& query) {
            std::unique_lock<mutex> lock(sim_mutex);
            double curr_time = (*p_timer)();
            for (auto obs : query) {
              unsigned int sensor_id = obs.at("sensor_id");
              double obs_time = obs.at("time");
              vector<double> pos = obs.at("obs");
              SimObs tmp {sensor_id, {obs_time + curr_time, pos}};
              sim_obs.push( std::move(tmp) );
            }
            //If simulation thread has not started, start it now
            if (!sim_thread.joinable()) {
              sim_thread = thread{ [this]() { this->run_sim_obs(); } };
            } else {
              sim_cv.notify_all();
            }
            json ans;
            return ans;
          }

          void register_methods(CollectObs& self) {
            auto flush_sensor_info = [&self](const json& body) -> json { return self.flush_sensor_info(body); };
            auto clear_sensor_info = [&self](const json& body) -> json { return self.clear_sensor_info(body); };
            auto add_sim_obs = [&self](const json& body) -> json { return self.add_sim_obs(body); };
            self.register_method("flush", flush_sensor_info);
            self.register_method("clear", clear_sensor_info);
            self.register_method("add_sim_obs", add_sim_obs);
          }
      };

      /**
       * @brief Constructor of the observations buffer controller
       */
      CollectObs::CollectObs() {
        _impl = unique_ptr<Impl>( new Impl );
        _impl->cb = make_shared<AsyncCallback>();
        _impl->register_methods(*this);
      }

      /**
       * @brief Constructs the observation buffer controller with passed callback handler
       */
      CollectObs::CollectObs(shared_ptr<AsyncCallback> handler) {
        _impl = unique_ptr<Impl>(new Impl);
        _impl->cb = handler;
        _impl->register_methods(*this);
      }

      /**
       * @brief Destructor for the observation buffer controller
       */
      CollectObs::~CollectObs() = default;

      void subscribe_and_listen(shared_ptr<CollectObs> obs_ctrl, const string& resource,
          unsigned int sensor_id, bool sl_time) {
        int time_sync_warn = 0;
        BOOST_LOG_TRIVIAL(info) << "Connecting to vision server at " << resource << 
          ". sl_time: " << sl_time;
        unique_ptr<void, function<int(void*)>> context { zmq_ctx_new(), zmq_ctx_destroy };
        unique_ptr<void, function<int(void*)>> sck { zmq_socket(context.get(), ZMQ_SUB), zmq_close };
        if ( zmq_connect(sck.get(), resource.c_str()) != 0 ) {
          BOOST_LOG_TRIVIAL(error) << "Error connecting a socket to vision server " << resource;
          throw std::logic_error("The ZMQ socket could not be opened.");
        }
        if (zmq_setsockopt(sck.get(), ZMQ_SUBSCRIBE, "", 0) != 0) {
          BOOST_LOG_TRIVIAL(error) << "Error setting the socket options for subscribing to the vision server";
          throw std::logic_error("Error setting the ZMQ socket options");
        }
        zmq_msg_t msg;
        zmq_msg_init(&msg);
        BOOST_LOG_TRIVIAL(debug) << "Listening for ball observations from vision server...";
        while (true) {
          zmq_msg_recv(&msg, sck.get(), 0);
          unsigned int len = zmq_msg_size(&msg);
          const char* data = (const char*)zmq_msg_data(&msg);
          string body(data, len);
          BOOST_LOG_TRIVIAL(debug) << "Vision server message [len=" << len <<", body=" << body << "]";
          auto jbody = nlohmann::json::parse(body);
          auto jobs = jbody.at("obs");
          if (jobs.is_array()) {
            double obs[jobs.size()];
            std::copy(jobs.begin(), jobs.end(), obs);
            double time;
            if (sl_time) time = obs_ctrl->time();
            else {
              time = jbody.at("time");
              time = time / 1.0e9; //Put it in seconds
              if (time_sync_warn<10 && fabs(obs_ctrl->time() - time) > 0.2) {
                BOOST_LOG_TRIVIAL(warning) << "The time synchronization between the vision and robot is off by " << fabs(obs_ctrl->time() - time) << " seconds";
                time_sync_warn++;
              }
            }
            obs_ctrl->add_obs(sensor_id, time, obs);
          }
        }
        zmq_msg_close(&msg);
      }

      /**
       * @brief Loads a observation buffer controller from the sensor configuration given in JSON format
       */
      std::shared_ptr<CollectObs> CollectObs::load(const json& sensors, shared_ptr<Chronometer> timer) {
        shared_ptr<CollectObs> ans{ new CollectObs };
        ans->set_timer(timer);
        for (auto sensor : sensors) {
          unsigned int id = sensor.at("id"), dim = sensor.at("dim"), buff_size = sensor.at("buff_size");
          string desc = sensor.at("desc");
          ans->add_sensor(id, dim, buff_size, desc);
          if (sensor.count("remote")) {
            bool sl_time = false;
            if (sensor.count("sl_time")) sl_time = sensor.at("sl_time");
            BOOST_LOG_TRIVIAL(info) << "Using sl_time: " << sl_time;
            thread t{subscribe_and_listen, ans, sensor.at("remote"), id, sl_time};
            t.detach();
          }
        }
        return ans;
      }

      /**
       * @brief Adds a sensor to the controller
       * @param[in] id The identifier of the sensor
       * @param[in] dim The dimensionality of the observations returned by this sensor
       * @param[in] buff_size The maximum buffer size for the observations before they are overriden
       */
      void CollectObs::add_sensor(unsigned int id, unsigned int dim, 
          unsigned int buff_size, const string& desc) {
        unique_lock<mutex> lock(_impl->obj_mutex);
        Sensor s {dim, desc, buff_size};
        _impl->sensors[id] = std::move(s);
      }

      /**
       * @brief Adds an observation to the a particular sensor identified with id
       */
      void CollectObs::add_obs(unsigned int id, double time, const double* obs) {
        vector<double> v_obs(obs, obs+_impl->sensors[id].dims);
        unique_lock<mutex> lock(_impl->obj_mutex);
        _impl->_add_obs(id, time, std::move(v_obs));
      }

      /**
       * @brief Adds an observation to the a particular sensor identified with id
       */
      bool CollectObs::is_obs_new(unsigned int id, const double* obs) {
        unique_lock<mutex> lock(_impl->obj_mutex);
        return _impl->_is_obs_new(id, obs);
      }

      /**
       * Receives a JSON object with the ids of the sensors that the client wants to read and returns
       * all measurements on those sensors to the client. It also cleans those sensor buffers. The
       * JSON object contains a property called "ids" whose value should be a non-empty list of integers
       * containing valid sensor ids. The returned answer contains an array with length equal to the 
       * number of sensors, each element contains a JSON object with properties "id" (The sensor id) and
       * "values" (the measured sensor values). Each of the measured sensor values is an object with
       * properties "time" and "obs", the time of the observation and the measurement respectively.
       * @brief Returns the collected sensor info to the caller in JSON format
       */
      CollectObs::json CollectObs::flush_sensor_info(const json& body) {
        return _impl->jflush_sensor_info(body);
      }

      /**
       * Receives a JSON object with the ids of the sensors that the client wants to clear, if no "ids"
       * property appears in the received JSON object then all the sensor information is cleared.
       * @brief On success return an empty JSON object
       */
      CollectObs::json CollectObs::clear_sensor_info(const json& body) {
        return _impl->jclear_sensor_info(body);
      }

      double CollectObs::time() const {
        return (*(_impl->p_timer))();
      } 

      /**
       * @brief Add simulated observations to be treated as real observations
       */
      CollectObs::json CollectObs::add_sim_obs(const json& body) {
        return _impl->jadd_sim_obs(body);
      }


      /**
       * @brief Registers an observation listener with the given name
       */
      void CollectObs::add_obs_listener(const std::string& name, const CollectObs::obs_listener& call_back) {
        unique_lock<mutex> lock(_impl->obj_mutex);
        _impl->obs_listeners[name] = call_back;
      }
      
      /**
       * @brief Unregisters an observation listener with the given name
       */
      void CollectObs::rm_obs_listener(const std::string& name) {
        unique_lock<mutex> lock(_impl->obj_mutex);
        _impl->obs_listeners.erase( name );
      }

      /**
       * @brief Adds a timer to the object
       */
      void CollectObs::set_timer(std::shared_ptr<Chronometer> timer) {
        _impl->p_timer = timer;
      }

    };
  };
};
