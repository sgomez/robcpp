#include <robotics/slpp/net/table_tennis.hpp>
#include <robotics/table_tennis.hpp>
#include <robotics.hpp>
#include <deque>
#include <mutex>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <condition_variable>


#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>

using namespace std;
namespace logging = boost::log;

namespace robotics {

  using namespace table_tennis;

  namespace slpp {
    namespace net {
      using json = nlohmann::json;

      class TableTennisProMP::Impl {
        public:
          /***** Definitions *****/
          struct Params {
            double deltaT = 1.0/60.0; //!< Average time between ball obs
            double T = 0.5; //!< Total duration of the ProMP
            double hit_mean = 0.5; //!< Hit prior mean
            double hit_std = 0.1; //!< Hit prior standard deviation
            //! Threshold on hit log-likelihood to consider a trajectory dangerous
            double min_init_hit_loglh = -10;
            //! Should the robot try to move to initial state of conditioned ProMP?
            bool go_to_cond_promp = false;
            //! Should the approach use additional ball observations to re-plan before moving
            bool use_new_ball_obs = true;
            //! Total time length for the predicted ball trajectory
            double time_pred_ball_traj = 2.0;
            //! Time before planned movement considered safe to recompute t0
            double safe_t0_recomp = 0.3;
            //! Minimal allowed reaction time after t0 is computed
            double min_react_time = 0.1;
            //! Minimal time before predicted hitting so re-planning is done.
            //! If larger than T, no re-planning is done.
            double replan_safe_time = 1.0;
            //! Which ball model to use
            string which_ball_model = "ball_model";
          };

          /***** State Variables *****/
          std::shared_ptr<MovPrimTask> mov_prim;
          std::shared_ptr<JointObsController> joint_obs;
          std::shared_ptr<CollectObs> sensor_obs;
          std::shared_ptr<TimeSyncController> time_cntr;
          std::shared_ptr<table_tennis::OutlierBallFilter> outlier_filter, conf_outlier_filter;
          std::shared_ptr<BallModel> ball_model, conf_ball_model;
          std::shared_ptr<FullProMP> prior_promp, conf_prior_promp;
          std::shared_ptr<FwdKin> fwd_kin, conf_fwd_kin;
          std::shared_ptr<FullProMP> ball_promp, conf_ball_promp;
          Params params, conf_params;
          bool running;
          std::mutex ball_mtx, joint_mtx, task_mtx, log_mtx, conf_mtx;
          std::condition_variable ball_obs;
          json trial_log;
          bool log_ready = true;
          vector<double> last_q, last_qd;
          double last_joint_time;
          vector<pair<double,LSSGaussState>> prev_ball_states;

          /***** Methods *****/

          void register_listeners() {
            auto joint_listener = [this](const std::vector<double>& q, const std::vector<double>& q_dot, 
                double time) {
              std::lock_guard<std::mutex> lck(this->joint_mtx);
              this->last_q = q;
              this->last_qd = q_dot;
              this->last_joint_time = time;
            };
            auto sensor_listener = [this](unsigned int id, double time, const std::vector<double>& obs) {
              std::lock_guard<std::mutex> lck(this->ball_mtx);
              if (id>2) return;
              BOOST_LOG_TRIVIAL(trace) << "Receiving observation from sensor " << id;
              this->outlier_filter->add_obs(id, time, obs.data());
              if (this->outlier_filter->ready()) 
                prev_ball_states.push_back( make_pair(time,this->outlier_filter->state()) );
              this->ball_obs.notify_all();
            };
            joint_obs->add_joint_listener("promp_tt", joint_listener);
            sensor_obs->add_obs_listener("promp_tt", sensor_listener);
          }

          void unregister_listeners() {
            joint_obs->rm_joint_listener("promp_tt");
            sensor_obs->rm_obs_listener("promp_tt");
          }

          json set_config(const json& conf) {
            lock_guard<mutex> conf_lock(conf_mtx);
            json ans;
            if (conf.count("deltaT")) conf_params.deltaT = conf.at("deltaT");
            if (conf.count("T")) conf_params.T = conf.at("T");
            if (conf.count("hit_mean")) conf_params.hit_mean = conf.at("hit_mean");
            if (conf.count("hit_std")) conf_params.hit_std = conf.at("hit_std");
            if (conf.count("min_init_hit_loglh"))
              conf_params.min_init_hit_loglh = conf.at("min_init_hit_loglh");
            if (conf.count("go_to_cond_promp"))
              conf_params.go_to_cond_promp = conf.at("go_to_cond_promp");
            if (conf.count("use_new_ball_obs"))
              conf_params.use_new_ball_obs = conf.at("use_new_ball_obs");
            if (conf.count("time_pred_ball_traj"))
              conf_params.time_pred_ball_traj = conf.at("time_pred_ball_traj");
            if (conf.count("safe_t0_recomp"))
              conf_params.safe_t0_recomp = conf.at("safe_t0_recomp");
            if (conf.count("min_react_time"))
              conf_params.min_react_time = conf.at("min_react_time");
            if (conf.count("replan_safe_time"))
              conf_params.replan_safe_time = conf.at("replan_safe_time");
            return ans;
          }

          void register_methods(TableTennisProMP& self) {
            self.register_method("start", [&self](const json& body) { return self.start_task(body); });
            self.register_method("stop", [&self](const json& body) { return self.stop_task(body); });
            self.register_method("load_ball_model", [&self](const json& body) { return self.load_ball_model(body); });
            self.register_method("load_ball_filter", [&self](const json& body) { return self.load_ball_filter(body); });
            self.register_method("load_barrett_kin", [&self](const json& body) { return self.load_barrett_kin(body); });
            self.register_method("load_prior_promp", [&self](const json& body) { return self.load_prior_promp(body); });
            self.register_method("get_log", [&self](const json& body) { return self.get_log(body); });
            self.register_method("set_config", [&self](const json& body) { return self.set_config(body); });
          }

          vector<pair<double,random::NormalDist>> predict_ball_traj(unsigned int time_steps, json& trial_log) {
            vector<pair<double,random::NormalDist>> ans;
            json times, means, covs;
            if (params.which_ball_model == "ball_promp") {

            } else {
              for (const auto& prev_st : this->prev_ball_states) {
                const auto& pos = ball_model->position_dist(prev_st.second);
                double t = prev_st.first;
                ans.push_back( make_pair(t, pos) );
                times.push_back(t);
                means.push_back(pos.mean());
                covs.push_back(pos.cov());
              }
              auto state = outlier_filter->state();
              double t = outlier_filter->last_time();
              while (ans.size() < time_steps) {
                state = ball_model->advance(state, params.deltaT);
                t += params.deltaT;
                const auto& pos = ball_model->position_dist(state);
                ans.push_back( make_pair(t, pos) );
                times.push_back(t);
                means.push_back(pos.mean());
                covs.push_back(pos.cov());
              }
            }
            trial_log["predict_ball_traj"] = { {"times", times}, {"means", means}, {"covs", covs} };
            return ans;
          }

          unsigned int find_cond_ix(const vector<pair<double,random::NormalDist>>& ball_traj, double pred_thit) {
            unsigned int a=0, b=ball_traj.size();
            while (a < b) {
              unsigned int m = (a + b) / 2;
              if (pred_thit > ball_traj[m].first) a = m+1;
              else b = m;
            }
            return b;
          }

          std::function<random::NormalDist(double z)> get_racket_dist(std::shared_ptr<FullProMP> promp) {
            auto fwd_kin = this->fwd_kin;
            auto ans = [promp, fwd_kin](double z) -> random::NormalDist {
              return prob_fwd_kin_pos(*fwd_kin, promp->joint_dist(z)); 
            };
            return ans;
          }

          /**
           * @brief Clean up the state between different trials
           */
          void trial_cleanup() {
            //1) Make sure ball state is reset for next incoming ball
            outlier_filter->reset();
            prev_ball_states.clear();
          }

          /**
           * @brief Adds/Modifies a ProMP with safety considerations. Returns true if the ProMP is added.
           */
          bool safe_add_promp(int& id, std::shared_ptr<FullProMP> promp, double t0, 
              double T) {
            //1) Check that all safety regulations are met
            //2) Add the ProMP
            if (id == -1) {
              id = mov_prim->robot_controller()->addProMP(promp, t0, T);
            } else {
              id = mov_prim->robot_controller()->editProMP((unsigned int)id, promp, t0, T);
            }
            return true;
          }

          int safe_goto_init_promp(std::shared_ptr<FullProMP> promp, double T,
              const Chronometer& timer, int old_id=-1) {
            if (T < params.min_react_time) return old_id; 
            auto init_pos = promp->mean_traj_step(0,1);
            init_pos.qd = arma::zeros(promp->get_num_joints());
            auto go_to_init = make_shared<FullProMP>(go_to_promp(init_pos.q, init_pos.qd, T));
            if (old_id == -1) {
              return mov_prim->robot_controller()->addProMP(go_to_init, timer(), T);
            } else {
              return mov_prim->robot_controller()->editProMP((unsigned int)old_id, go_to_init, timer(), T);
            }
          }

          shared_ptr<FullProMP> cond_promp_ball(const pair<double,random::NormalDist>& ball_state, 
              double t0, double T, shared_ptr<FullProMP> org_promp) {
            double z_contact = (ball_state.first - t0) / T;
            auto qdes = prob_inv_kin_pos(*fwd_kin, org_promp->joint_dist(z_contact), ball_state.second);
            return make_shared<FullProMP>( org_promp->condition_pos(z_contact, qdes) );
          }

          json serialize_racket_dist(const vector<pair<double, random::NormalDist>>& racket_dist) {
            json ans;
            for (const auto& p : racket_dist) {
              json elem = {{"t", p.first}, {"mean", p.second.mean()}, {"cov", p.second.cov()}};
              ans.push_back(elem);
            }
            return ans;
          }

          json serialize_joint_mean(std::shared_ptr<FullProMP> promp, double t0, double T) {
            json ans;
            for (double t=0; t<=T; t+=0.002) {
              auto x = promp->mean_traj_step(t, T);
              json elem {{"t",t+t0},{"q",x.q},{"qd",x.qd},{"qdd", x.qdd}};
              ans.push_back(elem);
            }
            return ans;
          }

          void check_mov_prim_state(int mov_id, unsigned int num_primitives, double curr_time) {
            int curr_prim = mov_prim->robot_controller()->control_primitive_id(curr_time);
            unsigned int curr_size = mov_prim->robot_controller()->num_primitives();
            if (num_primitives != curr_size || mov_id != curr_prim) {
              BOOST_LOG_TRIVIAL(warning) << "Wrong state for movement primitive controller: " <<
                "Size=" << curr_size << ", Expected=" << num_primitives << ". MovId=" << curr_prim <<
                ", Expected=" << mov_id;
            }
          }

          /**
           * @brief Main task code. At most one thread is executing this method.
           */
          void run_task() {
            std::unique_lock<std::mutex> task_lck(task_mtx);
            condition_variable task_cv;
            running = true; log_ready = false;
            while (running) {
              //0.1) Load all required parameters
              conf_mtx.lock();
              params = conf_params;
              prior_promp = conf_prior_promp;
              outlier_filter = conf_outlier_filter;
              ball_model = conf_ball_model;
              fwd_kin = conf_fwd_kin;
              conf_mtx.unlock();
              auto org_promp = prior_promp;
              auto prior_racket_dist = get_racket_dist(org_promp);
              double T = params.T;
              double min_init_hit_loglh = params.min_init_hit_loglh;
              const Chronometer& timer = *(time_cntr->get_chrono());
              bool go_to_cond_promp = params.go_to_cond_promp;
              double uz = params.hit_mean, sigz = params.hit_std;
              auto log_prior_z = [uz,sigz](double z) -> double {
                double dif = z - uz;
                return -dif*dif/(2*sigz*sigz); 
              };
              unsigned int pred_len = (unsigned int)(params.time_pred_ball_traj / params.deltaT);
              //0.2) Go to ProMP prior mean (Very important: previous movement must be finished)
              if (mov_prim->robot_controller()->num_primitives() != 0) {
                BOOST_LOG_TRIVIAL(warning) << "The number of primitives before going to initial "
                  "condition should be zero, but there are " << 
                  mov_prim->robot_controller()->num_primitives() <<
                  ". Clearing state";
                mov_prim->robot_controller()->clear();
              }
              safe_goto_init_promp(org_promp, 0.5, timer);
              BOOST_LOG_TRIVIAL(info) << "Going to initial position";
              task_cv.wait_for(task_lck, chrono::milliseconds{500});
              if (!running) break;
              //0.3) Wait until previous ball disappears
              BOOST_LOG_TRIVIAL(info) << "Making sure that previous ball if any is not present";
              task_lck.unlock();
              std::unique_lock<std::mutex> ball_off_lck(ball_mtx);
              while (outlier_filter->ready() && (timer()-outlier_filter->last_time())<0.08) {
                ball_obs.wait_for(ball_off_lck,chrono::milliseconds{20});
              }
              ball_off_lck.unlock();
              task_lck.lock();
              trial_cleanup();
              if (!running) break;
              //1) Wait until ball model is ready
              task_lck.unlock();
              std::unique_lock<std::mutex> ball_lck(ball_mtx);
              BOOST_LOG_TRIVIAL(info) << "Waiting for ball observations";
              while (running && !outlier_filter->ready()) {
                BOOST_LOG_TRIVIAL(debug) << "Not yet ready";
                ball_obs.wait(ball_lck);
              }
              BOOST_LOG_TRIVIAL(debug) << "Now ball filter is ready";
              ball_lck.unlock();
              BOOST_LOG_TRIVIAL(info) << "Got some good ball observations t=" << timer();
              //1.1) And clean the log
              log_mtx.lock();
              log_ready = false;
              trial_log.clear();
              log_mtx.unlock();
              task_lck.lock();
              if (!running) break;

              //2) Compute optimal t0
              auto ball_traj = predict_ball_traj(pred_len, trial_log);
              double t0 = opt_start_time(ball_traj, prior_racket_dist, T, log_prior_z);
              double tot_hit_log_lh = log_mar_hit_lh(ball_traj, prior_racket_dist, t0, T, log_prior_z);
              trial_log["original_t0"] = t0;
              trial_log["tot_hit_log_lh"] = tot_hit_log_lh;
              BOOST_LOG_TRIVIAL(info) << "Opt. t0=" << t0 << " t=" << timer() << " total hit log_lh=" << tot_hit_log_lh;
              //2.1) Pre-checks
              if (tot_hit_log_lh < min_init_hit_loglh || (t0 + params.min_react_time) < timer()) {
                BOOST_LOG_TRIVIAL(info) << "The ball trajectory is considered unlikely or dangerous, aborting";
                task_cv.wait_for(task_lck, chrono::milliseconds{1000});
                trial_cleanup();
                continue;
              }
              //2.2) Do conditioning
              LogOverlaps l_over {prior_racket_dist, log_prior_z};
              vector<double> log_overlap = l_over(ball_traj, t0, T);
              unsigned int cond_ix = max_element(log_overlap.begin(), log_overlap.end()) - log_overlap.begin();
              if (log_overlap[cond_ix] < min_init_hit_loglh) {
                BOOST_LOG_TRIVIAL(info) << "The initial likelihood of hitting the ball is too low, aborting";
                task_cv.wait_for(task_lck, chrono::milliseconds{1000});
                trial_cleanup();
                continue;
              }
              double hit_log_likelihood = log_overlap[cond_ix];
              auto c_promp = cond_promp_ball(ball_traj[cond_ix], t0, T, org_promp); 
              int mov_id = -1;
              safe_add_promp(mov_id, c_promp, t0, T);
              BOOST_LOG_TRIVIAL(info) << "Initial proMP added at t=" << timer();
              //2.3) And go to initial position
              int goto_id = -1;
              if (go_to_cond_promp) {
                goto_id = safe_goto_init_promp(c_promp, t0 - timer() - 0.01, timer);
                if (goto_id != -1) BOOST_LOG_TRIVIAL(info) << "Initial goto ProMP added at t=" << timer();
              }
              //3) Get more ball observations and keep correcting
              while (t0 > timer()) {
                ball_traj = predict_ball_traj(pred_len, trial_log);
                if ( (t0 - timer()) > params.safe_t0_recomp ) {
                  double nt0 = opt_start_time(ball_traj, prior_racket_dist, T, log_prior_z);
                  if ( (nt0 - timer()) > params.min_react_time) {
                    if(go_to_cond_promp && goto_id != -1 && nt0 < t0) { 
                      mov_prim->robot_controller()->removeMP(goto_id);
                      goto_id = -1;
                    }
                    t0 = nt0;
                    BOOST_LOG_TRIVIAL(debug) << "Correcting start time to t0=" << t0;
                  }
                }
                auto n_log_overlap = l_over(ball_traj, t0, T);
                auto n_cond_ix = max_element(n_log_overlap.begin(), n_log_overlap.end()) - n_log_overlap.begin();
                if (params.use_new_ball_obs && n_log_overlap[n_cond_ix] > min_init_hit_loglh) {
                  swap(log_overlap, n_log_overlap);
                  cond_ix = n_cond_ix;
                  hit_log_likelihood = log_overlap[cond_ix];
                  c_promp = cond_promp_ball(ball_traj[cond_ix], t0, T, org_promp); 
                  safe_add_promp(mov_id, c_promp, t0, T);
                  BOOST_LOG_TRIVIAL(debug) << "Correcting the ProMP at t=" << timer() << 
                    " with hitting log_lh=" << hit_log_likelihood;
                  if ( go_to_cond_promp ) {
                    goto_id = safe_goto_init_promp(c_promp, timer() - t0 - 0.01, timer, goto_id);
                    BOOST_LOG_TRIVIAL(trace) << "Correcting goto at t=" << timer();
                  }
                }
                task_cv.wait_for(task_lck, chrono::milliseconds{20});
              }
              //3.1) Compute a final Exec ProMP
              unique_lock<mutex> joint_lock(joint_mtx);
              BOOST_LOG_TRIVIAL(info) << "Conditioning posterior ProMP on initial condition at t=" << last_joint_time;
              auto e_promp = make_shared<FullProMP>(c_promp->condition_current_state((timer()-t0)/T, T, last_q, last_qd));
              joint_lock.unlock();
              //3.2) Check the right primitive is in execution
              check_mov_prim_state(mov_id, 1, timer());              
              //3.5) If desired, continue re-planning with the new ball observations
              if (params.replan_safe_time < params.T) {
                BOOST_LOG_TRIVIAL(info) << "Starting the re-planning phase";
                double pred_thit = ball_traj[cond_ix].first;
                LogOverlaps l_over_replan {get_racket_dist(e_promp), log_prior_z};
                while ((timer() + params.replan_safe_time) < pred_thit) {
                  auto n_ball_traj = predict_ball_traj(pred_len, trial_log);
                  auto n_log_overlap = l_over_replan(n_ball_traj, t0, T);
                  //auto n_cond_ix = find_cond_ix(n_ball_traj, pred_thit);
                  auto n_cond_ix = max_element(n_log_overlap.begin(), n_log_overlap.end()) - n_log_overlap.begin();
                  if (n_log_overlap[n_cond_ix] > min_init_hit_loglh) {
                    c_promp = cond_promp_ball(n_ball_traj[n_cond_ix], t0, T, org_promp);
                    safe_add_promp(mov_id, c_promp, t0, T);
                    check_mov_prim_state(mov_id, 1, timer());
                    BOOST_LOG_TRIVIAL(info) << "Re-planning ProMP at time t=" << timer() << 
                      " with hit log_lh=" << n_log_overlap[n_cond_ix];
                    swap(ball_traj, n_ball_traj);
                    swap(log_overlap, n_log_overlap);
                    cond_ix = n_cond_ix;
                  }
                  //Sleep for a bit before next ball
                  task_cv.wait_for(task_lck, chrono::milliseconds{20});
                } 
              }
              //4) Save some final log information
              BOOST_LOG_TRIVIAL(info) << "Creating log information";
              log_mtx.lock();
              trial_log["t0"] = t0;
              trial_log["log_overlap"] = log_overlap;
              trial_log["cond_ix"] = cond_ix;
              trial_log["racket_prior"] = serialize_racket_dist(l_over.get_racket_dist());
              LogOverlaps l_over_pos {get_racket_dist(c_promp), log_prior_z};
              log_overlap = l_over_pos(ball_traj, t0, T);
              trial_log["post_overlap"] = log_overlap;
              trial_log["racket_post"] = serialize_racket_dist(l_over_pos.get_racket_dist());
              l_over_pos.set_racket(get_racket_dist(e_promp));
              log_overlap = l_over_pos(ball_traj, t0, T);
              trial_log["racket_post_init_cond"] = serialize_racket_dist(l_over_pos.get_racket_dist());
              trial_log["joint_mean"] = serialize_joint_mean(org_promp, t0, T);
              trial_log["joint_mean_post"] = serialize_joint_mean(e_promp, t0, T);
              log_ready = true;
              log_mtx.unlock();
              BOOST_LOG_TRIVIAL(info) << "Log information created" << endl;

              //5) Wait and cleanup
              unsigned int promp_dur = (unsigned int)(1000*T);
              task_cv.wait_for(task_lck, chrono::milliseconds{promp_dur});
              BOOST_LOG_TRIVIAL(info) << "done" << endl;
            }
            BOOST_LOG_TRIVIAL(info) << "Quitting the table tennis task";
          }

          void start() {
            lock_guard<mutex> task_lck(task_mtx);
            if (!running) {
              register_listeners(); //Activate methods to receive ball and joint observations
              std::thread my_task{ [this]() { this->run_task(); } };
              my_task.detach();
            }
          }

          void stop() {
            lock_guard<mutex> task_lck(task_mtx);
            running = false;
            this->ball_obs.notify_all();
          }

          Impl(std::shared_ptr<MovPrimTask> mov_prim, std::shared_ptr<JointObsController> joint_obs, 
              std::shared_ptr<CollectObs> sensor_obs, std::shared_ptr<TimeSyncController> time_cntr) : 
              mov_prim(mov_prim), joint_obs(joint_obs), sensor_obs(sensor_obs), time_cntr(time_cntr) {
            running = false; 
          }
      };

      TableTennisProMP::TableTennisProMP(std::shared_ptr<MovPrimTask> mov_prim, 
              std::shared_ptr<JointObsController> joint_obs, std::shared_ptr<CollectObs> sensor_obs,
              std::shared_ptr<TimeSyncController> time_cntr) {
        _impl = unique_ptr<Impl>{ new Impl{mov_prim, joint_obs, sensor_obs, time_cntr} };
        _impl->register_methods(*this);
      }

      TableTennisProMP::~TableTennisProMP() = default;

      /**
       * @brief Starts the table tennis task and returns the current time on success
       */
      json TableTennisProMP::start_task(const json& body) {
        if (_impl->conf_ball_model == nullptr || _impl->conf_outlier_filter == nullptr || 
            _impl->conf_prior_promp == nullptr || _impl->conf_fwd_kin == nullptr) {
          return {{"error", "The ball model, promp, kinematics and outlier detection "
            "parameters must be passed to the table tennis task before it is started"}};
        }
        _impl->start();
        return _impl->time_cntr->getTime(body);
      }

      /**
       * @brief Stops the table tennis task and returns the current time on success
       */
      json TableTennisProMP::stop_task(const json& body) {
        _impl->stop();
        return _impl->time_cntr->getTime(body);
      }

      /**
       * @brief Loads ball filter from JSON configuration
       */
      json TableTennisProMP::load_ball_filter(const json& body) {
        auto ball_filter = table_tennis::ball_filters::load_kalman_outlier_3d(_impl->conf_ball_model, body);
        lock_guard<mutex> conf_lock(_impl->conf_mtx);
        _impl->conf_outlier_filter = ball_filter;
        return {{"status", "OK"}};
      }

      /**
       * @brief Loads a ball model from a JSON configuration
       */
      json TableTennisProMP::load_ball_model(const json& body) {
        auto ball_model = robotics::table_tennis::load_ball_model(body);
        lock_guard<mutex> conf_lock(_impl->conf_mtx);
        _impl->conf_ball_model = ball_model;
        return {{"status","OK"}};
      }

      /**
       * @brief Loads a ProMP prior strike movement
       */
      json TableTennisProMP::load_prior_promp(const json& body) {
        auto full_promp = make_shared<FullProMP>(json2full_promp(body));
        lock_guard<mutex> conf_lock(_impl->conf_mtx);
        _impl->conf_prior_promp = full_promp;
        return {{"status", "OK"}};
      }
      
      /**
       * @brief Loads the parameters of the Barrett forward kinematics from JSON
       */
      json TableTennisProMP::load_barrett_kin(const json& body) {
        auto kin = robotics::create_barrett_wam_kin(body);
        lock_guard<mutex> conf_lock(_impl->conf_mtx);
        _impl->conf_fwd_kin = kin;
        return {{"status", "OK"}};
      }

      /**
       * @brief Returns the JSON task log
       */
      json TableTennisProMP::get_log(const json& body) {
        (void)body;
        std::lock_guard<mutex> log_lock(_impl->log_mtx);
        json ans;
        if (_impl->log_ready) {
          std::swap(ans, _impl->trial_log);
          _impl->log_ready = false;
        }
        return ans;
      }

      /**
       * @brief Configures the method parameters via JSON
       */
      json TableTennisProMP::set_config(const json& body) {
        return _impl->set_config(body);
      }

    };
  };
};
