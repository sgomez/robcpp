#include <robotics/slpp/net/tt_single_promp.hpp>
#include <robotics/slpp/net/ball_traj_predict.hpp>
#include <robotics/table_tennis.hpp>
#include <robotics/utils/promp_utils.hpp>
#include <robotics.hpp>
#include <deque>
#include <mutex>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <armadillo>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>

using namespace std;
using namespace arma;
namespace logging = boost::log;

namespace robotics {

  using namespace table_tennis;

  namespace slpp {
    namespace net {
      using json = nlohmann::json;

      class TTSingleProMP::Impl {
        public:
          /***** Definitions *****/
          struct Params {
            double deltaT = 1.0/180.0; //!< Average time between ball obs
            double T = 0.5; //!< Total duration of the ProMP
            double hit_mean = 0.5; //!< Hit prior mean
            double hit_std = 0.1; //!< Hit prior standard deviation
            //! Minimum number of ball observations to consider a new trial started
            unsigned int min_obs_start_trial = 8, max_obs_start_trial = 12;
            //! Minimum ball trajectory log-lh to consider a new trial started
            double min_traj_llh_start_trial = -10; 
            //! Maximum time gap between ball observations
            double max_time_gap_ball = 0.1;
            //! Threshold on hit log-likelihood to consider a trajectory dangerous
            double min_init_hit_loglh = -10;
            //! Should the robot try to move to initial state of conditioned ProMP?
            bool go_to_cond_promp = false;
            //! Should the approach use additional ball observations to re-plan before moving
            bool use_new_ball_obs = true;
            //! Total time length for the predicted ball trajectory
            double time_pred_ball_traj = 1.2;
            //! Time before planned movement considered safe to recompute t0
            double safe_t0_recomp = 0.3;
            //! Minimal allowed reaction time after t0 is computed
            double min_react_time = 0.1;
            //! Minimal time before predicted hitting so re-planning is done.
            //! If larger than T, no re-planning is done.
            double replan_safe_time = 1.0;
            //! Reject any ball observation too far from predicted trajectory in Mah Distance
            double max_ball_mah_dist = 3.0;
            double max_qd = 5; //!< Maximum recommended joint velocity
            double max_qdd = 50; //!< Maximum recommended joint acceleration
            double min_ball_y = -3.5, max_ball_y=0;
          };

          /***** State Variables *****/
          std::shared_ptr<MovPrimTask> mov_prim;
          std::shared_ptr<JointObsController> joint_obs;
          std::shared_ptr<CollectObs> sensor_obs;
          std::shared_ptr<TimeSyncController> time_cntr;
          std::shared_ptr<BallTrajPredict> ball_model, conf_ball_model;
          std::shared_ptr<FullProMP> prior_promp, conf_prior_promp;
          std::shared_ptr<FwdKin> fwd_kin, conf_fwd_kin;
          Params params, conf_params;
          bool running;
          std::mutex ball_mtx, joint_mtx, task_mtx, log_mtx, conf_mtx;
          std::condition_variable ball_obs;
          json trial_log;
          bool log_ready = true;
          bool trial_started = false;
          vector<double> last_q, last_qd;
          double last_goto_duration, last_goto_finish = 0.0;
          double last_joint_time;
          deque<pair<double,vector<double>>> prev_ball_obs;
          vector<pair<double,random::NormalDist>> prev_ball_traj;

          /***** Methods *****/

          void register_listeners() {
            auto joint_listener = [this](const std::vector<double>& q, const std::vector<double>& q_dot, 
                double time) {
              std::lock_guard<std::mutex> lck(this->joint_mtx);
              this->last_q = q;
              this->last_qd = q_dot;
              this->last_joint_time = time;
            };
            auto sensor_listener = [this](unsigned int id, double time, const std::vector<double>& obs) {
              std::lock_guard<std::mutex> lck(this->ball_mtx);
              BOOST_LOG_TRIVIAL(trace) << "Receiving observation from sensor " << id;
              if (prev_ball_obs.size()>0 && (time - prev_ball_obs.back().first) > params.max_time_gap_ball
                  && !trial_started) {
                prev_ball_obs.clear();
              }
              if (obs[1]>params.min_ball_y && obs[1]<params.max_ball_y) {
                prev_ball_obs.push_back(make_pair(time, obs));
              }
              this->ball_obs.notify_all();
            };
            joint_obs->add_joint_listener("promp_tt", joint_listener);
            sensor_obs->add_obs_listener("promp_tt", sensor_listener);
          }

          void unregister_listeners() {
            joint_obs->rm_joint_listener("promp_tt");
            sensor_obs->rm_obs_listener("promp_tt");
          }

          json set_config(const json& conf) {
            lock_guard<mutex> conf_lock(conf_mtx);
            json ans;
            if (conf.count("deltaT")) conf_params.deltaT = conf.at("deltaT");
            if (conf.count("T")) conf_params.T = conf.at("T");
            if (conf.count("hit_mean")) conf_params.hit_mean = conf.at("hit_mean");
            if (conf.count("hit_std")) conf_params.hit_std = conf.at("hit_std");
            if (conf.count("min_obs_start_trial"))
              conf_params.min_obs_start_trial = conf.at("min_obs_start_trial");
            if (conf.count("max_obs_start_trial"))
              conf_params.max_obs_start_trial = conf.at("max_obs_start_trial");
            if (conf.count("min_traj_llh_start_trial"))
              conf_params.min_traj_llh_start_trial = conf.at("min_traj_llh_start_trial");
            if (conf.count("min_init_hit_loglh"))
              conf_params.min_init_hit_loglh = conf.at("min_init_hit_loglh");
            if (conf.count("go_to_cond_promp"))
              conf_params.go_to_cond_promp = conf.at("go_to_cond_promp");
            if (conf.count("use_new_ball_obs"))
              conf_params.use_new_ball_obs = conf.at("use_new_ball_obs");
            if (conf.count("time_pred_ball_traj"))
              conf_params.time_pred_ball_traj = conf.at("time_pred_ball_traj");
            if (conf.count("safe_t0_recomp"))
              conf_params.safe_t0_recomp = conf.at("safe_t0_recomp");
            if (conf.count("min_react_time"))
              conf_params.min_react_time = conf.at("min_react_time");
            if (conf.count("replan_safe_time"))
              conf_params.replan_safe_time = conf.at("replan_safe_time");
	    if (conf.count("max_time_gap_ball"))
              conf_params.max_time_gap_ball = conf.at("max_time_gap_ball");
	    if (conf.count("max_ball_mah_dist"))
              conf_params.max_ball_mah_dist = conf.at("max_ball_mah_dist");
            if (conf.count("max_qd"))
              conf_params.max_qd = conf.at("max_qd");
            if (conf.count("max_qdd"))
              conf_params.max_qdd = conf.at("max_qdd");
            if (conf.count("min_ball_y"))
              conf_params.min_ball_y = conf.at("min_ball_y");
            if (conf.count("max_ball_y"))
              conf_params.max_ball_y = conf.count("max_ball_y");
            return ans;
          }

          void register_methods(TTSingleProMP& self) {
            self.register_method("start", [&self](const json& body) { return self.start_task(body); });
            self.register_method("stop", [&self](const json& body) { return self.stop_task(body); });
            self.register_method("load_ball_model", [&self](const json& body) { return self.load_ball_model(body); });
            //self.register_method("load_ball_filter", [&self](const json& body) { return self.load_ball_filter(body); });
            self.register_method("load_barrett_kin", [&self](const json& body) { return self.load_barrett_kin(body); });
            self.register_method("load_prior_promp", [&self](const json& body) { return self.load_prior_promp(body); });
            self.register_method("get_log", [&self](const json& body) { return self.get_log(body); });
            self.register_method("set_config", [&self](const json& body) { return self.set_config(body); });
          }

          double mah_dist(vec x, const random::NormalDist& dist) {
            vec d = x - dist.mean();
            double dprod = dot(d, dist.inv_cov()*d);
            return sqrt(dprod);
          }

          vector<pair<double,random::NormalDist>> predict_ball_traj(unsigned int time_steps, json& trial_log) {
            const Chronometer& timer = *(time_cntr->get_chrono());
            vector<double> pred_times;
            deque<pair<double,vector<double>>> good_prev_ball_obs;
            double t0 = prev_ball_obs[0].first;
            for (unsigned int i=0; i<time_steps; i++) {
              pred_times.push_back(t0 + params.deltaT*i);
            }

            //1) Compute a set of ball observations close to previous predictions
            for (unsigned int i=0, j=0; i<prev_ball_obs.size(); i++) {
              double obs_mah_dist = 0.0;
              if (prev_ball_traj.size() > 0) {
                //It means that a ball trajectory was computed before
                while(j<prev_ball_traj.size() && 
                    (prev_ball_traj[j].first + params.deltaT*0.5) < prev_ball_obs[i].first) {
                  j++;
                }
                obs_mah_dist = mah_dist(prev_ball_obs[i].second, prev_ball_traj[j].second); 
              }
              if (obs_mah_dist < params.max_ball_mah_dist) {
                good_prev_ball_obs.push_back(prev_ball_obs[i]);
              }
            }
            BOOST_LOG_TRIVIAL(trace) << "Computing ball trajectory with " << good_prev_ball_obs.size() << 
              " obs out of "  << prev_ball_obs.size();

	    double t1 = timer();
            //2) Compute the ball traj distribution
            vector<pair<double,random::NormalDist>> ans;
            if (good_prev_ball_obs.size() >= params.min_obs_start_trial) {
              ans = ball_model->traj_dist(good_prev_ball_obs, pred_times);
            } else {
              ans = prev_ball_traj;
            }
	    double t2 = timer();
	    BOOST_LOG_TRIVIAL(info) << "Latency of ball trajectory prediction " << 1000*(t2 - t1) << " ms";
            json times, means, covs;
            BOOST_LOG_TRIVIAL(trace) << "Decoding the trajectory prediction answer";
            for (auto p : ans) {
              times.push_back(p.first);
              means.push_back(p.second.mean());
              covs.push_back(p.second.cov());
            }
            BOOST_LOG_TRIVIAL(trace) << "Trajectory prediction answer decoded. Saving the log";
            trial_log["predict_ball_traj"] = { {"times", times}, {"means", means}, {"covs", covs}, {"last_time", prev_ball_obs.back().first} };
            prev_ball_traj = ans;
            BOOST_LOG_TRIVIAL(trace) << "Saving the log of the ball trajectory prediction";
            return ans;
          }

          unsigned int find_cond_ix(const vector<pair<double,random::NormalDist>>& ball_traj, double pred_thit) {
            unsigned int a=0, b=ball_traj.size();
            while (a < b) {
              unsigned int m = (a + b) / 2;
              if (pred_thit > ball_traj[m].first) a = m+1;
              else b = m;
            }
            return b;
          }

          std::function<random::NormalDist(double z)> get_racket_dist(std::shared_ptr<FullProMP> promp) {
            auto fwd_kin = this->fwd_kin;
            auto ans = [promp, fwd_kin](double z) -> random::NormalDist {
              return prob_fwd_kin_pos(*fwd_kin, promp->joint_dist(z)); 
            };
            return ans;
          }

          /**
           * @brief Clean up the state between different trials
           */
          void trial_cleanup() {
            //1) Make sure ball state is reset for next incoming ball
            prev_ball_obs.clear();
            prev_ball_traj.clear();
            trial_started = false;
          }

          /**
           * @brief Adds/Modifies a ProMP with safety considerations. Returns true if the ProMP is added.
           */
          bool safe_add_promp(int& id, std::shared_ptr<FullProMP> promp, double t0, 
              double T) {
            //1) Check that all safety regulations are met
            //2) Add the ProMP
            if (id == -1) {
              id = mov_prim->robot_controller()->addProMP(promp, t0, T);
            } else {
              id = mov_prim->robot_controller()->editProMP((unsigned int)id, promp, t0, T);
            }
            return true;
          }

          int safe_goto_init_promp(std::shared_ptr<FullProMP> promp, double T,
              const Chronometer& timer, int old_id=-1) {
            if (T < params.min_react_time) {
              BOOST_LOG_TRIVIAL(trace) << "go_to duration " << T << " is lower than " << 
                params.min_react_time << ". Aborting";
              return old_id;
            }
            BOOST_LOG_TRIVIAL(debug) << "go_to: Obtaining the Prior ProMP mean";
            auto init_pos = promp->mean_traj_step(0,1);
            init_pos.qd = arma::zeros(promp->get_num_joints());
            vec q0 = last_q, qd0 = last_qd;
            BOOST_LOG_TRIVIAL(debug) << "go_to: Obtaining the fastest T respecting the limits vel<" << params.max_qd <<
              " and acc<" << params.max_qdd;
            BOOST_LOG_TRIVIAL(debug) << "go_to: Between q0= " << vec2json(q0) << ", qd0=" << vec2json(qd0) << 
              ", q1=" << vec2json(init_pos.q) << ", qd1=" << vec2json(init_pos.qd);
            double fastest_T = find_goto_duration(q0, qd0, init_pos.q, init_pos.qd, params.max_qd, params.max_qdd);
            BOOST_LOG_TRIVIAL(debug) << "go_to: fastest_T=" << fastest_T;
            fastest_T = max(0.1, fastest_T);
            if (fastest_T > T) {
              BOOST_LOG_TRIVIAL(warning) << "Not enough time for the go_to movement, T=" << T << " but fastest_T=" << fastest_T;
              if (old_id != -1) {
                BOOST_LOG_TRIVIAL(trace) << "Keeping old go_to movement. Aborting";
                return old_id;
              }
            }
            last_goto_duration = fastest_T;
            if (fastest_T > 0.01) {
              BOOST_LOG_TRIVIAL(info) << "Adding a fast go_to movement. T=" << T << " and fastest_T=" << fastest_T;
              auto go_to_init = make_shared<FullProMP>(go_to_promp(init_pos.q, init_pos.qd, last_goto_duration));
              if (old_id == -1) {
                return mov_prim->robot_controller()->addProMP(go_to_init, timer(), last_goto_duration);
              } else {
                return mov_prim->robot_controller()->editProMP((unsigned int)old_id, go_to_init, timer(), last_goto_duration);
              }
              last_goto_finish = timer() + last_goto_duration;
            } else {
              BOOST_LOG_TRIVIAL(trace) << "go_to movement too short. T=" << fastest_T << ", deltaQ=" << norm(q0 - init_pos.q) << ". Aborting";
              return old_id;
            }
          }

          shared_ptr<FullProMP> cond_promp_ball(const pair<double,random::NormalDist>& ball_state, 
              double t0, double T, shared_ptr<FullProMP> org_promp) {
            double z_contact = (ball_state.first - t0) / T;
            auto qdes = prob_inv_kin_pos(*fwd_kin, org_promp->joint_dist(z_contact), ball_state.second);
            return make_shared<FullProMP>( org_promp->condition_pos(z_contact, qdes) );
          }

          json serialize_racket_dist(const vector<pair<double, random::NormalDist>>& racket_dist) {
            json ans;
            for (const auto& p : racket_dist) {
              json elem = {{"t", p.first}, {"mean", p.second.mean()}, {"cov", p.second.cov()}};
              ans.push_back(elem);
            }
            return ans;
          }

          json serialize_joint_mean(std::shared_ptr<FullProMP> promp, double t0, double T) {
            json ans;
            for (double t=0; t<=T; t+=0.002) {
              auto x = promp->mean_traj_step(t, T);
              json elem {{"t",t+t0},{"q",x.q},{"qd",x.qd},{"qdd", x.qdd}};
              ans.push_back(elem);
            }
            return ans;
          }

          void check_mov_prim_state(int mov_id, unsigned int num_primitives, double curr_time) {
            int curr_prim = mov_prim->robot_controller()->control_primitive_id(curr_time);
            unsigned int curr_size = mov_prim->robot_controller()->num_primitives();
            if (num_primitives != curr_size || mov_id != curr_prim) {
              BOOST_LOG_TRIVIAL(warning) << "Wrong state for movement primitive controller: " <<
                "Size=" << curr_size << ", Expected=" << num_primitives << ". MovId=" << curr_prim <<
                ", Expected=" << mov_id;
            }
          }

          bool has_trial_started() {
            if (trial_started) return true;
            BOOST_LOG_TRIVIAL(trace) << "trial_started: Checking that the conditions to start a trial are satisfied";
            if (prev_ball_obs.size() >= params.min_obs_start_trial) {
              int last_ok = prev_ball_obs.size()-1;
              while (last_ok>0 && prev_ball_obs[last_ok-1].second[1] < prev_ball_obs[last_ok].second[1]
                  && (prev_ball_obs[last_ok].first - prev_ball_obs[last_ok-1].first) < params.max_time_gap_ball ) {
                last_ok--;
              }
              BOOST_LOG_TRIVIAL(trace) << "trial_started: The last observation satisfying the criteria is " << last_ok 
                << " out of " << prev_ball_obs.size();
              if (last_ok > 0) {
                prev_ball_obs.erase(prev_ball_obs.begin(), prev_ball_obs.begin()+last_ok);
              }
              BOOST_LOG_TRIVIAL(trace) << "trial_started: The new prev_ball_obs size is " << prev_ball_obs.size();
            }
            if (prev_ball_obs.size() < params.min_obs_start_trial) return false;
            double obs_ll = ball_model->traj_llh(prev_ball_obs);
            if (obs_ll > params.min_traj_llh_start_trial) {
              BOOST_LOG_TRIVIAL(trace) << "trial_started: Trial started with "  << prev_ball_obs.size() << 
                " obs. The likelihood is high enough " << obs_ll;
              return true;
            } else {
              BOOST_LOG_TRIVIAL(trace) << "trial_started: Trial not started with " << prev_ball_obs.size() << 
                "obs. The likelihood was too low " << obs_ll;
              BOOST_LOG_TRIVIAL(trace) << "Discarding ball observation";
              prev_ball_obs.clear();
              return false;
            }
          }

          /**
           * @brief Main task code. At most one thread is executing this method.
           */
          void run_task() {
            BOOST_LOG_TRIVIAL(info) << "Starting the table tennis task thread";
            std::unique_lock<std::mutex> task_lck(task_mtx);
            condition_variable task_cv;
            running = true; log_ready = false;
            // make sure all variables are set before starting. Sleep for a bit
            task_cv.wait_for(task_lck, chrono::milliseconds{20});
            trial_cleanup();
            while (running) {
              BOOST_LOG_TRIVIAL(trace) << "Loading all the required parameters";
              //0.1) Load all required parameters
              conf_mtx.lock();
              params = conf_params;
              prior_promp = conf_prior_promp;
              ball_model = conf_ball_model;
              fwd_kin = conf_fwd_kin;
              conf_mtx.unlock();
              BOOST_LOG_TRIVIAL(debug) << "Loading the prior ProMP";
              auto org_promp = prior_promp;
              BOOST_LOG_TRIVIAL(debug) << "Loading the racket distribution";
              auto prior_racket_dist = get_racket_dist(org_promp);
              BOOST_LOG_TRIVIAL(debug) << "Loading the ProMP duration and minimum likelihoods";
              double T = params.T;
              double min_init_hit_loglh = params.min_init_hit_loglh;
              BOOST_LOG_TRIVIAL(debug) << "Loading the chronometer (timer)";
              const Chronometer& timer = *(time_cntr->get_chrono());
              bool go_to_cond_promp = params.go_to_cond_promp;
              BOOST_LOG_TRIVIAL(debug) << "Loading the prior distribution parameters on the hitting time";
              double uz = params.hit_mean, sigz = params.hit_std;
              auto log_prior_z = [uz,sigz](double z) -> double {
                double dif = z - uz;
                return -dif*dif/(2*sigz*sigz); 
              };
              unsigned int pred_len = (unsigned int)(params.time_pred_ball_traj / params.deltaT);
              BOOST_LOG_TRIVIAL(debug) << "All parameters loaded. Attempting to go to the initial position";
              //0.2) Go to ProMP prior mean (Very important: previous movement must be finished)
              if (mov_prim->robot_controller()->num_primitives() != 0) {
                BOOST_LOG_TRIVIAL(warning) << "The number of primitives before going to initial "
                  "condition should be zero, but there are " << 
                  mov_prim->robot_controller()->num_primitives() <<
                  ". Clearing state";
                mov_prim->robot_controller()->clear();
              }
              safe_goto_init_promp(org_promp, 1.0, timer);
              BOOST_LOG_TRIVIAL(info) << "Going to initial position";
              task_cv.wait_for(task_lck, chrono::milliseconds{(int)(1000*last_goto_duration)});
              if (!running) break;
              //0.3) Wait until previous ball disappears
              /*BOOST_LOG_TRIVIAL(info) << "Making sure that previous ball if any is not present";
              task_lck.unlock();
              std::unique_lock<std::mutex> ball_off_lck(ball_mtx);
              while (prev_ball_obs.size()>0 && (timer()-prev_ball_obs.back().first)<params.max_time_gap_ball) {
                ball_obs.wait_for(ball_off_lck,chrono::milliseconds{10});
              }
              trial_started = false;
              ball_off_lck.unlock();
              task_lck.lock();
              trial_cleanup();
              if (!running) break;*/
              //1) Wait until the ball re-appears to start a trial
              task_lck.unlock();
              std::unique_lock<std::mutex> ball_lck(ball_mtx);
              BOOST_LOG_TRIVIAL(info) << "Waiting for ball observations";
              while (running && !has_trial_started()) {
                BOOST_LOG_TRIVIAL(debug) << "Not yet ready";
                ball_obs.wait(ball_lck);
              }
              BOOST_LOG_TRIVIAL(debug) << "Now ball filter is ready";
              trial_started = true;
              ball_lck.unlock();
              BOOST_LOG_TRIVIAL(info) << "Got some good ball observations t=" << timer();
              //1.1) And clean the log
              log_mtx.lock();
              log_ready = false;
              trial_log.clear();
              log_mtx.unlock();
              task_lck.lock();
              BOOST_LOG_TRIVIAL(trace) << "Log cleaned. info: running=" << running;
              if (!running) break;

              //2) Compute optimal t0
              BOOST_LOG_TRIVIAL(info) << "Computing trajectory prediction for the first time";
              auto ball_traj = predict_ball_traj(pred_len, trial_log);
              BOOST_LOG_TRIVIAL(trace) << "Computing optimal t0 for the first time";
              double t0 = opt_start_time(ball_traj, prior_racket_dist, T, log_prior_z);
              double tot_hit_log_lh = log_mar_hit_lh(ball_traj, prior_racket_dist, t0, T, log_prior_z);
              trial_log["original_t0"] = t0;
              trial_log["tot_hit_log_lh"] = tot_hit_log_lh;
              BOOST_LOG_TRIVIAL(info) << "Opt. t0=" << t0 << " t=" << timer() << " total hit log_lh=" << tot_hit_log_lh;
              //2.1) Pre-checks
              if (tot_hit_log_lh < min_init_hit_loglh || (t0 + params.min_react_time) < timer()) {
                BOOST_LOG_TRIVIAL(info) << "The ball trajectory is considered unlikely or dangerous, aborting";
                task_cv.wait_for(task_lck, chrono::milliseconds{1000});
                trial_cleanup();
                continue;
              }
              //2.2) Do conditioning
              LogOverlaps l_over {prior_racket_dist, log_prior_z};
              vector<double> log_overlap = l_over(ball_traj, t0, T);
              unsigned int cond_ix = max_element(log_overlap.begin(), log_overlap.end()) - log_overlap.begin();
              if (log_overlap[cond_ix] < min_init_hit_loglh) {
                BOOST_LOG_TRIVIAL(info) << "The initial likelihood of hitting the ball is too low, aborting";
                task_cv.wait_for(task_lck, chrono::milliseconds{1000});
                trial_cleanup();
                continue;
              }
              double hit_log_likelihood = log_overlap[cond_ix];
              auto c_promp = cond_promp_ball(ball_traj[cond_ix], t0, T, org_promp); 
              int mov_id = -1;
              safe_add_promp(mov_id, c_promp, t0, T);
              BOOST_LOG_TRIVIAL(info) << "Initial proMP added at t=" << timer();
              //2.3) And go to initial position
              int goto_id = -1;
              if (go_to_cond_promp) {
                goto_id = safe_goto_init_promp(c_promp, t0 - timer() - 0.01, timer);
                if (goto_id != -1) BOOST_LOG_TRIVIAL(info) << "Initial goto ProMP added at t=" << timer();
              }
              //3) Get more ball observations and keep correcting
              while (t0 > timer()) {
                BOOST_LOG_TRIVIAL(trace) << "Get more ball observations and keep correcting";
                ball_traj = predict_ball_traj(pred_len, trial_log);
                if ( (t0 - timer()) > params.safe_t0_recomp ) {
                  double nt0 = opt_start_time(ball_traj, prior_racket_dist, T, log_prior_z);
                  if ( (nt0 - timer()) > params.min_react_time) {
                    if(go_to_cond_promp && goto_id != -1 && nt0 < t0) { 
                      mov_prim->robot_controller()->removeMP(goto_id);
                      goto_id = -1;
                    }
                    t0 = nt0;
                    BOOST_LOG_TRIVIAL(debug) << "Correcting start time to t0=" << t0;
                  }
                }
                auto n_log_overlap = l_over(ball_traj, t0, T);
                auto n_cond_ix = max_element(n_log_overlap.begin(), n_log_overlap.end()) - n_log_overlap.begin();
                if (params.use_new_ball_obs && n_log_overlap[n_cond_ix] > min_init_hit_loglh) {
                  swap(log_overlap, n_log_overlap);
                  cond_ix = n_cond_ix;
                  hit_log_likelihood = log_overlap[cond_ix];
                  c_promp = cond_promp_ball(ball_traj[cond_ix], t0, T, org_promp);
                  safe_add_promp(mov_id, c_promp, t0, T);
                  BOOST_LOG_TRIVIAL(debug) << "Correcting the ProMP at t=" << timer() << 
                    " with hitting log_lh=" << hit_log_likelihood;
                  if ( go_to_cond_promp && timer()>last_goto_finish) {
                    goto_id = safe_goto_init_promp(c_promp, t0 - timer() - 0.01, timer, goto_id);
                    BOOST_LOG_TRIVIAL(trace) << "Correcting goto at t=" << timer();
                  }
                } else {
                  BOOST_LOG_TRIVIAL(trace) << "Not correcting ProMP at t=" << timer() << ", hit_lh=" << 
                    n_log_overlap[n_cond_ix] << " < " << min_init_hit_loglh << " or flag not set";
                }
                task_cv.wait_for(task_lck, chrono::milliseconds{20});
              }
              //3.1) Compute a final Exec ProMP
              unique_lock<mutex> joint_lock(joint_mtx);
              BOOST_LOG_TRIVIAL(info) << "Conditioning posterior ProMP on initial condition at t=" << last_joint_time;
              auto e_promp = make_shared<FullProMP>(c_promp->condition_current_state((timer()-t0)/T, T, last_q, last_qd));
              joint_lock.unlock();
              //3.2) Check the right primitive is in execution
              check_mov_prim_state(mov_id, 1, timer());              
              //3.5) If desired, continue re-planning with the new ball observations
              if (params.replan_safe_time < params.T) {
                BOOST_LOG_TRIVIAL(info) << "Starting the re-planning phase";
                double pred_thit = ball_traj[cond_ix].first;
                LogOverlaps l_over_replan {get_racket_dist(e_promp), log_prior_z};
                while ((timer() + params.replan_safe_time) < pred_thit) {
                  auto n_ball_traj = predict_ball_traj(pred_len, trial_log);
                  auto n_log_overlap = l_over_replan(n_ball_traj, t0, T);
                  //auto n_cond_ix = find_cond_ix(n_ball_traj, pred_thit);
                  auto n_cond_ix = max_element(n_log_overlap.begin(), n_log_overlap.end()) - n_log_overlap.begin();
                  if (n_log_overlap[n_cond_ix] > min_init_hit_loglh) {
                    c_promp = cond_promp_ball(n_ball_traj[n_cond_ix], t0, T, org_promp);
                    safe_add_promp(mov_id, c_promp, t0, T);
                    check_mov_prim_state(mov_id, 1, timer());
                    BOOST_LOG_TRIVIAL(info) << "Re-planning ProMP at time t=" << timer() << 
                      " with hit log_lh=" << n_log_overlap[n_cond_ix];
                    swap(ball_traj, n_ball_traj);
                    swap(log_overlap, n_log_overlap);
                    cond_ix = n_cond_ix;
                  }
                  //Sleep for a bit before next ball
                  task_cv.wait_for(task_lck, chrono::milliseconds{20});
                } 
              }
              //4) Save some final log information
              BOOST_LOG_TRIVIAL(info) << "Creating log information";
              log_mtx.lock();
              trial_log["t0"] = t0;
              trial_log["log_overlap"] = log_overlap;
              trial_log["cond_ix"] = cond_ix;
              trial_log["racket_prior"] = serialize_racket_dist(l_over.get_racket_dist());
              LogOverlaps l_over_pos {get_racket_dist(c_promp), log_prior_z};
              log_overlap = l_over_pos(ball_traj, t0, T);
              trial_log["post_overlap"] = log_overlap;
              trial_log["racket_post"] = serialize_racket_dist(l_over_pos.get_racket_dist());
              l_over_pos.set_racket(get_racket_dist(e_promp));
              log_overlap = l_over_pos(ball_traj, t0, T);
              trial_log["racket_post_init_cond"] = serialize_racket_dist(l_over_pos.get_racket_dist());
              trial_log["joint_mean"] = serialize_joint_mean(org_promp, t0, T);
              trial_log["joint_mean_post"] = serialize_joint_mean(e_promp, t0, T);
              log_ready = true;
              log_mtx.unlock();
              BOOST_LOG_TRIVIAL(info) << "Log information created" << endl;

              //5) Wait and cleanup
              unsigned int promp_dur = (unsigned int)(1000*T);
              task_cv.wait_for(task_lck, chrono::milliseconds{promp_dur});
              BOOST_LOG_TRIVIAL(info) << "done" << endl;
              BOOST_LOG_TRIVIAL(info) << "Cleaning up for the next trial";
              trial_cleanup();
            }
            BOOST_LOG_TRIVIAL(info) << "Quitting the table tennis task";
          }

          void start() {
            lock_guard<mutex> task_lck(task_mtx);
            if (!running) {
              BOOST_LOG_TRIVIAL(trace) << "Registering the listeners for the table tennis task";
              register_listeners(); //Activate methods to receive ball and joint observations
              BOOST_LOG_TRIVIAL(trace) << "Listeners registered. Starting the table tennis thread";
              std::thread my_task{ [this]() { this->run_task(); } };
              my_task.detach();
              BOOST_LOG_TRIVIAL(trace) << "Deataching table tennis thread";
            } else {
              BOOST_LOG_TRIVIAL(warning) << "The start method was called while the task was already running";
            }
          }

          void stop() {
            lock_guard<mutex> task_lck(task_mtx);
            BOOST_LOG_TRIVIAL(info) << "The stop method was called. Setting running to false";
            running = false;
            this->ball_obs.notify_all();
          }

          Impl(std::shared_ptr<MovPrimTask> mov_prim, std::shared_ptr<JointObsController> joint_obs, 
              std::shared_ptr<CollectObs> sensor_obs, std::shared_ptr<TimeSyncController> time_cntr) : 
              mov_prim(mov_prim), joint_obs(joint_obs), sensor_obs(sensor_obs), time_cntr(time_cntr) {
            running = false; 
          }
      };

      TTSingleProMP::TTSingleProMP(std::shared_ptr<MovPrimTask> mov_prim, 
              std::shared_ptr<JointObsController> joint_obs, std::shared_ptr<CollectObs> sensor_obs,
              std::shared_ptr<TimeSyncController> time_cntr) {
        _impl = unique_ptr<Impl>{ new Impl{mov_prim, joint_obs, sensor_obs, time_cntr} };
        _impl->register_methods(*this);
      }

      TTSingleProMP::~TTSingleProMP() = default;

      /**
       * @brief Starts the table tennis task and returns the current time on success
       */
      json TTSingleProMP::start_task(const json& body) {
        if (_impl->conf_ball_model == nullptr || _impl->conf_prior_promp == nullptr || _impl->conf_fwd_kin == nullptr) {
          string err = "The ball model, promp, kinematics and outlier detection "
            "parameters must be passed to the table tennis task before it is started";
          BOOST_LOG_TRIVIAL(error) << err;
          return {{"error", err}};
        }
        BOOST_LOG_TRIVIAL(info) << "The start table tennis task method was called";
        _impl->start();
        return _impl->time_cntr->getTime(body);
      }

      /**
       * @brief Stops the table tennis task and returns the current time on success
       */
      json TTSingleProMP::stop_task(const json& body) {
        _impl->stop();
        return _impl->time_cntr->getTime(body);
      }

      /**
       * @brief Loads ball filter from JSON configuration (Deprecated)
       */
      /*json TTSingleProMP::load_ball_filter(const json& body) {
        return {{"status", "OK"}};
      }*/

      /**
       * @brief Loads a ball model from a JSON configuration
       */
      json TTSingleProMP::load_ball_model(const json& body) {
        auto ball_model = load_traj_pred(body);
        lock_guard<mutex> conf_lock(_impl->conf_mtx);
        _impl->conf_ball_model = ball_model;
        return {{"status","OK"}};
      }

      /**
       * @brief Loads a ProMP prior strike movement
       */
      json TTSingleProMP::load_prior_promp(const json& body) {
        auto full_promp = make_shared<FullProMP>(json2full_promp(body));
        lock_guard<mutex> conf_lock(_impl->conf_mtx);
        _impl->conf_prior_promp = full_promp;
        return {{"status", "OK"}};
      }
      
      /**
       * @brief Loads the parameters of the Barrett forward kinematics from JSON
       */
      json TTSingleProMP::load_barrett_kin(const json& body) {
        auto kin = robotics::create_barrett_wam_kin(body);
        lock_guard<mutex> conf_lock(_impl->conf_mtx);
        _impl->conf_fwd_kin = kin;
        return {{"status", "OK"}};
      }

      /**
       * @brief Returns the JSON task log
       */
      json TTSingleProMP::get_log(const json& body) {
        (void)body;
        std::lock_guard<mutex> log_lock(_impl->log_mtx);
        json ans;
        if (_impl->log_ready) {
          std::swap(ans, _impl->trial_log);
          _impl->log_ready = false;
        }
        return ans;
      }

      /**
       * @brief Configures the method parameters via JSON
       */
      json TTSingleProMP::set_config(const json& body) {
        return _impl->set_config(body);
      }

    };
  };
};
