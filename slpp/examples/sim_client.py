
import numpy as np
import matplotlib.pyplot as plt
import zmq
import json
import time
import math

context = zmq.Context()
sock = context.socket(zmq.REQ)
sock.connect("tcp://localhost:7647")

def query(method, body):
  sock.send(json.dumps({'method': method, 'body': body}))
  res = json.loads( sock.recv() )
  if (res and 'error' in res):
    raise Exception(res['error'])
  return res

def get_time():
  body = {}
  method = 'time/get_time'
  res = query(method, body)
  return res['time']

def go_to(t0,T,qdes,qddes):
  body = {'t0': t0, 'T': T, 'qdes': qdes, 'qddes': qddes}
  method = 'mov_prim/add_go_to'
  res = query(method, body)
  return res['id']

def get_joint_obs():
  method = 'joints/flush'
  body = {}
  return query(method, body)

def add_sim_obs(obs):
  method = 'obs/add_sim_obs'
  body = obs
  return query(method, body)

def flush_sensor_info(sensors):
  method = 'obs/flush'
  body = {'ids': sensors}
  return query(method, body)


#Here begins the test code

def joints_to_numpy(joints):
  q = []
  qd = []
  t = []
  for x in joints:
    q.append(x['q'])
    qd.append(x['qd'])
    t.append(x['t'])
  return np.array(q), np.array(qd), np.array(t)

def plot_goto():
  t0 = get_time() + 0.1
  m1 = go_to(t0,1,[1,1,1,1,-1,-1,-1],[0,0,0,0,0,0,0])
  time.sleep(1.2)
  joints = get_joint_obs()
  q, qd, t = joints_to_numpy(joints)
  for d in xrange(7):
    plt.plot(t,q[:,d])
    plt.show()

def plot_sim_obs():
  t0 = get_time() + 0.1
  obs = []
  N = 400

  for i in xrange(N):
    t = i/float(N)
    obs.append({'sensor_id': 1, 'time': t0 + t, 'obs': [t,1-t,abs(np.sin(2*math.pi*t))]})
  add_sim_obs(obs)

  c_obs = flush_sensor_info([1])
  assert(len(c_obs) == 1 and c_obs[0]['id']==1)
  c_obs = c_obs[0]['values']

  X = []
  t = []
  for o in c_obs:
    if (len(t) > 0): assert(o['time'] > t[-1])
    t.append(o['time'])
    X.append(o['obs'])
  t = np.array(t)
  X = np.array(X)
  for i in xrange(3):
    plt.plot(t,X[:,i])
    plt.show()

plot_goto()
