
#include <robotics/controllers/mov_prim.hpp>
#include <robotics/slpp/net/net_task.hpp>
#include <robotics/slpp/net/mov_prim_task.hpp>
#include <robotics/slpp/net/chrono.hpp>
#include <robotics/slpp/net/table_tennis.hpp>
#include <robotics/controllers/inv_dyn.hpp>
#include <robotics/utils/chrono.hpp>
#include <robotics/callback.hpp>
#include <robotics/slpp/sl.hpp>
#include <armadillo>
#include <iostream>
#include <time.h>


#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

using namespace robotics;
using namespace std;
using namespace arma;
namespace logging = boost::log;

void log_init() {
  logging::add_common_attributes();
  logging::register_simple_formatter_factory< boost::log::trivial::severity_level, char >("Severity");
  logging::add_console_log(std::cout, 
      logging::keywords::auto_flush = true,
      logging::keywords::format = "[%TimeStamp%] <%Severity%>: %Message%"
      );
  logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::info);
}

int main() {
  try {
    /* First create the movement primitive controller */
    log_init();
    shared_ptr<AsyncCallback> cb_handler{ new AsyncCallback };
    auto rob_control = MovPrimController::create("inv_dyn");
    auto task = make_shared<slpp::net::MovPrimTask>(rob_control);

    /* And create a controller that will store the joint observations with a limited buffer size */
    auto joint_obs_controller = make_shared<slpp::net::JointObsController>( 1000, cb_handler ); /* 2 seconds */

    /* Create also a chronometer controller */
    auto p_chrono = make_shared<robotics::Chronometer>();
    auto time_controller = make_shared<slpp::net::TimeSyncController>(p_chrono);
    auto& chrono = *p_chrono;

    /* Create a sensor net controller */
    auto sensor_cntr = make_shared<slpp::net::CollectObs>(cb_handler);
    sensor_cntr->add_sensor(1, 3, 1024, "Test pair of cameras"); //sensor_id 1 with 3D dim
    sensor_cntr->set_timer(time_controller->get_chrono());

    /* Table tennis controller */
    auto tab_tennis_task = make_shared<slpp::net::TableTennisProMP>(task, joint_obs_controller, sensor_cntr,
        time_controller);

    /* Now create the server and register the controllers to it */
    slpp::net::MVCServer server("tcp://*:7647");
    BOOST_LOG_TRIVIAL(info) << "Server up and listening in port 7647";
    server.register_controller("mov_prim", task);
    server.register_controller("joints", joint_obs_controller);
    server.register_controller("time", time_controller);
    server.register_controller("obs", sensor_cntr);
    server.register_controller("table_tennis", tab_tennis_task);

    /* Here we set up the simulation */
    auto controller = [rob_control](void* signal, const void* state, double time) -> void {
      rob_control->control(signal, state, time);
    };
    InvDynJointObs init_cond;
    init_cond.q = init_cond.qd = zeros<vec>(7);
    init_cond.time = chrono();
    InvDynSimulation sim(controller, init_cond);
    sim.set_sim_noise(1e-6,1e-6);
    double deltaT = 0.002;
    timespec sleep_time;
    sleep_time.tv_sec = 0;
    sleep_time.tv_nsec = (long long)(deltaT * 1e9); //turn seconds to nanoseconds

    /* And run the simulation in an infinite loop (Until the user kills the program) */
    BOOST_LOG_TRIVIAL(info) << "Simulation running..." << endl;
    while ( true ) {
      double dt = chrono() - sim.time();
      auto s = sim(dt);
      double t1 = chrono();
      joint_obs_controller->addJointMeasure(conv_to<vector<double>>::from(s.q), 
          conv_to<vector<double>>::from(s.qd), s.time);
      double t2 = chrono();
      if ((t2 - t1) > deltaT) {
        BOOST_LOG_TRIVIAL(warning) << "Adding a joint observation took more time than expected: " << t2-t1 << "s";
      }
      nanosleep(&sleep_time, 0);
    }
  } catch (const std::exception& ex) {
    cout << "Exception: " << ex.what() << endl;
  }
}
