
#include <robotics/slpp.hpp>
#include <memory>
#include <iostream>
#include <zmq.h>
#include <json.hpp>
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE net_controller_test
#include <boost/test/unit_test.hpp>

using namespace std;
using namespace robotics::slpp;
using namespace robotics::slpp::net;
using json = nlohmann::json;

struct ServerFixture {
  unique_ptr<SLNetTask> task;
  unique_ptr<void, function<int(void*)>> context;
  unique_ptr<void, function<int(void*)>> socket;

  json query(const string& method,  const json& body) {
    json j_message;
    //zmqpp::message msg, m_resp;
    j_message["method"] = method;
    j_message["body"] = body;
    string msg = j_message.dump();
    unique_ptr<char[]> buff { new char[1<<12] };
    zmq_send(socket.get(), msg.c_str(), msg.size(), 0);
    int num_bytes = zmq_recv(socket.get(), buff.get(), 1<<12, 0);
    buff[num_bytes] = '\0';
    msg = string(buff.get());
    return json::parse(msg);
  }

  ServerFixture() {
    task = load_net_task_file("test/test_net_config.json");
    const string endpoint = "tcp://localhost:7641";
    context = unique_ptr<void, function<int(void*)>>(zmq_ctx_new(), zmq_ctx_destroy);
    socket = unique_ptr<void, function<int(void*)>>( zmq_socket(context.get(), ZMQ_REQ), zmq_close );
    zmq_connect(socket.get(), endpoint.c_str());
  }

  ~ServerFixture() {
    json empty;
    query("quit", empty);
    socket = nullptr;
    task = nullptr;
    context = nullptr;
  }
};


BOOST_FIXTURE_TEST_SUITE( net_controller_suite, ServerFixture )

  BOOST_AUTO_TEST_CASE( obs_controller1 ) {
    const double obs1[] = {1.0,-1.0,0.5};
    task->sl_object->add_obs(1, 0.0, obs1);
    BOOST_CHECK( !task->sl_object->is_obs_new(1, obs1) );
    BOOST_CHECK( task->sl_object->is_obs_new(2, obs1) );
    task->sl_object->add_obs(2, 0.01, obs1);
    BOOST_CHECK( !task->sl_object->is_obs_new(2, obs1) );

    json req;
    vector<int> req_ids {1,2,10};
    req["ids"] = req_ids;
    json ans = query("obs/flush", req);
    cout << ans.dump() << endl;
    for (auto it : ans) {
      BOOST_REQUIRE(it.count("id") == 1);
      unsigned int id = it.at("id");
      if (id != 10) {
        BOOST_REQUIRE(it.count("values") == 1);
        auto vals = it.at("values");
        BOOST_CHECK(vals.size() == 1);
      }
    }

    ans = query("obs/clear", req_ids);
    cout << ans.dump() << endl;
    BOOST_CHECK( ans.size() == 0 );

    ans = query("obs/flush", json());
    cout << ans.dump() << endl;
    BOOST_CHECK( ans.size() == 0 );
  }

  BOOST_AUTO_TEST_CASE( time_controller ) {
    json body;
    json ans = query("time/get_time", body);
    BOOST_REQUIRE( ans.count("error") == 0 );
    BOOST_REQUIRE( ans.count("time") == 1 );
    cout << ans["time"] << endl;
  }

BOOST_AUTO_TEST_SUITE_END()
