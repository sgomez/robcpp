%module table_tennis
%{
#define SWIG_FILE_WITH_INIT
#include <robotics/table_tennis.hpp>
#include <robotics.hpp>
#include <vector>
#include <array>
#include <unordered_map>

using namespace robotics;
using namespace robotics::table_tennis;

%}

%include "armanpy.i"
%include "std_vector.i"
%include "std_array.i"
%include "std_string.i"

namespace std {
  %template(list_vec) vector<arma::vec>;
};

namespace robotics {


  /**
   * A set of methods to produce random numbers that can be used in conjunction with the standard
   * library for random numbers. It also contains a set of definitions of probability distributions.
   * @brief Random number generation and probability distribution utilities
   */ 
  namespace random {

    /**
     * @brief Represents a Multivariate Normal (Gaussian) distribution
     */
    class NormalDist {
      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
      public:
        NormalDist(const arma::vec& mean, const arma::mat& cov);
        NormalDist(const NormalDist& b);
        ~NormalDist();

        const arma::vec& mean() const; //!< Mean of a normal distribution
        const arma::mat& cov() const; //!< Covariance matrix of a normal distribution
        const arma::mat& inv_cov() const; //!< Inverse of the covariance matrix
        const arma::mat& chol_cov() const; //!< Cholesky decomposition R such that R.t()*R = covariance
        double log_det_cov() const; //!< Log of the determinant of the covariance matrix

        void set_mean(const arma::vec& mean); //!< Sets the mean of the normal distribution
        void set_cov(const arma::mat& cov); //!< Sets the covariance matrix of the normal distribution
    };

    /**
     * @brief Estimates a Gaussian distribution from samples using maximum likelihood estimation.
     */
    NormalDist mle_multivariate_normal(const std::vector<arma::vec>& samples);

    double log_normal_overlap(const random::NormalDist& dist1, const random::NormalDist& dist2);
    double log_normal_density(const random::NormalDist& dist, const arma::vec& x);
  };

  /**
   * State space model representation as a Gaussian distribution. Construct this class with an
   * initial distribution for the state (mean and covariance).
   * Then call observe to incorporate observations to the state distribution or next to obtain
   * a distribution after updating it according to the model dynamics.
   * @brief Gaussian state for state space models
   * @since version 0.0.1
   */ 
  class LSSGaussState {
    public:
      LSSGaussState();
      LSSGaussState(const random::NormalDist& dist);
      LSSGaussState(const arma::vec init_mean, const arma::mat init_cov);
      LSSGaussState(const LSSGaussState& b);
      ~LSSGaussState();

      /*LSSGaussState observe(const AbstractLSSModel& dynamics, const arma::vec& obs, 
          const arma::vec& bias, const void* params=nullptr) const;

      LSSGaussState next(const AbstractLSSModel& dynamics, const arma::vec& action, 
          const void* params=nullptr) const;

      random::NormalDist obs_dist(const AbstractLSSModel& dynamics, const arma::vec& bias, 
          const void* params=nullptr) const;

      double obs_mah_dist(const AbstractLSSModel& dynamics, const arma::vec& obs, 
          const arma::vec& bias, const void* params=nullptr) const;*/

      const arma::vec& mean() const;
      const arma::mat& cov() const;
      const random::NormalDist& dist() const;
    private:
      class Impl;
      std::unique_ptr<Impl> _impl;
  };


  namespace table_tennis {

    /**
     * This class implements a generic ball model. The particular implementations for the
     * ball models are in the header file raw_ball_models.h. This class provide a simple
     * interface for adding new ball observations to the state and advancing it in time.
     * @brief Generic ball model with a simple API
     */
    class BallModel {
      public:
        BallModel();
        //BallModel(const LSSGaussState& init_st, std::shared_ptr<const AbstractLSSModel> dyn);
        //BallModel(const Table& table, const std::string& conf_json);
        BallModel(const BallModel& b);
        ~BallModel();

        /*LSSGaussState new_traj() const;
        LSSGaussState advance(const LSSGaussState& state, double deltaT) const;
        LSSGaussState observe(const LSSGaussState& state, const arma::vec& obs) const;
        double obs_mah_dist(const LSSGaussState& state, const arma::vec& obs) const;

        arma::vec mean_position(const LSSGaussState& state) const;
        random::NormalDist position_dist(const LSSGaussState& state) const;

        std::vector<arma::vec> sample_ball_traj(const LSSGaussState& init, unsigned int len, double deltaT);
        std::vector<arma::vec> sample_ball_obs(const std::vector<arma::vec>& ball_traj);*/

      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
    };

    BallModel load_ball_model_obj(const std::string& json);
    arma::mat sample_ball_traj_obs(const BallModel& model, unsigned int len, double deltaT);

  };

};
