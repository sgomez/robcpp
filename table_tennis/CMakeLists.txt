cmake_minimum_required(VERSION 3.5)
project(robotics_table_tennis)

find_package(Armadillo REQUIRED)
find_library(ROB_LIB
  NAMES robotics)
find_path(ROB_INCLUDES 
  NAMES robotics.hpp )

add_definitions(-DBOOST_LOG_DYN_LINK)
find_package(Boost 1.58 COMPONENTS log REQUIRED)
include_directories( ${Boost_INCLUDE_DIR} )

option (PYLIB "Create a Python Module with interface to some of the C++ implementations" ON)

include_directories(
  include
  ${ROB_INCLUDES}
  ${Boost_INCLUDE_DIR}
  )
add_library(table_tennis SHARED
  src/table_tennis/table.cpp
  src/table_tennis/ball.cpp
  src/table_tennis/ball_filter.cpp
  src/table_tennis/time_series.cpp
  src/table_tennis/segment.cpp
  src/table_tennis/hit.cpp
  src/table_tennis/ball_filters/filter_ransac_3d.cpp
  src/table_tennis/ball_filters/kalman_outlier_3d.cpp
  src/table_tennis/raw_ball_models/lgbm1.cpp
  src/table_tennis/utils.cpp
  src/table_tennis/env_state.cpp
  src/table_tennis/c_api/table_tennis.cpp
  )
target_link_libraries(table_tennis
  ${ARMADILLO_LIBRARIES}
  ${ROB_LIB}
  ${Boost_LIBRARIES}
  )

if(NOT MSVC AND (("${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU") OR ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")))
  include(CheckCXXCompilerFlag)
  check_cxx_compiler_flag("-std=c++11" COMPILER_SUPPORTS_CXX11)
  if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
  else()
    message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
  endif()

  if("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
  endif()
endif()

add_subdirectory(test)
add_subdirectory(examples)
enable_testing ()
add_test (NAME TableTennisSetup COMMAND TestTableTennisSetup WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
add_test (NAME BallModels COMMAND TestBallModels WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
add_test (NAME TestTTUtils COMMAND TestTTUtils  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

install(TARGETS table_tennis DESTINATION lib)
install(DIRECTORY include/robotics DESTINATION include)

if (PYLIB)
  find_package(SWIG REQUIRED)
  include( ${SWIG_USE_FILE} )

  find_package(PythonLibs 2.7 REQUIRED)
  include_directories(${PYTHON_INCLUDE_PATH})

  find_path(ARMANPY_INC armanpy.i)
  include_directories(${ARMANPY_INC})

  set_source_files_properties( modules/table_tennis.i PROPERTIES CPLUSPLUS ON)
  set_source_files_properties( modules/table_tennis.i PROPERTIES SWIG_FLAGS "-includeall" )
  swig_add_module(py_table_tennis python modules/table_tennis.i)
  swig_link_libraries(py_table_tennis ${PYTHON_LIBRARIES} ${ARMADILLO_LIBRARIES} table_tennis)
  configure_file(./modules/setup.py ./setup.py)
  install(TARGETS _py_table_tennis DESTINATION lib)

endif (PYLIB)
