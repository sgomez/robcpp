#ifndef ROB_TABTENNIS_UTILS
#define ROB_TABTENNIS_UTILS

#include <json.hpp>
#include <string>
#include "robotics/table_tennis/table.hpp"
#include "robotics/table_tennis/ball.hpp"

namespace robotics {
  namespace table_tennis {

    Table json2table(const nlohmann::json& obj);

    Table loadTable(const std::string& f_name);
    std::shared_ptr<BallModel> loadBallModel(const Table& table, const std::string& f_name);

  };
};

#endif
