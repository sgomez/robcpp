#ifndef ROB_TABTENNIS_RAW_BALL_MODELS
#define ROB_TABTENNIS_RAW_BALL_MODELS

#include <memory>
#include <armadillo>
#include "robotics/table_tennis/table.hpp"
#include "robotics/lss_model.hpp"
#include "json.hpp"

namespace robotics {
  namespace table_tennis {

    /**
     * @brief Params for the raw ball model LGBM1
     */ 
    struct LGBM1Params {
      double default_deltaT;
      arma::vec air_drag;
      arma::vec bounce_fac;
      double gravety;
      arma::mat fly_obs_noise;
      arma::mat fly_trans_noise;
      arma::mat bounce_obs_noise;
      arma::mat bounce_trans_noise;
    };

    LGBM1Params json2LGBM1Params(const nlohmann::json& j);

    /**
     * Linear Gaussian Ball Model representation class. In the assumed dynamical model, when the
     * ball is flying the only forces acting on X and Y axis are the air drag and in the Z axis the air
     * drag plus the gravety. 
     * When the ball bounces on the table the velocities in X, Y and Z are all reduced proportionally 
     * to the bouncing factor. The position is mantained in Z and and updated in X and Y according to
     * the original velocity.
     * For the additional parameters of the transition matrices function (The varable params) pass a
     * pointer to a double precision value containing deltaT.
     */ 
    class LGBM1 : public AbstractLSSModel {
      public:
        LGBM1();
        LGBM1(const LGBM1Params& params, const Table& table);
        LGBM1(const LGBM1& b);
        LGBM1(LGBM1&& b);
        LGBM1& operator=(const LGBM1& b);

        ~LGBM1();
        arma::mat transition(const LSSGaussState& state, const void* params) const;
        arma::mat action_mat(const LSSGaussState& state, const void* params) const;
        arma::mat obs_mat(const LSSGaussState& state, const void* params) const;
        arma::mat obs_bias(const LSSGaussState& state, const void* params) const;
        arma::mat obs_noise(const LSSGaussState& state, const void* params) const;
        arma::mat trans_noise(const LSSGaussState& state, const void* params) const;
        unsigned int dim_hidden() const;
        unsigned int dim_obs() const;
      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
    };

  };
};

#endif
