#ifndef ROB_TABTENNIS_ENV_STATE_H
#define ROB_TABTENNIS_ENV_STATE_H

/**
 * @file
 * This file proportions a C API to manage the environment observations of a robot table tennis 
 * task. As with the rest of this library the C API functions start with rt meaning
 * "robot task". The prefix of the functions of this file in particular is "idtt" that stands
 * for "inverse dynamics (controller based for) table tennis".
 */

#ifdef __cplusplus
extern "C" {  
#endif

  /**
   * This struct represents a table tennis ball observation.
   */
  struct TTBallObs {
    double pos[3]; //< Ball position in (X,Y,Z) coordinates
    double time; //< Time of this particular ball observation
    unsigned int sensor_id; //< Id of the sensor that produced this particular ball observation
    unsigned int dims; //< Number of dimensions of the location of the ball observation
  };

  void* tt_envobs_init();
  void tt_envobs_clean(void* envobs);
  void tt_envobs_free(void* envobs);

  void tt_envobs_add_ball(void* envobs, const struct TTBallObs* obs);
  int tt_envobs_add_sl_ball(void* envobs, const struct TTBallObs* obs);
#ifdef __cplusplus
}
#endif

#endif
