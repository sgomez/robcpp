#ifndef ROB_TABTENNIS_HIT
#define ROB_TABTENNIS_HIT

#include <memory>
#include <string>
#include <armadillo>
#include <vector>
#include <robotics/lss_model.hpp>
#include <robotics/table_tennis/ball.hpp>
#include <robotics/utils/random.hpp>

namespace robotics {
  namespace table_tennis {

    double golden_optimization(const std::function<double(double t)>& f, double t_a, double t_b, double tol);

    double hit_likelihood(const random::NormalDist& ball_pos, const random::NormalDist& racket_pos);

    double hit_log_likelihood(const random::NormalDist& ball_pos, const random::NormalDist& racket_pos);

    std::vector<double> log_overlaps(const std::vector<std::pair<double,random::NormalDist>>& ball_pos, 
        const std::function<random::NormalDist(double z)>& racket_pos, double t0, double T,
        const std::function<double(double z)>& log_prior_z);

    double log_mar_hit_lh(const std::vector<std::pair<double,random::NormalDist>>& ball_pos, 
        const std::function<random::NormalDist(double z)>& racket_pos, double t0, double T,
        const std::function<double(double z)>& log_prior_z);

    double opt_start_time(const std::vector<std::pair<double,random::NormalDist>>& ball_pos, 
        const std::function<random::NormalDist(double z)>& racket_pos, double T,
        const std::function<double(double z)>& log_prior_z);

    /**
     * @brief Compute the log overlap between the ball and racket
     * 
     * Compute the log overlap between the ball and racket, but stores intermediate calculations that 
     * can be used for analysis of the trial after execution.
     */
    class LogOverlaps {
      public:
        LogOverlaps(const std::function<random::NormalDist(double z)>& racket_pos,
            const std::function<double(double z)>& log_prior_z);
        ~LogOverlaps();

        std::vector<double> operator()(const std::vector<std::pair<double, random::NormalDist>>& ball_pos,
            double t0, double T);
        void set_racket(const std::function<random::NormalDist(double z)>& racket_pos);
        const std::vector<std::pair<double, random::NormalDist>>& get_racket_dist() const;
      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
    };

  };
};

#endif
