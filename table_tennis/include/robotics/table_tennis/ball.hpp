#ifndef ROB_TABTENNIS_BALL
#define ROB_TABTENNIS_BALL

#include <memory>
#include <string>
#include <armadillo>
#include <robotics/utils/random.hpp>
#include <robotics/table_tennis/table.hpp>
#include <robotics/lss_model.hpp>
#include <json.hpp>

namespace robotics {
  namespace table_tennis {

    /**
     * This class implements a generic ball model. The particular implementations for the
     * ball models are in the header file raw_ball_models.h. This class provide a simple
     * interface for adding new ball observations to the state and advancing it in time.
     * @brief Generic ball model with a simple API
     */
    class BallModel {
      public:
        BallModel();
        BallModel(const LSSGaussState& init_st, std::shared_ptr<const AbstractLSSModel> dyn);
        //BallModel(const Table& table, const std::string& conf_json);
        BallModel(const BallModel& b);
        BallModel(BallModel&& b);
        ~BallModel();
        BallModel& operator=(const BallModel& b);
        BallModel& operator=(BallModel&& b);

        LSSGaussState new_traj() const;
        LSSGaussState advance(const LSSGaussState& state, double deltaT) const;
        LSSGaussState observe(const LSSGaussState& state, const arma::vec& obs) const;
        double obs_mah_dist(const LSSGaussState& state, const arma::vec& obs) const;

        arma::vec mean_position(const LSSGaussState& state) const;
        random::NormalDist position_dist(const LSSGaussState& state) const;

        std::vector<arma::vec> sample_ball_traj(const LSSGaussState& init, unsigned int len, double deltaT) const;
        std::vector<arma::vec> sample_ball_obs(const std::vector<arma::vec>& ball_traj) const;

      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
    };

    std::shared_ptr<BallModel> load_ball_model(const Table& table, const nlohmann::json& ball_model_params);
    std::shared_ptr<BallModel> load_ball_model(const nlohmann::json& conf);
    std::shared_ptr<BallModel> load_ball_model(const std::string& conf);
    std::shared_ptr<AbstractLSSModel> load_ball_dynamics(const Table& table, const nlohmann::json& conf);

    BallModel load_ball_model_obj(const std::string& conf);
    arma::mat sample_ball_traj_obs(const BallModel& model, unsigned int len, double deltaT);

  };
};

#endif
