#ifndef ROB_TABTENNIS_SEGMENT
#define ROB_TABTENNIS_SEGMENT

#include <json.hpp>
#include <memory>
#include <vector>
#include <armadillo>

namespace robotics {
  namespace table_tennis {

    class Segmenter {
      public:
        virtual ~Segmenter() = default;
        virtual void update_config(const nlohmann::json& conf) = 0;
        virtual nlohmann::json segm_meta_data(const nlohmann::json& raw_data) = 0;
        virtual nlohmann::json get_segments(const nlohmann::json& raw_data, 
            const nlohmann::json& segm_meta,
            const nlohmann::json& options) = 0;
    };

    class Segment : public Segmenter {
      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
      public:
        Segment();
        ~Segment();
        void update_config(const nlohmann::json& conf);
        nlohmann::json segm_meta_data(const nlohmann::json& raw_data);
        nlohmann::json get_segments(const nlohmann::json& raw_data, 
            const nlohmann::json& segm_meta,
            const nlohmann::json& options);
    };

    struct TrialConf {
      double max_ball_dist; //!< Maximum ball distance between different sensors
      double max_vel; //!< Maximum velocity
      double time_bw_trials; //!< Time not seeing the ball between games
      double time_bw_subtraj; //!< Time between sub-trajectories inside a trial
      unsigned int min_subseq_obs; //!< Minimum number of observations in a sub-sequence
      unsigned int max_subseq_obs; //!< Maximum number of observations in a sub-sequence
      bool verbose; //!< Should this script print debug information?
      unsigned int window_size; //!< Bounce detection window size
      double win_tol; //window tolerance to avoid detecting plateau as minima
      std::vector<double> court;

      TrialConf();
      void update(const nlohmann::json& conf);
    };

    nlohmann::json segm_game_in_trials(const std::vector<double>& game_ball_time,
        const std::vector<arma::vec>& game_ball_pos, const TrialConf& conf);

    class PlaySegmenter : public Segmenter {
      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
      public:
        PlaySegmenter();
        ~PlaySegmenter();
        void update_config(const nlohmann::json& conf);
        nlohmann::json segm_meta_data(const nlohmann::json& raw_data);
        nlohmann::json get_segments(const nlohmann::json& raw_data, 
            const nlohmann::json& segm_meta,
            const nlohmann::json& options);
    };
  };
};

#endif
