#ifndef ROB_TABTENNIS_BALL_FILTER
#define ROB_TABTENNIS_BALL_FILTER

#include <memory>
#include <string>
#include <armadillo>
#include <functional>
#include <robotics/table_tennis/ball.hpp>
#include <robotics/utils.hpp>
#include <json.hpp>

namespace robotics {
  namespace table_tennis {

    /**
     * @brief Abstract class that can be used to provide several different implementations of robust
     * ball filters.
     */
    class OutlierBallFilter {
      public:
        virtual ~OutlierBallFilter() = default;

        /**
         * @brief Resets the ball filter to its initial condition (Before adding any ball observations)
         */
        virtual void reset() = 0;

        /**
         * @brief Adds a ball observation to the internal state
         */
        virtual void add_obs(unsigned int sensor, double time, const double* ball_obs) = 0;

        /**
         * @brief Returns the believed read state of the ball as a Gaussian distribution. Do not
         * call this method if ready() returns false.
         */
        virtual LSSGaussState state() const = 0;

        /**
         * @brief Returns the time stamp of the last accepted ball observation
         */
        virtual double last_time() const = 0;

        /**
         * @brief Returns true if the ball state is available.
         */
        virtual bool ready() const = 0;
    };

    /**
     * This namespace contains different implementations of ball outlier filters.
     */
    namespace ball_filters {

      class FilterRansac3D : public OutlierBallFilter {
        public:
          struct Params {
            unsigned int min_support; //!< Don't accept solutions with less than this number of inliers
            unsigned int max_buff_size; //!< Don't process with RANSAC more than this number of observations
            double max_time_gap; //<! Maximum allowed time gap between consecutive balls
            RansacConf rans_conf; //!< Configuration for the RANSAC algorithm
          };
        private:
          class Impl;
          std::unique_ptr<Impl> _impl;
        public:
          FilterRansac3D(std::shared_ptr<BallModel> ball_model);
          ~FilterRansac3D();
          void set_params(const Params& params);
          Params& params();
          void reset();
          void add_obs(unsigned int sensor, double time, const double* ball_obs);
          LSSGaussState state() const;
          double last_time() const;
          bool ready() const;
      };

      class KalmanOutlier3D : public OutlierBallFilter {
        public:
          struct Params {
            FilterRansac3D::Params ransac_params; //!< Configuration for the RANSAC based ball filter in 3D
            double max_mah_dist; //!< Maximum mahalanobis distance accepted for considering an observation
          };
        private:
          class Impl;
          std::unique_ptr<Impl> _impl;
        public:
          KalmanOutlier3D(std::shared_ptr<BallModel> ball_model);
          ~KalmanOutlier3D();
          void set_params(const Params& params);
          Params& params();
          void reset();
          void add_obs(unsigned int sensor, double time, const double* ball_obs);
          LSSGaussState state() const;
          double last_time() const;
          bool ready() const;
      };

      std::shared_ptr<KalmanOutlier3D> load_kalman_outlier_3d(std::shared_ptr<BallModel> ball_model, 
          const nlohmann::json& conf);
      KalmanOutlier3D::Params load_kalman_outlier_3d_params(const nlohmann::json& conf);
    };

  };
};

#endif
