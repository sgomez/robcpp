#ifndef ROB_BALL_H
#define ROB_BALL_H

#ifdef __cplusplus
extern "C" {  
#endif
  void* rob_tt_load_table(const char* fname);
  void rob_tt_free_table(void *table);
  void* rob_tt_load_ball_model(const void *table, const char* fname);
  void rob_tt_free_ball_model(void* ball_model);
  void* rob_tt_new_ball_traj(const void* ball_model);
  void rob_tt_ball_advance_traj(void* ball_state, const void* ball_model, double deltaT);
  void rob_tt_ball_observe(void* ball_state, const void* ball_model, const double* obs);
  double rob_tt_ball_mah_dist(const void* ball_state, const void* ball_model, const double* obs);
  void rob_tt_ball_position(double* position, const void* ball_state, const void* ball_model);
#ifdef __cplusplus  
} // extern "C"  
#endif

#endif
