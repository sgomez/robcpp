#ifndef ROB_TABTENNIS_TIME_SERIES
#define ROB_TABTENNIS_TIME_SERIES

#include <armadillo>
#include <vector>
#include <functional>
#include <memory>

namespace robotics {
  namespace table_tennis {
    
    struct TSeries {
      std::vector<double> time; //!< Time
      std::vector<arma::vec> y; //!< Vector observations
    };

    std::vector<unsigned int> split_by_time(const std::vector<double>& times, double max_time);

    /**
     * @brief Type for a function that returns true if the two given nighbouring 
     * elements make sense
     */
    using is_sensible_func = std::function<bool(double t1, const arma::vec& y1, 
        double t2, const arma::vec& y2)>;

    /**
     * @brief Find subsequences of maximal length that make sense according to some criteria
     */
    class SensibleSubseq {
      public:
        /**
         * @brief Criteria used to segment and mark outliers
         */
        struct Criteria {
          double max_time_gap; //!< Maximum time interval without observations in a subsequence
          is_sensible_func is_sensible; //!< Sensible sequence criteria
        };

        SensibleSubseq(const std::vector<double>& time, const std::vector<arma::vec>& y, 
            const Criteria& conf);
        ~SensibleSubseq();

        unsigned int size_subseq(unsigned int first_elem) const;
        std::vector<unsigned int> get_subseq(unsigned int first_elem) const;
        std::vector<unsigned int> get_subseq(unsigned int first_elem, 
            const std::function<bool(unsigned int)>& can_use) const;
      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
    };

    std::vector<unsigned int> get_outliers(const std::vector<unsigned int>& inliers, unsigned int N);
    std::pair<unsigned int, unsigned int> zero_cross_vel(const std::vector<arma::vec>& q_dot, 
        unsigned int joint_id=0, double zero_tol=0.01);
    std::vector<double> extract_coordinate(const std::vector<arma::vec>& y, unsigned int ix);
    std::vector<unsigned int> detect_local_min(const std::vector<double>& z, 
        unsigned int window_size, double tol);
    int main_direction(const std::vector<double>& y);

  };
};

#endif
