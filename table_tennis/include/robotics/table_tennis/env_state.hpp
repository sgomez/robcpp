#ifndef ROB_TABTENNIS_ENV_STATE_HPP
#define ROB_TABTENNIS_ENV_STATE_HPP

#include <memory>
#include <deque>
#include <functional>
#include "env_state.h"

namespace robotics {
  namespace table_tennis {

    /**
     * This class is a container for the environment observations on a table tennis task like the
     * ball information. Objects of this class are meant to be used from several tasks and thus
     * its methods are implemented to be thread safe using the standard thread library.
     * @brief Environment state for a table tennis task.
     */
    class EnvObs {
      public:
        EnvObs();
        ~EnvObs();

        void clear();
        void add_ball_obs(const TTBallObs& obs);
        bool add_sl_ball_obs(const TTBallObs& obs);
        std::deque<TTBallObs> flush_ball_obs();
        unsigned int num_ball_obs() const;
        void wait_new_balls();
      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
    };
  };
};

#endif
