#ifndef ROB_TABTENNIS_TABLE
#define ROB_TABTENNIS_TABLE

#include <memory>
#include <armadillo>

namespace robotics {

  namespace table_tennis {

    /**
     * Represents a table tennis table, assuming that the height (value on the Z coordinate) of the
     * table is constant.
     * @since version 0.0
     * @brief Table tennis table with constant height.
     */ 
    class Table {
      public:
        Table();
        Table(double width, double len, const arma::vec& center, double net_height);
        Table(const Table& b);
        Table(Table&& b);
        Table& operator=(const Table& b);
        Table& operator=(Table&& b);
        bool segm_intersect_table(const arma::vec& from, const arma::vec& to) const;
        const arma::vec& center() const;
        ~Table();
      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
    };
  };
};

#endif
