
#include <robotics/table_tennis/env_state.hpp>
#include <armadillo>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <json.hpp>
#include <fstream>
#include <string>
#include <unordered_map>
#include <thread>
#include <mutex>

using namespace robotics;
using namespace robotics::table_tennis;
using namespace arma;
using namespace std;
using json = nlohmann::json;

#define EPS 1e-8

bool pos_equal(const TTBallObs& a, const TTBallObs& b) {
  if (a.dims != b.dims) return false;
  for (unsigned int i=0; i<a.dims; i++) {
    if ( fabs(a.pos[i] - b.pos[i]) > 1e-8 ) return false;
  }
  return true;
}

BOOST_AUTO_TEST_CASE( TestRepeatedBallObs ) {
  EnvObs env;
  for (int i=0; i<10; i++) {
    env.add_sl_ball_obs({{0,0,0},1e-3*i,1,3});
    env.add_sl_ball_obs({{0,1e-8,0},1e-3*i,2,3});
    env.add_sl_ball_obs({{100,100,0},1e-3*i,1,2});
    env.add_sl_ball_obs({{150,80,0},1e-3*i,2,2});
  }
  env.add_sl_ball_obs({{0.1,0,0}, 0.2, 1, 3});
  env.add_sl_ball_obs({{100,101,0}, 0.2, 1, 2});

  BOOST_CHECK(env.num_ball_obs() == 6);
  deque<TTBallObs> ball_obs = env.flush_ball_obs();
  BOOST_CHECK(ball_obs.size() == 6);
  BOOST_CHECK(env.num_ball_obs() == 0);
  BOOST_CHECK(pos_equal(ball_obs[0], {{0,0,0},0,1,3}));
  BOOST_CHECK(pos_equal(ball_obs[1], {{0,1e-8,0},0,2,3}));
  BOOST_CHECK(pos_equal(ball_obs[2], {{100,100,0},0,1,2}));
  BOOST_CHECK(pos_equal(ball_obs[3], {{150,80,0},0,1,2}));
  BOOST_CHECK(pos_equal(ball_obs[4], {{0.1,0,0},0.2,1,3}));
  BOOST_CHECK(pos_equal(ball_obs[5], {{100,101,0},0.2,1,2}));
  BOOST_CHECK(env.num_ball_obs() == 0);
}
