
#include <robotics/table_tennis/hit.hpp>
#include <robotics/utils/random.hpp>
#include <armadillo>
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <json.hpp>
#include <fstream>
#include <string>
#include <unordered_map>

using namespace robotics;
using namespace robotics::table_tennis;
//using namespace robotics::table_tennis::ball_filters;
using namespace arma;
using namespace std;
using json = nlohmann::json;

BOOST_AUTO_TEST_CASE( golden_opt_test ) {
  //The minimum of cos(t) is in PI
  auto f = [](double t) -> double { return cos(t); };
  double opt_t = golden_optimization(f, 0, 6, 1e-6);
  double pi = acos(-1);
  BOOST_CHECK_MESSAGE( fabs(opt_t-pi) < 1e-5, 
      "Error in golden optimization. Expected " << pi << " but obtained " << opt_t);
}

BOOST_AUTO_TEST_CASE( hit_likelihood_test ) {
  unsigned int num_samples = 10000; //Precise to 2 figures because error decrease with sqrt(N)
  unsigned int seed = 0;
  mt19937 gen(seed);
  mat cov_racket {{1,0,0},{0,0.7,0},{0,0,0.94}};
  mat cov_ball {{0.94,0,0},{0,0.77,0},{0,0,1}};
  mat rot{{0.36,0.48,-0.8},{-0.8,0.6,0},{0.48,0.64,0.6}};
  random::NormalDist dist1{ vec{1,2,3}, rot.t()*cov_racket*rot };
  random::NormalDist dist2{ vec{1.1,2,2.9}, rot.t()*cov_ball*rot };

  double monte_carlo_overlap = 0.0;
  vector<vec> samples = random::sample_multivariate_normal(gen, dist2, num_samples);
  for (unsigned int i=0; i<num_samples; i++) {
    monte_carlo_overlap += exp(log_normal_density(dist1, samples[i]));
  }
  monte_carlo_overlap /= num_samples;

  double analytical_overlap = exp(log_normal_overlap(dist1, dist2));
  cout << "Dist overlap: mc=" << monte_carlo_overlap << " analytic=" << analytical_overlap << endl;
  BOOST_CHECK_MESSAGE( fabs(monte_carlo_overlap - analytical_overlap) < 1e-3,
      "Analytic and Montecarlo estimate of the overlap is different");
}

BOOST_AUTO_TEST_CASE( opt_time_test ) {
  mat cov_mats = eye(3,3);
  vector<pair<double,random::NormalDist>> balls;
  auto racket_dist = [](double z) -> random::NormalDist {
    return random::NormalDist(vec{10,10*z-5,0}, eye(3,3));
  };
  for (double t=8; t<15; t+=0.02) {
    balls.push_back(make_pair(t, random::NormalDist(vec{1.0*t,0,0}, cov_mats)));
  }

  double T = 1.0;
  double t0 = opt_start_time(balls, racket_dist, T, nullptr);
  double true_t0 = 9.5;
  cout << "Found start time: " << t0 << endl;
  cout << "Value: " << log_mar_hit_lh(balls, racket_dist, t0, T, nullptr) << endl;
  cout << "True opt value: " << log_mar_hit_lh(balls, racket_dist, true_t0, T, nullptr) << endl;
  BOOST_CHECK_MESSAGE( fabs(t0-true_t0) < 1e-2, "Wrong start time" );
}
