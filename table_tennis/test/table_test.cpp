
#include <robotics/table_tennis/table.hpp>
#include <armadillo>
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TableTennisSetup
#include <boost/test/unit_test.hpp>
#include <json.hpp>
#include <fstream>
#include <string>
#include <unordered_map>

using namespace robotics;
using namespace robotics::table_tennis;
using namespace arma;
using namespace std;
using json = nlohmann::json;

#define EPS 1e-8

BOOST_AUTO_TEST_CASE( TableIntersectSegment ) {
  vec table_center{ 0.0, -2.13, -0.99 };
  Table t(1.526, 2.74, table_center, 0.1525);

  //Balls that should intersect the table
  BOOST_CHECK(t.segm_intersect_table(vec{0.0,-2.13,-0.9}, vec{0.0,-2.13,-1.08}));
  BOOST_CHECK(t.segm_intersect_table(vec{-0.1,-3,-0.9}, vec{0.1,-2.8,-1}));
  BOOST_CHECK(t.segm_intersect_table(vec{-0.7,-3,-0.9}, vec{-0.8,-2.8,-1.1})); //very close border
  BOOST_CHECK(t.segm_intersect_table(vec{0.0,-0.9,-0.9}, vec{0.1,-0.8,-1.1}));


  //Balls that should not intersect the table
  //Because they move parallel
  BOOST_CHECK(!t.segm_intersect_table(vec{0.0,-3,-0.6}, vec{0.0,-2,-0.6}));
  BOOST_CHECK(!t.segm_intersect_table(vec{-0.1,-3,-0.6}, vec{0.1,-3,-0.6}));
  BOOST_CHECK(!t.segm_intersect_table(vec{-0.1,-3,-0.6}, vec{0.1,-2,-0.6}));
  //Because the segment did not get to the table
  BOOST_CHECK(!t.segm_intersect_table(vec{-0.1,-3,-0.6}, vec{0.1,-2.8,-0.7}));
  BOOST_CHECK(!t.segm_intersect_table(vec{-0.1,-3,-0.8}, vec{0.1,-2.8,-0.9}));
  //Because it goes off the table limits
  BOOST_CHECK(!t.segm_intersect_table(vec{-0.7,-3,-0.9}, vec{-0.9,-2.8,-1.1}));
  BOOST_CHECK(!t.segm_intersect_table(vec{0.0,-0.7,-0.9}, vec{0.1,-0.4,-1.1}));
}
