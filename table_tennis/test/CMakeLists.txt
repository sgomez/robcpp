find_package(Boost COMPONENTS unit_test_framework REQUIRED)
add_executable(TestTableTennisSetup
  table_test.cpp
  env_state_test.cpp
  hit_test.cpp
  )
add_executable(TestBallModels
  ball_test.cpp
  )
target_link_libraries(TestTableTennisSetup
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
  table_tennis
  )
target_link_libraries(TestBallModels
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
  table_tennis
  )

add_executable(TestTTUtils
  time_series_test.cpp
  )
target_link_libraries(TestTTUtils
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
  table_tennis
  )

