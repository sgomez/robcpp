
#include "robotics/table_tennis/ball.hpp"
#include "robotics/table_tennis/table.hpp"
#include "robotics/table_tennis/utils.hpp"
#include "robotics/table_tennis/ball_filter.hpp"
#include "robotics/table_tennis/raw_ball_models.hpp"
#include <armadillo>
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE BallModels
#include <boost/test/unit_test.hpp>
#include <json.hpp>
#include <fstream>
#include <string>
#include <unordered_map>

using namespace robotics;
using namespace robotics::table_tennis;
using namespace robotics::table_tennis::ball_filters;
using namespace arma;
using namespace std;
using json = nlohmann::json;

#define EPS 1e-8

vector<vec> some_ball_data() {
  vector<vec> ans = {vec{0.6698, -3.9281, -0.6092},
    vec{0.6075, -3.7815, -0.5529},
    vec{0.5466, -3.6346, -0.5059},
    vec{0.5092, -3.5445, -0.4821},
    vec{0.4803, -3.4734, -0.4669},
    vec{0.4531, -3.3996, -0.4557},
    vec{0.3886, -3.2397, -0.436},
    vec{0.3341, -3.107,  -0.4373},
    vec{0.307, -3.0434, -0.4397},
    vec{0.2808, -2.9793, -0.4458},
    vec{0.2198, -2.8329, -0.4697},
    vec{0.195, -2.7735, -0.4815},
    vec{0.1697, -2.7111, -0.499},
    vec{0.145, -2.6512, -0.517},
    vec{0.1207, -2.5937, -0.5391}};
  return ans;
}

double distance(const vec& p1, const vec& p2) {
  vec diff = p1-p2;
  return sqrt(dot(diff, diff));
}

BOOST_AUTO_TEST_CASE( TestLGBM1 ) {
  Table t = loadTable("examples/models/table.txt");
  BallModel m = *loadBallModel(t, "examples/models/trained_kf.txt");
  double max_mah_dist = 4.0;

  LSSGaussState state = m.new_traj();
  vector<vec> traj = some_ball_data();
  double approx_deltaT = 0.016;
//  cout << "state " << state.mean() << endl << state.cov() << endl;
  for (const vec& obs : traj) {
//    cout << "Hello: " << obs << endl;
    BOOST_CHECK( m.obs_mah_dist(state, obs) < max_mah_dist || distance(m.mean_position(state),obs)<0.15 );
//    cout << "World: " << m.obs_mah_dist(state, obs) << " " << distance(m.mean_position(state),obs) << endl;
    LSSGaussState obs_state = m.observe(state, obs);
//    cout << "obs_state " << obs_state.mean() << endl << obs_state.cov() << endl; 
    //BOOST_CHECK( m.obs_mah_dist(state, obs) > m.obs_mah_dist(obs_state, obs) );
    state = m.advance(obs_state, approx_deltaT);
//    cout << "n_state " << state.mean() << endl << state.cov() << endl;
  }
}

BOOST_AUTO_TEST_CASE( TestBallFilter3D ) {
  Table table = loadTable("examples/models/table.txt");
  ifstream f_in("test/data/ball_traj.json");
  json data;
  f_in >> data;
  mat bounce_gamma = json2mat(data["Gamma"]);

  LGBM1Params ball_model_params;
  ball_model_params.air_drag = json2vec(data["air_drag"]);
  ball_model_params.gravety = -9.8;
  ball_model_params.bounce_fac = json2vec(data["bounce_coef"]);
  ball_model_params.bounce_trans_noise = bounce_gamma;
  ball_model_params.fly_trans_noise = json2mat(data["Gamma"]);
  ball_model_params.fly_obs_noise = json2mat(data["Sigma"]);
  ball_model_params.bounce_obs_noise = json2mat(data["Sigma"]);

  LSSGaussState init_st{ json2vec(data["mu0"]), json2mat(data["P0"]) };
  auto dyn = make_shared<LGBM1>(ball_model_params, table);

  auto ball_model = make_shared<BallModel>( init_st, dyn );
  KalmanOutlier3D ball_filter {ball_model};

  RansacConf ransac_conf;
  ransac_conf.tol = 0.05; //5cm tolerance (otherwise outlier)
  ransac_conf.num_iter = 50; //Do 50 iterations of RANSAC
  ransac_conf.group_size = 6; //Pick groups of this many elements
  FilterRansac3D::Params ransac_pars;
  ransac_pars.max_buff_size = 12; //Keep these last ball observations only (at most)
  ransac_pars.min_support = 8; //If we have less than this number of inliers do not proceed
  ransac_pars.max_time_gap = 1.0;
  ransac_pars.rans_conf = ransac_conf;
  KalmanOutlier3D::Params kalman_params; //5 mah distances is outlier
  kalman_params.max_mah_dist = 5.0; //More than this Mahalanobis dist means outlier
  kalman_params.ransac_params = ransac_pars;

  ball_filter.set_params(kalman_params);
  
  auto trajs = data["trajectories"];
  double deltaT = data["deltaT"];
  for (const auto& traj : trajs) {
    BOOST_CHECK_MESSAGE(!ball_filter.ready(), "Ransac ball filter should not be ready at the start");
    mat Xn = json2mat( traj.at("X") );
    mat Un = json2mat( traj.at("U") );
    mat Zn = json2mat( traj.at("Z") );
    for (unsigned int t=0; t<Xn.n_rows; t++) {
      double obs[3] = {Xn(t,0), Xn(t,1), Xn(t,2)};
      ball_filter.add_obs(0, t*deltaT, obs);
      if (t>ransac_pars.max_buff_size) {
        BOOST_CHECK_MESSAGE(ball_filter.ready(), "The ball filter should be ready now, but it is not");
      }
      if (ball_filter.ready()) {
        LSSGaussState state = ball_filter.state();
        for (unsigned int tn=t; tn<Xn.n_rows; tn++) {
          vec diff = Zn.row(tn).t() - state.mean();
          mat C = dyn->obs_mat(state, &deltaT);
          vec mean_obs = C*state.mean();
          vec obs_truth = C*Zn.row(tn).t();
          vec obs_diff = mean_obs - obs_truth;
          mat obs_cov = dyn->obs_noise(state, &deltaT) + C*state.cov()*C.t();
          double mah_dist = sqrt(dot(diff, inv(state.cov())*diff));
          double obs_mah_dist = sqrt(dot(obs_diff, inv(obs_cov)*obs_diff));

          BOOST_CHECK_MESSAGE(obs_mah_dist < 3.6,
              "The ground truth ball position is outside the 99.5\% of prob. mass");
          if (mah_dist > 5.0) break; //bounce happens at wrong time
          if (t > 30) {
            BOOST_CHECK_MESSAGE(arma::norm(obs_truth - mean_obs) < 0.05, //set 5cm threshold
                "Error between ball trajectory and ball prediction larger that threshold");
          }
          state = state.next(*dyn, Un.row(tn).t(), &deltaT);
        }
      }
    }
    ball_filter.reset();
  }
}
