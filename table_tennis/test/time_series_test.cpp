
#include <robotics/table_tennis/time_series.hpp>
#include <robotics/table_tennis/segment.hpp>
#include <armadillo>
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TimeSeries
#include <boost/test/unit_test.hpp>
#include <json.hpp>
#include <fstream>

using namespace std;
using namespace arma;
using namespace robotics::table_tennis;
using json = nlohmann::json;

BOOST_AUTO_TEST_CASE( test_split_by_time ) {
  vector<double> time {3, 3.1, 3.9, 5, 5.2, 5.7, 6, 8, 9.1, 9.2};
  vector<unsigned int> ans {0, 3, 7, 8};
  vector<unsigned int> ix = split_by_time(time, 1.0);

  BOOST_REQUIRE(ans.size() == ix.size());
  for (unsigned int i=0; i<ans.size(); i++) {
    BOOST_CHECK( ans[i] == ix[i] );
  }
}

BOOST_AUTO_TEST_CASE( test_max_sensible_seq ) {
  TSeries ts;
  ts.y = {vec{3},vec{3.4},vec{10},vec{9.5},vec{9},vec{8.7},vec{8.1},vec{20},vec{8},vec{3}};
  ts.time = {1,2,3,4,5,6,7,8,9,10};
  auto is_sensible = [](double t1, const vec& y1, double t2, const vec& y2) -> bool {
    (void)t1; (void)t2;
    return fabs(y1[0] - y2[0]) < 1.0;
  };
  SensibleSubseq::Criteria criteria {2.0, is_sensible};
  SensibleSubseq sub {ts.time, ts.y, criteria};
  auto ans = sub.get_subseq(2);
  vector<unsigned int> myans {2,3,4,5,6,8};
  BOOST_REQUIRE( ans.size() == myans.size() );
  for (unsigned int i=0; i<ans.size(); i++) {
    BOOST_CHECK(ans[i] == myans[i]);
  }
  BOOST_CHECK( ans.size() == sub.size_subseq(2) );
}

BOOST_AUTO_TEST_CASE( test_get_outliers ) {
  vector<unsigned int> inliers = {1,2,4,5,7}, outliers = {0,3,6,8,9};
  unsigned int N = 10;
  BOOST_CHECK(N == (inliers.size() + outliers.size()));
  auto _outliers = get_outliers(inliers, N);
  BOOST_REQUIRE(outliers.size() == _outliers.size());
  for (unsigned int i=0; i<outliers.size(); i++) {
    BOOST_CHECK(outliers[i] == _outliers[i]);
  }
}

BOOST_AUTO_TEST_CASE( test_local_min ) {
  vector<double> z {3,2,1,2,2,3,1,2,3};
  auto ans = detect_local_min(z,5,0.0);
  vector<unsigned int> right_ans {2,6};
  BOOST_REQUIRE( right_ans.size() == ans.size() );
  for (unsigned int i=0; i<ans.size(); i++) {
    BOOST_CHECK(ans[i] == right_ans[i]);
  }
}

BOOST_AUTO_TEST_CASE( old_vision_segmenter ) {
  json in;
  ifstream input("test/data/rec_20161111_142829.json");
  input >> in;
  json conf;
  ifstream configuration("test/data/segm_conf.json");
  configuration >> conf;
  Segment s;
  s.update_config(conf);
  json out = s.segm_meta_data(in);

  ifstream output("test/data/segm_142829.json");
  json correct_out;
  output >> correct_out;

  //1) Verify segments
  json segments = out.at("segments");
  json correct_segments = correct_out.at("segments");
  BOOST_REQUIRE( segments.size() == correct_segments.size() );
  //1.1) Verify the cutting points (Assuming the ordering is actually the same)
  for (unsigned int i=0; i<correct_segments.size(); i++) {
    BOOST_CHECK(segments[i].at("start_ball_ix") == correct_segments[i].at("start_ball_ix"));
    BOOST_CHECK(segments[i].at("end_ball_ix") == correct_segments[i].at("end_ball_ix"));
    json strike = segments[i].at("strike"), c_strike = correct_segments[i].at("strike");
    BOOST_CHECK(strike.at("start_ix") == c_strike.at("start_ix"));
    BOOST_CHECK(strike.at("end_ix") == c_strike.at("end_ix"));

    BOOST_CHECK(segments[i].at("success") == correct_segments[i].at("success"));
  }
}
