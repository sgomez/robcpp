
#include "robotics/table_tennis/time_series.hpp"
#include <queue>

using namespace std;
using namespace arma;

namespace robotics {
  namespace table_tennis {

    /**
     * Returns possible splitting indices for trials, by considering that the maximum time not
     * seeing anything in a trial should not be too large. The parameter max_time represents
     * how large this intervals are acceptable within a trial.
     * @brief Returns possible splitting times for trials
     * @param[in] times Time stamp of the observations
     * @param[in] max_time Maximum time between two consecutive observations that is considered 
     * acceptable inside one single trial
     * @returns Vector with starting indices for each data segment
     */
    std::vector<unsigned int> split_by_time(const std::vector<double>& times, double max_time) {
      vector<unsigned int> ans{0};
      for (unsigned int i=1; i<times.size(); i++) {
        if ( (times[i] - times[i-1]) > max_time ) {
          ans.push_back(i);
        }
      }
      return ans;
    }

    /**
     * @brief Given the indices of the inliers returns the indices of the outliers
     */
    std::vector<unsigned int> get_outliers(const std::vector<unsigned int>& inliers, unsigned int N) {
      std::vector<unsigned int> outliers;
      for (unsigned int i=0, j=0; i<N; i++) {
        if ( j == inliers.size() || i < inliers[j] ) outliers.push_back(i);
        else j++;
      }
      return outliers;
    }

    class SensibleSubseq::Impl {
      public:
        vector<unsigned int> max_length;
        vector<unsigned int> next_elem;

        Impl() = default;
        ~Impl() = default;

        vector<unsigned int> get_subseq(unsigned int first_elem, const function<bool(unsigned int)>& can_use) const {
          //Reconstruct the answer
          std::vector<unsigned int> ans;
          for (int current=first_elem; current != -1 && can_use(current); current = next_elem[current]) {
            ans.push_back(current);
          }
          return ans;
        }

        void run_dp(const std::vector<double>& time, const std::vector<arma::vec>& y, 
            const Criteria& conf) {
          unsigned int N = time.size();
          if (N != y.size()) 
            throw std::logic_error("The time and value arrays should have the same length for time series");

          //Start the dynamic programming algorithm with cost O(N^2)
          max_length = vector<unsigned int>(N, 1);
          next_elem = vector<unsigned int>(N, -1);
          for (int i=((int)N)-2; i>=0; i--) {
            for (unsigned int j=i+1; j<N; j++) {
              if ((time[j] - time[i]) > conf.max_time_gap) break;
              if (conf.is_sensible(time[i], y[i], time[j], y[j])) {
                unsigned int c_val = 1 + max_length[j];
                if (c_val > max_length[i]) {
                  max_length[i] = c_val;
                  next_elem[i] = j;
                }
              }
            }
          }
        }
    };

    /**
     * Computes the maximal length sub-sequence of the given sequence such that neighboring
     * elements are sensible (or make sense) according to the given function. If several options
     * are equally long, it returns the lowest in lexicographical order.
     * @brief Computes the maximal length sensible sub-sequence
     * @param[in] seq Time series sequence
     * @param[in] is_sensible Function that given two consecutive elements determine weather or
     * not they are sensible
     * @returns A vector with the indices of the sensible elements
     */
    SensibleSubseq::SensibleSubseq(const std::vector<double>& time, 
        const std::vector<arma::vec>& y, const Criteria& conf) {
      _impl = unique_ptr<Impl>(new Impl);
      _impl->run_dp(time, y, conf);
    }

    SensibleSubseq::~SensibleSubseq() = default;

    unsigned int SensibleSubseq::size_subseq(unsigned int first_elem) const {
      return _impl->max_length.at(first_elem);
    }

    std::vector<unsigned int> SensibleSubseq::get_subseq(unsigned int first_elem) const {
      auto tautology = [](unsigned int i) -> bool { (void)i; return true; };
      return _impl->get_subseq(first_elem, tautology);
    }

    std::vector<unsigned int> SensibleSubseq::get_subseq(unsigned int first_elem, 
            const std::function<bool(unsigned int)>& can_use) const {
      return _impl->get_subseq(first_elem, can_use);
    }

    /**
     * @brief Zero crossing velocity algorithm for robot trajectory splitting

     * This method returns a pair of integers (a,b) specifying an index range
     * that can be used to split the given robot trajctory. The algorithm works
     * as follows:
     *
     * 1) Find the maximum joint velocity
     * 2) Find an index range (a,b) that contains the maximum joint velocity and
     *    where the velocity at a and b are close to zero.
     *
     * Two optional parameters are used to configure this method:
     * @param[in] split_joint Joint index used to split the data
     * @param[in] zero_tol Percentaje of the maximum velocity considered zero
     */
    std::pair<unsigned int, unsigned int> zero_cross_vel(const std::vector<arma::vec>& q_dot, 
        unsigned int split_joint, double zero_tol) {
      //1) Find maximum joint velocity
      double max_vel = 0.0;
      unsigned int max_ix = -1;
      for (unsigned int i=0; i<q_dot.size(); i++) {
        if (fabs(q_dot[i][split_joint]) > max_vel) {
          max_vel = fabs(q_dot[i][split_joint]);
          max_ix = i;
        }
      }
      double zero_vel = zero_tol*max_vel;
      double sgn = (q_dot[max_ix][split_joint] >= 0.0) ? 1 : -1;
      //2) Move from maximum until zero velocity is found
      unsigned int a=max_ix, b=max_ix;
      while ( a>0 && (q_dot[a][split_joint]*sgn)>zero_vel ) a--;
      while ( b<q_dot.size() && (q_dot[b][split_joint]*sgn)>zero_vel ) b++;
      return make_pair(a,b);
    }

    /**
     * @brief Extracts a particular coordinate from a time series data
     */
    std::vector<double> extract_coordinate(const std::vector<arma::vec>& y, unsigned int ix) {
      vector<double> ans;
      for (const auto& x : y) {
        ans.push_back(x.at(ix));
      }
      return ans;
    }

    /**
     * @brief Detects local minimum in a vector
     * Given a window size and a vector this method finds all local minimums of
     * the given vector. A local minimum here is defined as the minimum value of
     * a window with the given size.
     */
    std::vector<unsigned int> detect_local_min(const std::vector<double>& z, 
        unsigned int window_size, double tol) {
      priority_queue<pair<double,unsigned int>> pq; //Insert -Z in the pq. It is max heap but we want minimum
      vector<unsigned int> ans;
      for (unsigned int i=0; i<z.size(); i++) {
        pq.push( make_pair(-z[i], i) );
        if (pq.size() < window_size) continue;
        while ((pq.top().second+window_size) <= i) pq.pop();
        unsigned int min_ix = pq.top().second;
        unsigned int win_center = i - (window_size / 2);
        if (min_ix == win_center && fabs(z[min_ix] - z[i]) > tol) ans.push_back(min_ix);
      }
      return ans;
    }

    /**
     * @brief Returns a value in {-1,0,+1} depending on the main direction of the given time series
     */
    int main_direction(const std::vector<double>& y) {
      int votes = 0;
      for (unsigned int i=1; i<y.size(); i++) {
        double diff = y[i] - y[i-1];
        if (fabs(diff) < 1e-6) continue;
        int curr = (diff > 0) ? 1 : -1;
        votes += curr;
      }
      if (votes > 0) return 1;
      else if (votes < 0) return -1;
      else return 0;
    }


  };
};
