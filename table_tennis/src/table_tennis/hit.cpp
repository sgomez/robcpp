
#include <robotics/table_tennis/hit.hpp>
#include <robotics/table_tennis/ball_filter.hpp>
#include <robotics/utils/math.hpp>

using namespace std;
using namespace arma;

namespace robotics {
  namespace table_tennis {

    /**
     * @brief Returns the log likelihood of the ball and racket being in the same position
     * @param[in] ball_pos Probability distribution of the ball position
     * @param[in] racket_pos Probability distribution of the racket position
     */
    double hit_log_likelihood(const random::NormalDist& ball_pos, const random::NormalDist& racket_pos) {
      return log_normal_overlap(ball_pos, racket_pos);
    }

    /**
     * @brief Returns the likelihood of the ball and racket being in the same position
     * @param[in] ball_pos Probability distribution of the ball position
     * @param[in] racket_pos Probability distribution of the racket position
     */
    double hit_likelihood(const random::NormalDist& ball_pos, const random::NormalDist& racket_pos) {
      return exp(hit_log_likelihood(ball_pos, racket_pos));
    }

    vector<double> log_overlaps(const std::vector<pair<double,random::NormalDist>>& ball_pos, 
        const std::function<random::NormalDist(double z)>& racket_pos, double t0, double T,
        const std::function<double(double z)>& log_prior_z) {
      LogOverlaps tmp {racket_pos, log_prior_z};
      return tmp(ball_pos, t0, T);
    }

    class LogOverlaps::Impl {
      public:
        std::function<random::NormalDist(double z)> racket_pos;
        std::function<double(double z)> log_prior_z;
        std::vector<pair<double, random::NormalDist>> v_racket_dist;

        Impl(const function<random::NormalDist(double z)>& racket_pos, 
            const function<double(double z)>& log_prior_z) : racket_pos(racket_pos), 
            log_prior_z(log_prior_z) {
        }

        vector<double> comp_overlaps(const std::vector<pair<double,random::NormalDist>>& ball_pos, 
            double t0, double T) {
          vector<double> log_values;
          v_racket_dist.clear();
          for (auto p : ball_pos) {
            double z = (p.first-t0) / T;
            if (z>0 && z<1) {
              double lw = (log_prior_z) ? log_prior_z(z) : 0.0;
              auto racket_dist = racket_pos(z);
              log_values.push_back(lw + hit_log_likelihood(p.second, racket_dist));
              v_racket_dist.push_back( make_pair(p.first,std::move(racket_dist)) );
            } else {
              log_values.push_back(-1e100);
            }
          }
          return log_values;
        }
    };

    LogOverlaps::LogOverlaps(const std::function<random::NormalDist(double z)>& racket_pos,
        const std::function<double(double z)>& prior_z) {
      _impl = unique_ptr<Impl>( new Impl{racket_pos, prior_z} );
    }
    
    LogOverlaps::~LogOverlaps() = default;

    std::vector<double> LogOverlaps::operator()(const vector<pair<double, random::NormalDist>>& ball_pos,
            double t0, double T) {
      return _impl->comp_overlaps(ball_pos, t0, T);
    }

    const std::vector<std::pair<double, random::NormalDist>>& LogOverlaps::get_racket_dist() const {
      return _impl->v_racket_dist;
    }

    void LogOverlaps::set_racket(const std::function<random::NormalDist(double z)>& racket_pos) {
      _impl->racket_pos = racket_pos;
      _impl->v_racket_dist.clear();
    }

    /**
     * @brief Hit log-likelihood after marginalizing the time
     */
    double log_mar_hit_lh(const std::vector<pair<double,random::NormalDist>>& ball_pos, 
        const std::function<random::NormalDist(double z)>& racket_pos, double t0, double T,
        const std::function<double(double z)>& log_prior_z) {
      return log_sum_exp(log_overlaps(ball_pos, racket_pos, t0, T, log_prior_z));
    }

    /**
     * @brief Applies the golden optimization algorithm to minimize the received function
     * @param[in] t_a Inferior limit for the search
     * @param[in] t_b Upper limit for the search
     */
    double golden_optimization(const std::function<double(double t)>& f, 
        double t_a, double t_b, double tol) {
      const static double gr = (1.0 + sqrt(5.0)) / 2.0; //< Golden ratio
      double dist = (t_b - t_a) / gr;
      double p1 = t_b - dist, p2 = t_a + dist;
      while (fabs(p2-p1) > tol) {
        if (f(p1) < f(p2)) {
          t_b = p2;
        } else {
          t_a = p1;
        }
        dist = (t_b - t_a) / gr;
        p1 = t_b - dist, p2 = t_a + dist;
      }
      return (t_b + t_a) / 2.0;
    }


    /**
     * @brief Returns the initial time that maximizes the likelihood of the racket hitting the ball
     */
    double opt_start_time(const std::vector<pair<double,random::NormalDist>>& ball_pos, 
        const std::function<random::NormalDist(double z)>& racket_pos, double T,
        const std::function<double(double z)>& log_prior_z) {
      auto opt_func = [&](double t0) -> double { 
        return -log_mar_hit_lh(ball_pos, racket_pos, t0, T, log_prior_z);
      };
      double t_a = ball_pos.front().first, t_b = ball_pos.back().first;
      return golden_optimization(opt_func, t_a, t_b, 0.001);
    }

  };
};

