
#include "robotics/table_tennis/env_state.hpp"
#include <thread>
#include <deque>
#include <mutex>
#include <cmath>
#include <unordered_map>
#include <condition_variable>

using namespace std;

namespace robotics {
  namespace table_tennis {

    class EnvObs::Impl {
      private:
        unsigned int compute_key(const TTBallObs& obs) {
          unsigned int key = obs.dims + (obs.sensor_id<<2);
          return key;
        }

      public:
        deque<TTBallObs> ball_obs;
        unordered_map<unsigned int, TTBallObs> last_ball_obs;
        mutex env_mutex;
        std::condition_variable new_ball;

        Impl() {
        }

        Impl(const Impl& b) {
          ball_obs = b.ball_obs;
        }

        void clear() {
          ball_obs.clear();
          last_ball_obs.clear();
        }

        void add_ball_obs(const TTBallObs& obs) {
          ball_obs.push_back(obs);
          last_ball_obs[compute_key(obs)] = obs;
        }

        bool is_ball_obs_new(const TTBallObs& obs) {
          unsigned int key = compute_key(obs);
          if (last_ball_obs.count(key) == 0) return true;
          const TTBallObs& last = last_ball_obs[key];
          for (int i=0; i<3; i++) {
            if ( fabs(obs.pos[i] - last.pos[i]) > 1e-6 ) return true;
          }
          return false;
        }

    };

    EnvObs::EnvObs() {
      _impl = unique_ptr<Impl>(new Impl());
    }

    EnvObs::~EnvObs() = default;

    /**
     * @brief Clear the observation buffers
     */
    void EnvObs::clear() {
      std::lock_guard<mutex> l(_impl->env_mutex);
      _impl->clear();
    }

    /**
     * @brief Insert the given ball observation to the ball buffer
     */
    void EnvObs::add_ball_obs(const TTBallObs& obs) {
      std::unique_lock<mutex> l(_impl->env_mutex);
      _impl->add_ball_obs(obs);
      l.unlock();
      _impl->new_ball.notify_all();
    }
    
    /**
     * This method inserts a ball observation only if it is different from the previous
     * ball observation using some tolerance. Returns true if the given ball observation
     * is actually new (and insert it to the observation buffer) or false if the given
     * ball observation was not new (in this case the observation is not inserted in the
     * observation buffer).
     * @brief Insert the given ball observation to the ball buffer only if is different from the 
     * last observation
     */    
    bool EnvObs::add_sl_ball_obs(const TTBallObs& obs) {
      std::unique_lock<mutex> l(_impl->env_mutex);
      if (_impl->is_ball_obs_new(obs)) {
        _impl->add_ball_obs(obs);
        l.unlock();
        _impl->new_ball.notify_all();
        return true;
      }
      return false;
    }

    /**
     * Return all the ball observations and clean the buffer of ball observations. The
     * last observed ball is kept in every sensor (camera) to make sure that the next
     * added observation is different from the last stored observation.
     * @brief Return all ball observations in the buffer and erase the buffer
     */
    std::deque<TTBallObs> EnvObs::flush_ball_obs() {
      std::lock_guard<mutex> l(_impl->env_mutex);
      deque<TTBallObs> ans;
      swap(ans, _impl->ball_obs);
      return ans;
    }

    /**
     * If the ball buffer is not empty, it blocks until a new ball is obtained. Otherwise the
     * function returns immediately.
     * @brief Waits until there are available ball observations
     */
    void EnvObs::wait_new_balls() {
      std::unique_lock<mutex> l(_impl->env_mutex);
      if (_impl->ball_obs.size() == 0) {
        _impl->new_ball.wait(l);
      }
    }

    /**
     * @brief Returns the number of ball observations in the buffer
     */
    unsigned int EnvObs::num_ball_obs() const {
      std::lock_guard<mutex> l(_impl->env_mutex);
      return _impl->ball_obs.size();
    }

  };
};

/* C API */
extern "C" {  

  /**
   * @brief Creates a table tennis enviroment observations object
   */
  void* tt_envobs_init() {
    using namespace robotics::table_tennis;
    return (void*)(new EnvObs());
  }
  
  /**
   * @brief Clean the observation buffers of the environment observation object
   */
  void tt_envobs_clean(void* envobs) {
    using namespace robotics::table_tennis;
    EnvObs* obs = (EnvObs*)envobs;
    obs->clear();
  }

  /**
   * Destructor of the enviroment observation object
   */
  void tt_envobs_free(void* envobs) {
    using namespace robotics::table_tennis;
    EnvObs* obs = (EnvObs*)envobs;
    delete obs;
  }

  /**
   * Add a ball observation to the enviroment observation object
   */
  void tt_envobs_add_ball(void* envobs, const struct TTBallObs* obs) {
    using namespace robotics::table_tennis;
    EnvObs* env = (EnvObs*)envobs;
    env->add_ball_obs(*obs);
  }

  /**
   * @brief Insert the given ball observation to the ball buffer only if is different from the 
   * last observation
   */ 
  int tt_envobs_add_sl_ball(void* envobs, const struct TTBallObs* obs) {
    using namespace robotics::table_tennis;
    EnvObs* env = (EnvObs*)envobs;
    return env->add_sl_ball_obs(*obs);
  }
};

