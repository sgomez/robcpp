
#include "robotics/table_tennis/raw_ball_models.hpp"
#include "robotics/utils.hpp"
#include <vector>

using namespace std;
using namespace arma;

namespace robotics {

  namespace table_tennis {

    LGBM1Params json2LGBM1Params(const nlohmann::json& j) {
      LGBM1Params ans;
      ans.air_drag = json2vec(j["airDrag"]);
      ans.bounce_fac = json2vec(j["bounceFac"]);
      ans.gravety = j["gravety"];
      ans.default_deltaT = j["deltaT"];
      ans.fly_obs_noise = json2mat(j["fly_obs_noise"]);
      ans.fly_trans_noise = json2mat(j["fly_trans_noise"]);
      ans.bounce_obs_noise = json2mat(j["bounce_obs_noise"]);
      ans.bounce_trans_noise = json2mat(j["bounce_trans_noise"]);
      return ans;
    }

    class LGBM1BallFlyingModel : public AbstractLSSModel {
      public:          
        arma::vec airDrag;
        double gravety;
        double deltaT;
        arma::mat Sigma, Gamma;
        arma::mat C, D;

        LGBM1BallFlyingModel(const arma::vec& airDrag, double gravety, double deltaT, mat Sigma, mat Gamma) : 
          airDrag(airDrag), gravety(gravety), deltaT(deltaT), Sigma(Sigma), Gamma(Gamma) {
          C << 1 << 0 << 0 << 0 << 0 << 0 << endr
            << 0 << 0 << 1 << 0 << 0 << 0 << endr
            << 0 << 0 << 0 << 0 << 1 << 0 << endr;
          D = zeros<mat>(3,1);
        }
        
        arma::mat transition(const LSSGaussState& state, const void* params) const {
          (void)state;
          double deltaT = (params==nullptr) ? this->deltaT : *(static_cast<const double*>(params));
          vector<mat> tmp1;
          for (int i=0; i<3; i++) {
            mat tmp2; 
            tmp2 << 1.0 << deltaT - 0.5*airDrag[i]*deltaT*deltaT << endr 
              << 0.0 << 1.0-airDrag[i]*deltaT << endr;
            tmp1.push_back(tmp2);
          }
          return block_diag(tmp1);
        }

        arma::mat action_mat(const LSSGaussState& state, const void* params) const {
          (void)state;
          mat B(6,1,fill::zeros);
          double deltaT = (params==nullptr) ? this->deltaT : *(static_cast<const double*>(params));
          B(4,0) = 0.5*deltaT*deltaT*gravety;
          B(5,0) = deltaT*gravety;
          return B;
        }

        arma::mat obs_mat(const LSSGaussState& state, const void* params) const {
          (void)state; (void)params;
          return C;
        }

        arma::mat obs_bias(const LSSGaussState& state, const void* params) const {
          (void)state; (void)params;
          return D;
        }

        arma::mat obs_noise(const LSSGaussState& state, const void* params) const {
          (void)state; (void)params;
          return Sigma;
        }

        arma::mat trans_noise(const LSSGaussState& state, const void* params) const {
          (void)state; (void)params;
          return Gamma;
        }

        unsigned int dim_hidden() const {
          return 6;
        }

        unsigned int dim_obs() const {
          return 3;
        }
    };


    class LGBM1BallBounceModel {
      public:          
        arma::vec bounceFac;
        double gravety;
        double deltaT;
        double bounceZ;
        arma::mat Sigma, Gamma;
        arma::mat B;

        LGBM1BallBounceModel(const arma::vec& bounceFac, double gravety, double deltaT, double bounceZ,
            mat Sigma, mat Gamma) : bounceFac(bounceFac), gravety(gravety), deltaT(deltaT), 
            bounceZ(bounceZ), Sigma(Sigma), Gamma(Gamma) {
          B = mat(6,1,fill::zeros);
          B(4,0) = bounceZ*(1+bounceFac[2]);
        }
        
        arma::mat transition(const LSSGaussState& state, const void* params) const {
          (void)state;
          double deltaT = (params==nullptr) ? this->deltaT : *(static_cast<const double*>(params));
          mat ans(6,6,fill::zeros);
          ans(0,0) = 1.0; ans(0,1) = deltaT - 0.5*bounceFac[0]*deltaT*deltaT;
          ans(1,1) = bounceFac[0];
          ans(2,2) = 1.0; ans(2,3) = deltaT - 0.5*bounceFac[1]*deltaT*deltaT;
          ans(3,3) = bounceFac[1];
          //ans(4,4) = 1.0;
          //ans(5,5) = -bounceFac[2];
          ans(4,4) = -bounceFac[2]; ans(4,5) = -bounceFac[2]*deltaT;
          ans(5,5) = -bounceFac[2];
          //opt2: [0,0,0,0,-bounceFac[2],-bounceFac[2]*deltaT],[0,0,0,0,0,-bounceFac[2]]]
          //opt1: [0,0,0,0,1,0], [0,0,0,0,0,-bounceFac[2]]]
          return ans;
        }

        arma::mat action_mat(const LSSGaussState& state, const void* params) const {
          (void)state; (void)params;
          return B;
        }

        arma::mat obs_noise(const LSSGaussState& state, const void* params) const {
          (void)state; (void)params;
          return Sigma;
        }

        arma::mat trans_noise(const LSSGaussState& state, const void* params) const {
          (void)state; (void)params;
          return Gamma;
        }
    };
    
    class LGBM1::Impl {
      public:
        Table table;
        shared_ptr<const LGBM1BallFlyingModel> fly_model;
        shared_ptr<const LGBM1BallBounceModel> bounce_model;
        arma::vec u;

        Impl(const LGBM1Params& params, const Table& table) {
          this->table = table;
          const vec& tab_center = table.center();
          fly_model = shared_ptr<const LGBM1BallFlyingModel>( new LGBM1BallFlyingModel(params.air_drag, 
                params.gravety, params.default_deltaT, params.fly_obs_noise, params.fly_trans_noise) );
          bounce_model = shared_ptr<const LGBM1BallBounceModel>( new LGBM1BallBounceModel(params.bounce_fac, 
                params.gravety, params.default_deltaT, tab_center[2], params.bounce_obs_noise, 
                params.bounce_trans_noise) );
          u = vec{1.0};
        }

        Impl(const Impl& b) = default;

        /**
         * Return true if it is expected that there will be a bounce for the next state.
         */
        bool next_bounces(const LSSGaussState& state, const void* params) const {
          double deltaT = (params==nullptr) ? fly_model->deltaT : *(static_cast<const double*>(params));
          /* It may happen that the predicted ball end up a few millimeters above the table and no
           * collision is detected. Therefore we add to deltaT a small value to avoid not detecting
           * the collision with the table due to precision problems in the estimation of the hidden state.
           */
          deltaT += 0.002;
          LSSGaussState nstate = state.next(*fly_model, u, &deltaT);
          mat Cfly = fly_model->obs_mat(state, params);
          vec cpos = Cfly*state.mean();
          vec npos = Cfly*nstate.mean();
          return (table.segm_intersect_table(cpos, npos));
        }
        
        arma::mat transition(const LSSGaussState& state, const void* params) const {
          if (next_bounces(state, params)) {
            return bounce_model->transition(state, params);
          } else {
            return fly_model->transition(state, params);
          }
        }

        arma::mat action_mat(const LSSGaussState& state, const void* params) const {
          if (next_bounces(state, params)) {
            return bounce_model->action_mat(state, params);
          } else {
            return fly_model->action_mat(state, params);
          }
        }

        arma::mat obs_mat(const LSSGaussState& state, const void* params) const {
          return fly_model->obs_mat(state, params);
        }

        arma::mat obs_bias(const LSSGaussState& state, const void* params) const {
          return fly_model->obs_bias(state, params);
        }

        arma::mat obs_noise(const LSSGaussState& state, const void* params) const {
          if (next_bounces(state, params)) {
            return bounce_model->obs_noise(state, params);
          } else {
            return fly_model->obs_noise(state, params);
          }
        }

        arma::mat trans_noise(const LSSGaussState& state, const void* params) const {
          if (next_bounces(state, params)) {
            return bounce_model->trans_noise(state, params);
          } else {
            return fly_model->trans_noise(state, params);
          }         
        }
    };

    LGBM1::LGBM1() {
      _impl = nullptr;
    }

    LGBM1::LGBM1(const LGBM1Params& params, const Table& table) {
      _impl = unique_ptr<Impl>( new Impl(params, table) );
    }

    LGBM1::LGBM1(const LGBM1& b) {
      _impl = unique_ptr<Impl>( new Impl(*b._impl) );
    }

        
    LGBM1::LGBM1(LGBM1&& b) = default;
    LGBM1& LGBM1::operator=(const LGBM1& b) {
      if (&b != this) {
        _impl = unique_ptr<Impl>( new Impl(*b._impl) );
      }
      return *this;
    }

    LGBM1::~LGBM1() = default;
        
    arma::mat LGBM1::transition(const LSSGaussState& state, const void* params) const {
      return _impl->transition(state, params);
    }

    arma::mat LGBM1::action_mat(const LSSGaussState& state, const void* params) const {
      return _impl->action_mat(state, params);
    }

    arma::mat LGBM1::obs_mat(const LSSGaussState& state, const void* params) const {
      return _impl->obs_mat(state, params);
    }

    arma::mat LGBM1::obs_bias(const LSSGaussState& state, const void* params) const {
      return _impl->obs_bias(state, params);
    }

    arma::mat LGBM1::obs_noise(const LSSGaussState& state, const void* params) const {
      return _impl->obs_noise(state, params);
    }
        
    arma::mat LGBM1::trans_noise(const LSSGaussState& state, const void* params) const {
      return _impl->trans_noise(state, params);
    }

    unsigned int LGBM1::dim_hidden() const {
      return _impl->fly_model->dim_hidden();
    }

    unsigned int LGBM1::dim_obs() const {
      return _impl->fly_model->dim_obs();
    }

    
  };

};
