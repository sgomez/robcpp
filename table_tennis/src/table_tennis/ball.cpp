#include "robotics/table_tennis/ball.hpp"
#include "robotics/table_tennis/raw_ball_models.hpp"
#include "robotics/table_tennis/utils.hpp"
#include "robotics/utils.hpp"
#include "json.hpp"

using namespace std;
using namespace arma;


namespace robotics {
  namespace table_tennis {

    class BallModel::Impl {
      public:
        LSSGaussState init_st;
        shared_ptr<const AbstractLSSModel> dyn;
        arma::vec bias;

        Impl() = default;

        Impl(const LSSGaussState& init_st, shared_ptr<const AbstractLSSModel> dyn) : 
          init_st(init_st), dyn(dyn) {
          bias = vec{1};
        }

        /*Impl(const Table& table, const nlohmann::json& obj) {
          bias = vec{1};
          string model_type = obj.at("model");
          init_st = LSSGaussState(json2vec(obj.at("mu_0")), json2mat(obj.at("P0")));
          if (model_type == "LGBM1") {
            LGBM1Params par = json2LGBM1Params(obj);
            dyn = make_shared<LGBM1>(par, table);
          }
        }*/

        arma::vec mean_position(const LSSGaussState& state) {
          vec tmp = state.mean();
          return vec{tmp[0], tmp[2], tmp[4]};
        }

        random::NormalDist position_dist(const LSSGaussState& state) const {
          return state.obs_dist(*dyn, bias, nullptr);
        }
    };

    BallModel::BallModel() {
      _impl = nullptr;
    }

    /*BallModel::BallModel(const Table& table, const std::string& conf_json) {
      auto obj = nlohmann::json::parse(conf_json);
      _impl = unique_ptr<Impl>( new Impl(table, obj) );
    }*/

    BallModel::BallModel(const LSSGaussState& init_st, std::shared_ptr<const AbstractLSSModel> dyn) {
      _impl = unique_ptr<Impl>( new Impl{init_st, dyn} );
    }

    BallModel::BallModel(const BallModel& b) {
      _impl = unique_ptr<Impl>( new Impl(*b._impl) );
    }
        
    BallModel::BallModel(BallModel&& b) = default;
    BallModel::~BallModel() = default;
        
    BallModel& BallModel::operator=(const BallModel& b) {
      if (&b != this) {
        _impl = unique_ptr<Impl>( new Impl(*b._impl) );
      }
      return *this;
    }

    BallModel& BallModel::operator=(BallModel&& b) = default;

    LSSGaussState BallModel::new_traj() const {
      return _impl->init_st;
    }

    LSSGaussState BallModel::advance(const LSSGaussState& state, double deltaT) const {
      return state.next(*(_impl->dyn), _impl->bias, &deltaT);
    }

    LSSGaussState BallModel::observe(const LSSGaussState& state, const arma::vec& obs) const {
      return state.observe(*(_impl->dyn), obs, _impl->bias, nullptr);
    }

    double BallModel::obs_mah_dist(const LSSGaussState& state, const arma::vec& obs) const {
      return state.obs_mah_dist(*(_impl->dyn), obs, _impl->bias, nullptr);
    }

    arma::vec BallModel::mean_position(const LSSGaussState& state) const {
      return _impl->mean_position(state);
    }

    random::NormalDist BallModel::position_dist(const LSSGaussState& state) const {
      return _impl->position_dist(state);
    }

    std::vector<arma::vec> BallModel::sample_ball_traj(const LSSGaussState& init, 
        unsigned int len, double deltaT) const {
      vector<vec> actions {len-1, _impl->bias};
      return sample_lss_hidden_states(init, *(_impl->dyn), actions, &deltaT);
    }

    std::vector<arma::vec> BallModel::sample_ball_obs(const std::vector<arma::vec>& ball_traj) const {
      vector<vec> actions {ball_traj.size(), _impl->bias};
      return sample_lss_observations(ball_traj, *(_impl->dyn), actions, nullptr);
    }

    std::shared_ptr<BallModel> load_ball_model(const Table& table, const nlohmann::json& ball_model_params) {
      auto dyn = load_ball_dynamics(table, ball_model_params);
      auto init_st = LSSGaussState(json2vec(ball_model_params.at("mu_0")), json2mat(ball_model_params.at("P0")));
      return std::make_shared<BallModel>(init_st, dyn);
    }

    std::shared_ptr<BallModel> load_ball_model(const nlohmann::json& conf) {
      Table table = json2table(conf.at("table"));
      nlohmann::json ball_model_params = conf.at("ball_model");
      return load_ball_model(table, ball_model_params);
    }


    std::shared_ptr<BallModel> load_ball_model(const std::string& conf) {
      return load_ball_model(nlohmann::json::parse(conf));
    }

    std::shared_ptr<AbstractLSSModel> load_ball_dynamics(const Table& table, const nlohmann::json& conf) {
      string model_type = conf.at("model");      
      if (model_type == "LGBM1") {
        LGBM1Params par = json2LGBM1Params(conf);
        return make_shared<LGBM1>(par, table);
      } else {
        throw std::logic_error("The given ball model type is not recognized");
      }
    }

    BallModel load_ball_model_obj(const std::string& conf) {
      return BallModel{*load_ball_model(conf)};
    }

    arma::mat sample_ball_traj_obs(const BallModel& model, unsigned int len, double deltaT) {
      auto traj = model.sample_ball_traj(model.new_traj(), len, deltaT);
      auto obs = model.sample_ball_obs(traj);
      arma::mat ans(obs.size(), 3);
      for (unsigned int i=0; i<obs.size(); i++) {
        ans.row(i) = obs[i].t();
      }
      return ans;
    }

  };
};
