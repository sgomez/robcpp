
#include <robotics/table_tennis/segment.hpp>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <armadillo>
#include <robotics/table_tennis/time_series.hpp>
#include <robotics/utils.hpp>
#include <boost/log/trivial.hpp>

using namespace std;
using json = nlohmann::json;
using namespace arma;

namespace robotics {
  namespace table_tennis {

    namespace {
      TSeries get_ball_obs(const json& obs_values) {
        TSeries ans;
        for (auto x : obs_values) {
          ans.time.push_back(x["time"]);
          ans.y.push_back(robotics::json2vec(x["obs"]));
        }
        return ans;
      }

      TSeries get_joint_vel(const json& joints) {
        TSeries ans;
        for (auto x: joints) {
          ans.time.push_back(x["t"]);
          ans.y.push_back(robotics::json2vec(x["qd"]));
        }
        return ans;
      }

      struct MergeSeries {
        vector<unsigned int> sensor_id; //!< Sensor id of the given element
        vector<unsigned int> index; //!< Index in the sensor sequence of obs
        vector<double> time; //!< Time of the observation
        vector<arma::vec> y; //!< The observation itself

        void merge(const TSeries& curr, unsigned int new_id) {
          vector<double> _t;
          vector<unsigned int> _id, _ix;
          vector<arma::vec> _y;
          unsigned int i=0, j=0;
          while (i<time.size() && j<curr.time.size()) {
            if (time[i] < curr.time[j]) {
              _t.push_back(time[i]);
              _id.push_back(sensor_id[i]);
              _ix.push_back(index[i]);
              _y.push_back(y[i]);
              i++;
            } else {
              _t.push_back(curr.time[j]);
              _id.push_back(new_id);
              _ix.push_back(j);
              _y.push_back(curr.y[j]);
              j++;
            }
          }
          while (i<time.size()) {
            _t.push_back(time[i]);
            _id.push_back(sensor_id[i]);
            _ix.push_back(index[i]);
            _y.push_back(y[i]);
            i++;
          }
          while (j<curr.time.size()) {
            _t.push_back(curr.time[j]);
            _id.push_back(new_id);
            _ix.push_back(j);
            _y.push_back(curr.y[j]);
            j++;
          }
          swap(sensor_id, _id);
          swap(time, _t);
          swap(y, _y);
          swap(index, _ix);
        }

        MergeSeries(const json& obs, const unordered_set<unsigned int>& sids) {
          for (auto elem : obs) {
            unsigned int id = elem["id"];
            if (sids.count(id) != 0) {
              merge(get_ball_obs(elem["values"]), id);
            }
          }
        }
      };

      struct SegmConfig {
        double min_trial_len; //!< Minimal length of a table tennis trial in seconds
        double max_trial_len; //!< Maximal trial length in seconds
        double max_ball_dist; //!< Maximum ball distance between different sensors
        double max_vel; //!< Maximum velocity
        double time_bw_trials; //!< Time between different trials
        double time_bw_subtraj; //!< Time between sub-trajectories inside a trial
        unsigned int min_subseq_obs; //!< Minimum number of observations in a sub-sequence
        unsigned int max_subseq_obs; //!< Maximum number of observations in a sub-sequence
        unsigned int min_trial_subs; //!< Minimum number of sub-sequences in a trial
        unsigned int max_trial_subs; //!< Maximum number of sub-sequences in a trial
        bool verbose; //!< Should this script print debug information?
        unsigned int split_joint; //!< Index of the joint used to split by velocity
        double still_max_joint_vel; //!< Consider no movement if max joint velocity is below this
        double zero_tol; //!< Zero tolerance for the zero cross velocity algorithm
        unsigned int window_size; //!< Bounce detection window size
        double win_tol; //window tolerance to avoid detecting plateau as minima
        vector<int> subs_y_direction; //!< Main direction required in each subsequence
        vector<vector<double>> opponent_court;

        SegmConfig() : min_trial_len(1.0), max_trial_len(3.0), max_ball_dist(0.06), 
        max_vel(4.0), time_bw_trials(0.5), 
        time_bw_subtraj(0.08), min_subseq_obs(50), max_subseq_obs(1000), min_trial_subs(2),
        max_trial_subs(3), verbose(true), split_joint(0), still_max_joint_vel(0.1), 
        zero_tol(0.01), window_size(11), win_tol(0.05),
        opponent_court({{-0.76,0.76},{-3.5,-2.13},{-0.99,-0.8}}) {
        }

        void update(const json& conf) {
          if (conf.count("min_trial_len")) min_trial_len = conf.at("min_trial_len");
          if (conf.count("max_trial_len")) max_trial_len = conf.at("max_trial_len");
          if (conf.count("max_ball_dist")) max_ball_dist = conf.at("max_ball_dist");
          if (conf.count("max_vel")) max_vel = conf.at("max_vel");
          if (conf.count("time_bw_trials")) time_bw_trials = conf.at("time_bw_trials");
          if (conf.count("time_bw_subtraj")) time_bw_subtraj = conf.at("time_bw_subtraj");
          if (conf.count("min_subseq_obs")) min_subseq_obs = conf.at("min_subseq_obs");
          if (conf.count("max_subseq_obs")) max_subseq_obs = conf.at("max_subseq_obs");
          if (conf.count("min_trial_subs")) min_trial_subs = conf.at("min_trial_subs");
          if (conf.count("max_trial_subs")) max_trial_subs = conf.at("max_trial_subs");
          if (conf.count("verbose")) verbose = conf.at("verbose");
          if (conf.count("split_joint")) split_joint = conf.at("split_joint");
          if (conf.count("still_max_joint_vel")) 
            still_max_joint_vel = conf.at("still_max_joint_vel");
          if (conf.count("zero_tol")) zero_tol = conf.at("zero_tol");
          if (conf.count("subs_y_direction")) subs_y_direction = 
            conf.at("subs_y_direction").get<vector<int>>();
          if (conf.count("window_size")) window_size = conf.at("window_size");
          if (conf.count("win_tol")) win_tol = conf.at("win_tol");
          if (conf.count("opponent_court"))
            opponent_court = conf.at("opponent_court").get<vector<vector<double>>>();
        }
      };
    };

    class Segment::Impl {
      public:
        SegmConfig conf;
        unordered_map<unsigned int, vector<unsigned int>> all_outliers;

        json segm_meta_data(const json& raw_data) {
          MergeSeries merged(raw_data["obs"], {0,1});
          auto time_splits = split_by_time(merged.time, conf.time_bw_trials);
          BOOST_LOG_TRIVIAL(trace) << "Number of time splits " << time_splits.size();
          if (merged.time.size() > 0) time_splits.push_back(merged.time.size());
          auto q_dot = get_joint_vel(raw_data["joints"]);

          json segm;
          unordered_map<unsigned int, vector<unsigned int>> all_outliers;
          for (unsigned int i=1; i<time_splits.size(); i++) {
            //1) Mark start and end of each trial
            json elem;
            elem["trial_id"] = i;
            elem["start_ball_ix"] = time_splits[i-1];
            elem["end_ball_ix"] = time_splits[i] - 1;

            double start_time = elem["start_time"] = merged.time[time_splits[i-1]];
            double end_time = elem["end_time"] = merged.time[time_splits[i] - 1];
            double duration = end_time - start_time;
            if (duration < conf.min_trial_len || duration > conf.max_trial_len) continue;
            unsigned int traj_start_ix = lower_bound(q_dot.time.begin(), q_dot.time.end(),
                start_time) - q_dot.time.begin();
            unsigned int traj_end_ix = lower_bound(q_dot.time.begin(), q_dot.time.end(),
                end_time) - q_dot.time.begin();
            //1.1) Copy segment to its own space in memory
            vector<double> ball_time(merged.time.begin() + time_splits[i-1],
                merged.time.begin() + time_splits[i]);
            vector<arma::vec> ball_pos(merged.y.begin() + time_splits[i-1],
                merged.y.begin() + time_splits[i]);
            vector<double> joint_time(q_dot.time.begin() + traj_start_ix,
                q_dot.time.begin() + traj_end_ix);
            vector<arma::vec> joint_vel(q_dot.y.begin() + traj_start_ix,
                q_dot.y.begin() + traj_end_ix);

            //2) Mark outliers in ball observations
            auto is_sensible = [this](double t1, const vec& y1, double t2, 
                const vec& y2) -> bool {
              assert(t2 >= t1);
              assert((t2 - t1) <= this->conf.time_bw_subtraj);
              //if ((t2 - t1) > conf.time_bw_subtraj) return false;
              double max_dist = this->conf.max_ball_dist + (t2-t1)*this->conf.max_vel;
              double min_dist = 1e-2; //Discard repeated observations
              double dist = norm(y2-y1);
              return (dist < max_dist && dist > min_dist); 
            };
            SensibleSubseq::Criteria subs_criteria;
            subs_criteria.max_time_gap = conf.time_bw_subtraj;
            subs_criteria.is_sensible = is_sensible;
            SensibleSubseq subs(ball_time, ball_pos, subs_criteria); 
            vector<bool> is_inlier(ball_time.size(), false);
            //2.1) Compute a vector with all the sub-sequences that make sense
            auto can_use_ix = [&is_inlier](unsigned int i) -> bool { return !is_inlier[i]; };
            vector<vector<unsigned int>> subs_ix;
            vector<TSeries> subsequences;
            for (unsigned int first=0; first<ball_time.size(); first++) {
              if (!is_inlier[first] && subs.size_subseq(first) >= conf.min_subseq_obs
                  && subs.size_subseq(first) <= conf.max_subseq_obs) {
                vector<unsigned int> inliers = subs.get_subseq(first, can_use_ix);
                //The following check is necessary because sub-sequences must be disjoint
                if (inliers.size() < conf.min_subseq_obs) continue;
                TSeries curr;
                for (auto elem : inliers) {
                  is_inlier[elem] = true;
                  curr.time.push_back(ball_time[elem]);
                  curr.y.push_back(ball_pos[elem]);
                }
                subs_ix.push_back( std::move(inliers) );
                subsequences.push_back( std::move(curr) );
              }
            }
            if (subsequences.size() < conf.min_trial_subs || 
                subsequences.size() > conf.max_trial_subs) continue;
            double ball_fw_time;
            if (subsequences.size() == 1)
              elem["ball_fw_time"] = ball_fw_time = subsequences[0].time.back();
            else
              elem["ball_fw_time"] = ball_fw_time = (subsequences[0].time.back() +
                  subsequences[1].time.front()) / 2.0;
            //2.1) Check that subsequences are not overlapping (Detect double ball problem)
            bool overlap = false;
            for (unsigned int subs_id=1; subs_id < subsequences.size(); subs_id++) {
              if (subsequences[subs_id-1].time.back() > subsequences[subs_id].time.front())
                overlap = true;
            }
            if (overlap) continue;
            //2.2) Check that y has the right direction
            if (conf.subs_y_direction.size() > 0) {
              bool sign_ok = true;
              for (unsigned int subs_id=0; subs_id<conf.subs_y_direction.size(); subs_id++) {
                auto y = extract_coordinate(subsequences[subs_id].y, 1);
                if (main_direction(y) != conf.subs_y_direction[subs_id]) sign_ok = false;
              }
              if (!sign_ok) continue;
            }

            //2.9) Actually mark the outliers
            for (unsigned int ix=0; ix<ball_time.size(); ix++) {
              if (is_inlier[ix]) continue;
              unsigned int global_ix = time_splits[i-1] + ix;
              unsigned int sensor_id = merged.sensor_id[global_ix];
              all_outliers[sensor_id].push_back(merged.index[global_ix]);
            }

            //3) Mark the striking movement by the robot
            auto p = zero_cross_vel(joint_vel, conf.split_joint, conf.zero_tol);
            //3.1) Delete trajectories where the robot didn't move
            if (fabs(joint_vel[(p.first + p.second)/2][conf.split_joint]) < 
                conf.still_max_joint_vel) continue;
            //3.2) Delete trajectories where racket interval does not contain the change on dir
            if (joint_time[p.first] > ball_fw_time || 
                joint_time[p.second] < ball_fw_time) continue;
            elem["strike"] = {
              {"start", joint_time[p.first]},
              {"end", joint_time[p.second-1]},
              {"duration", joint_time[p.second-1] - joint_time[p.first]},
              {"start_ix", p.first + traj_start_ix}, 
              {"end_ix", p.second - 1 + traj_start_ix}
            };

            //4) Mark the trial with additional meta-data
            //4.1) Mark if trial was successful by finding bounce point in ball return
            if (subsequences.size() > 1) {
              auto z = extract_coordinate(subsequences[1].y, 2);
              auto bounces = detect_local_min(z, conf.window_size, conf.win_tol);
              bool success = true;
              if (bounces.size()>0) {
                vec bounce_obs = subsequences[1].y[bounces[0]];
                for (unsigned int d=0; d<conf.opponent_court.size(); d++) {
                  if (bounce_obs[d]<conf.opponent_court[d][0] 
                      || bounce_obs[d]>conf.opponent_court[d][1]) {
                    success = false;
                  }
                }
              } else {
                success = false;
              }
              elem["success"] = success;
            }

            if (conf.verbose) {
              cout << "i=" << i << " Total: " << ball_time.size() << ". "; 
              for (const auto& v : subs_ix) {
                cout << v.size() << " ";
              }
              cout << endl;
            }
            segm.push_back(elem);
          }
          json obs;
          for (auto ix : all_outliers) {
            json elem;
            elem["id"] = ix.first;
            elem["outliers"] = ix.second;
            obs.push_back(elem);
          }
          json ans;
          ans["segments"] = segm;
          ans["obs"] = obs;
          swap(all_outliers, this->all_outliers);
          return ans;
        }

        json get_segments(const json& raw_data, const json& segm_meta, 
            const json& options) {
          unordered_map<unsigned int, unsigned int> ids_sensors;
          unsigned int id_joint=0;
          bool no_outliers = true;
          if (options.count("no_outliers")) no_outliers = options.at("no_outliers");
          json ans;
          for (auto elem : segm_meta["segments"]) {
            double start_time = elem["start_time"];
            double end_time = elem["end_time"];
            unsigned int trial_id = elem["trial_id"];
            json segm;
            for (auto sensor : raw_data.at("obs")) {
              unsigned int sensor_id = sensor.at("id");
              const json& vals = sensor.at("values");
              json segm_vals;
              unsigned int i = ids_sensors[sensor_id];
              while (i < vals.size()) {
                if (no_outliers && all_outliers.count(sensor_id)) {
                  const auto& v = all_outliers.at(sensor_id);
                  if (binary_search(v.begin(), v.end(), i)) {
                    i++; continue;
                  }
                }
                double time = vals[i].at("time");
                if (time > end_time) break;
                if (time >= start_time) {
                  segm_vals.push_back(vals[i]);
                }
                i++;
              }
              ids_sensors[sensor_id] = i;
              json curr_obs = {{"id", sensor_id}, {"values", segm_vals}};
              segm["obs"].push_back(curr_obs);
            }
            if (elem.count("strike") == 1) {
              start_time = elem.at("strike").at("start");
              end_time = elem.at("strike").at("end");
            }
            while (id_joint < raw_data["joints"].size()) {
              const json& curr_joint = raw_data["joints"][id_joint];
              double time = curr_joint.at("t");
              if (time > end_time) break;
              if (time > start_time) {
                segm["joints"].push_back(curr_joint);
              }
              id_joint++;
            }
            segm["trial_id"] = trial_id;
            if (elem.count("success")) segm["success"] = elem["success"];
            ans.push_back(segm);
          }
          return ans;
        }
    };

    Segment::Segment() {
      _impl = unique_ptr<Impl>(new Impl);
    }
    
    Segment::~Segment() = default;

    void Segment::update_config(const json& conf) {
      _impl->conf.update(conf);
    }

    json Segment::segm_meta_data(const json& raw_data) {
      return _impl->segm_meta_data(raw_data);
    }

    json Segment::get_segments(const json& raw_data, const json& segm_meta, 
        const json& options) {
      return _impl->get_segments(raw_data, segm_meta, options);
    }

    struct PlaySegmenterIsSensible {
      double max_ball_dist, max_vel;
      int y_dir;

      bool operator()(double t1, const vec& y1, double t2, 
          const vec& y2) {
        int y_dir = ((y2[1] - y1[1]) > 0) ? 1 : -1;
        if (y_dir != this->y_dir) return false;
        double max_dist = this->max_ball_dist + (t2-t1)*this->max_vel;
        double dist = norm(y2-y1);
        return (dist < max_dist); 
      }
    };

    bool ball_land_success(int player, const TSeries& seq, const TrialConf& conf) {
      unsigned int player_ix = max(0, player);
      auto z = extract_coordinate(seq.y, 2);
      auto bounces = detect_local_min(z, conf.window_size, conf.win_tol);
      if (bounces.size()>0) {
        vec bounce_obs = seq.y[bounces[0]];
        BOOST_LOG_TRIVIAL(trace) << "Bounce detected at (" << bounce_obs[0] << ", " <<
          bounce_obs[1] << ", " << bounce_obs[2] << ")";
        for (unsigned int d=0; d<3; d++) {
          if (bounce_obs[d]<conf.court[6*player_ix + 2*d] 
              || bounce_obs[d]>conf.court[6*player_ix + 2*d + 1]) {
            BOOST_LOG_TRIVIAL(trace) << "Bounce out of court in coordinate " << d;
            return false;
          }
        }
        BOOST_LOG_TRIVIAL(trace) << "Bounce detected in the court. Successful trial";
        return true;
      } else {
        BOOST_LOG_TRIVIAL(trace) << "No bounce detected, must be a failure";
        return false;
      }
    }


    nlohmann::json segm_game_in_trials(const std::vector<double>& game_ball_time,
        const std::vector<arma::vec>& game_ball_pos, const TrialConf& conf) {
      json trials;
      BOOST_LOG_TRIVIAL(trace) << "Computing longest sensible sub-sequences for the current game";
      //1) Mark outliers in ball observations and find trials
      SensibleSubseq::Criteria subs_criteria1, subs_criteria2;
      subs_criteria1.max_time_gap = subs_criteria2.max_time_gap = conf.time_bw_subtraj;
      subs_criteria1.is_sensible = PlaySegmenterIsSensible{conf.max_ball_dist,
        conf.max_vel, 1};
      subs_criteria2.is_sensible = PlaySegmenterIsSensible{conf.max_ball_dist,
        conf.max_vel, -1};
      SensibleSubseq subs_incoming(game_ball_time, game_ball_pos, subs_criteria1),
                     subs_outgoing(game_ball_time, game_ball_pos, subs_criteria2);
      vector<bool> is_inlier(game_ball_time.size(), false);
      //2) Compute a vector with all the sub-sequences that make sense
      auto can_use_ix = [&is_inlier](unsigned int i) -> bool { return !is_inlier[i]; };
      vector<vector<unsigned int>> subs_ix;
      vector<TSeries> subsequences;
      vector<int> subs_y_directions;
      for (unsigned int first=0; first<game_ball_time.size(); first++) {
        vector<unsigned int> inliers;
        int curr_y_dir;
        if (!is_inlier[first] && subs_incoming.size_subseq(first) >= conf.min_subseq_obs
            && subs_incoming.size_subseq(first) <= conf.max_subseq_obs) {
          inliers = subs_incoming.get_subseq(first, can_use_ix);
          curr_y_dir = 1;
        } else if (!is_inlier[first] && subs_outgoing.size_subseq(first) >= conf.min_subseq_obs
            && subs_outgoing.size_subseq(first) <= conf.max_subseq_obs) {
          inliers = subs_outgoing.get_subseq(first, can_use_ix);
          curr_y_dir = -1;
        }
        if (!inliers.empty()) {
          //The following check is necessary because sub-sequences must be disjoint
          if (inliers.size() < conf.min_subseq_obs) continue;
          TSeries curr;
          for (auto elem : inliers) {
            is_inlier[elem] = true;
            curr.time.push_back(game_ball_time[elem]);
            curr.y.push_back(game_ball_pos[elem]);
          }
          BOOST_LOG_TRIVIAL(debug) << "Finding a valid sub-sequence at sub-index " << first << 
            " with " << inliers.size() << " valid ball observations, direction " << curr_y_dir <<
            " and duration of " << (curr.time.back() - curr.time.front()) << " seconds";
          subs_ix.push_back( std::move(inliers) );
          subsequences.push_back( std::move(curr) );
          subs_y_directions.push_back(curr_y_dir);
        }
      }

      //3) Iterate over all the trials
      BOOST_LOG_TRIVIAL(trace) << "Iterating over the sub-sequences to generate trials";
      for (unsigned int subs_id = 0; subs_id<subsequences.size(); ++subs_id) {
        int player = subs_y_directions[subs_id];
        json trial;
        trial["trial_id"] = subs_id;
        trial["player"] = player;
        trial["start_ball_ix"] = subs_ix[subs_id].front();
        trial["start_time"] = subsequences[subs_id].time.front();
        bool valid, double_ball;
        if ((subs_id+1) < subsequences.size()) {
          trial["end_ball_ix"] = subs_ix[subs_id+1].back();
          trial["end_time"] = subsequences[subs_id+1].time.back();
          double_ball = (player == subs_y_directions[subs_id+1]);
          valid = !double_ball;
          trial["valid"] = valid;
          trial["incoming_ball_ix"] = subs_ix[subs_id];
          trial["outgoing_ball_ix"] = subs_ix[subs_id+1];
          if (valid) {
            double ball_fw_time = (subsequences[subs_id].time.back() + 
                subsequences[subs_id+1].time.front()) / 2.0;
            trial["ball_fw_time"] = ball_fw_time;
            trial["opponent_success"] = ball_land_success(subs_y_directions[subs_id], 
                subsequences[subs_id], conf);
            trial["success"] = ball_land_success(subs_y_directions[subs_id+1], 
                subsequences[subs_id+1], conf);
          }
        } else {
          trial["end_ball_ix"] = subs_ix[subs_id].back();
          trial["end_time"] = subsequences[subs_id].time.back();
          double_ball = false;
          valid = !double_ball;
          trial["valid"] = valid;
          trial["incoming_ball_ix"] = subs_ix[subs_id];
          trial["outgoing_ball_ix"] = nullptr;
          if (valid) {
            trial["opponent_success"] = ball_land_success(subs_y_directions[subs_id], 
                subsequences[subs_id], conf);
          }
        }

        //3.last) Add the trial to some global object
        trials.push_back(trial);
      }

      if (subsequences.size() > 0) {
      }
      return trials;
    }

    class PlaySegmenter::Impl {
      public:
        //SegmConfig conf;
        TrialConf trial_conf;
        unordered_map<unsigned int, vector<unsigned int>> all_outliers;

        /**
         * Constructs a Game meta-data independently of the robot data
         */
        json segm_meta_data(const json& raw_data) {
          MergeSeries merged(raw_data["obs"], {0,1});
          auto time_splits = split_by_time(merged.time, trial_conf.time_bw_trials);
          BOOST_LOG_TRIVIAL(info) << "Number of games " << time_splits.size();
          if (merged.time.size() > 0) time_splits.push_back(merged.time.size());
          BOOST_LOG_TRIVIAL(debug) << "Obtaining joint velocities";

          json games;
          unordered_map<unsigned int, vector<unsigned int>> all_outliers;

          // This for loop will iterate over all the games
          for (unsigned int i=1; i<time_splits.size(); i++) {
            //1) Copy each game to its own space in memory
            BOOST_LOG_TRIVIAL(debug) << "Time splits (" << time_splits[i-1] << "," <<
              time_splits[i] << ") for game " << i;
            double game_start_time = merged.time[time_splits[i-1]];
            double game_end_time = merged.time[time_splits[i] - 1];
            double game_duration = game_end_time - game_start_time;

            unsigned int game_start_ix = time_splits[i-1];
            unsigned int game_end_ix = time_splits[i];
            BOOST_LOG_TRIVIAL(info) << "Game " << i << " between indices (" << game_start_ix <<
             ", " << game_end_ix << ") with duration " << game_duration << " seconds"; 


            vector<double> game_ball_time(merged.time.begin() + time_splits[i-1],
                merged.time.begin() + time_splits[i]);
            vector<arma::vec> game_ball_pos(merged.y.begin() + time_splits[i-1],
                merged.y.begin() + time_splits[i]);

            BOOST_LOG_TRIVIAL(trace) << "Splitting game " << i << " in its trials";
            json trials = segm_game_in_trials(game_ball_time, game_ball_pos, trial_conf);
            json game{ 
              {"game_id", i},
              {"game_start_time", game_start_time},
              {"game_end_time", game_end_time},
              {"game_start_ix", game_start_ix},
              {"game_end_ix", game_end_ix},
              {"trials", trials}
            };
            games.push_back(game);
          }
          /*json obs;
          for (auto ix : all_outliers) {
            json elem;
            elem["id"] = ix.first;
            elem["outliers"] = ix.second;
            obs.push_back(elem);
          }
          json ans;
          ans["segments"] = segm;
          ans["obs"] = obs;
          swap(all_outliers, this->all_outliers);*/
          return games;
        }

        json get_segments(const json& raw_data, const json& segm_meta, 
            const json& options) {
          json ans;
          return ans;
        }
    };

    TrialConf::TrialConf() : max_ball_dist(0.06), max_vel(4.0), time_bw_trials(0.5),
      time_bw_subtraj(0.08), min_subseq_obs(50), max_subseq_obs(1000), 
      verbose(true), window_size(33), win_tol(0.05),
      court({-0.95,0.95,-3.7,-2.0,-0.99,-0.8,-0.8,0.8,-2.0,-0.54,-0.99,-0.8}){
    }

    void TrialConf::update(const json& conf) {
      if (conf.count("max_ball_dist")) max_ball_dist = conf.at("max_ball_dist");
      if (conf.count("max_vel")) max_vel = conf.at("max_vel");
      if (conf.count("time_bw_trials")) time_bw_trials = conf.at("time_bw_trials");
      if (conf.count("time_bw_subtraj")) time_bw_subtraj = conf.at("time_bw_subtraj");
      if (conf.count("min_subseq_obs")) min_subseq_obs = conf.at("min_subseq_obs");
      if (conf.count("max_subseq_obs")) max_subseq_obs = conf.at("max_subseq_obs");
      if (conf.count("verbose")) verbose = conf.at("verbose");
      if (conf.count("window_size")) window_size = conf.at("window_size");
      if (conf.count("win_tol")) win_tol = conf.at("win_tol");
      if (conf.count("court"))  court = conf.at("court").get<vector<double>>();
    }

    PlaySegmenter::PlaySegmenter() {
      _impl = unique_ptr<Impl>( new Impl );
    }

    PlaySegmenter::~PlaySegmenter() = default;

    void PlaySegmenter::update_config(const nlohmann::json& conf) {
      _impl->trial_conf.update(conf);
    }

    nlohmann::json PlaySegmenter::segm_meta_data(const nlohmann::json& raw_data) {
      return _impl->segm_meta_data(raw_data);
    }

    nlohmann::json PlaySegmenter::get_segments(const nlohmann::json& raw_data, 
        const nlohmann::json& segm_meta,
        const nlohmann::json& options) {
      return _impl->get_segments(raw_data, segm_meta, options);
    }


  };
};

