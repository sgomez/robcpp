#include <robotics/table_tennis/ball_filter.hpp>

using namespace std;
using namespace arma;
using json = nlohmann::json;

namespace robotics {
  namespace table_tennis {
    namespace ball_filters {

      class KalmanOutlier3D::Impl {
        public:
          std::shared_ptr<BallModel> ball_model;
          KalmanOutlier3D::Params params;
          FilterRansac3D ransac_filter;
          LSSGaussState _state;
          double _last_time;

          Impl(std::shared_ptr<BallModel> ball_model) : ball_model(ball_model), ransac_filter(ball_model) {
          }

          void reset() {
            ransac_filter.reset();
          }

          bool add_obs(unsigned int sensor, double time, const double* ball_obs) {
            if (!ransac_filter.ready()) {
              ransac_filter.add_obs(sensor, time, ball_obs);
              if (ransac_filter.ready()) {
                _state = ransac_filter.state();
                _last_time = time;
              }
            } else {
              arma::vec obs(3);
              std::copy(ball_obs, ball_obs+3, obs.begin());
              double deltaT = time - _last_time;
              if (deltaT > 1e-3) {
                _state = ball_model->advance(_state, deltaT);
                _last_time = time;
              }
              if (ball_model->obs_mah_dist(_state, obs) < params.max_mah_dist) {
                _state = ball_model->observe(_state, obs);
                return true;
              }
            }
            return false;
          }

          LSSGaussState state() const {
            return _state;
          }

          double last_time() const {
            if (ransac_filter.ready()) {
              return _last_time;
            } else {
              return ransac_filter.last_time();
            }
          }

          bool ready() const {
            return ransac_filter.ready();
          }
      };

      KalmanOutlier3D::KalmanOutlier3D(std::shared_ptr<BallModel> ball_model) {
        _impl = unique_ptr<Impl>( new Impl(ball_model) );
      }

      KalmanOutlier3D::~KalmanOutlier3D() = default;

      void KalmanOutlier3D::set_params(const KalmanOutlier3D::Params& params) {
        _impl->params = params;
        _impl->ransac_filter.set_params(params.ransac_params);
      }

      KalmanOutlier3D::Params& KalmanOutlier3D::params() {
        return _impl->params;
      }

      void KalmanOutlier3D::reset() {
        _impl->reset();
      }

      void KalmanOutlier3D::add_obs(unsigned int sensor, double time, const double* ball_obs) {
        _impl->add_obs(sensor, time, ball_obs);
      }

      LSSGaussState KalmanOutlier3D::state() const {
        return _impl->state();
      }

      double KalmanOutlier3D::last_time() const {
        return _impl->last_time();
      }

      bool KalmanOutlier3D::ready() const {
        return _impl->ready();
      }
    };
  };
};
