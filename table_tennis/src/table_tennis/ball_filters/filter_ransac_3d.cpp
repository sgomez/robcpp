#include <robotics/table_tennis/ball_filter.hpp>
#include <robotics/basis_functions.hpp>

#include <deque>
#include <armadillo>
#include "json.hpp"
#include <boost/log/trivial.hpp>

using namespace std;
using namespace arma;
using json = nlohmann::json;

namespace robotics {
  namespace table_tennis {

    namespace ball_filters {

      class FilterRansac3D::Impl {
        public:
          deque<arma::vec> raw_obs;
          deque<double> times;
          FilterRansac3D::Params params;
          mutable bool _ready;
          mutable bool _updated; //!< Allows for lazy evaluation of the ready() and state() methods
          mutable LSSGaussState _state;
          std::shared_ptr<BallModel> ball_model;

          Impl(shared_ptr<BallModel> ball_model) {
            _ready = false;
            _updated = true;
            this->ball_model = ball_model;
          }

          /**
           * Deletes wrong observations with RANSAC and then runs the kalman filter
           * through the remaining observations
           */
          void run_ransac() const {
            unsigned int nobs = raw_obs.size();
            unsigned int poly_deg = 2;
            _updated = true;
            if (nobs <= params.min_support) {
              _ready = false;
              return;
            }
            BOOST_LOG_TRIVIAL(trace) << "Running RANSAC on given observations";
            mat At(poly_deg+1, nobs);
            mat b(3, nobs);
            ScalarPolyBasis poly(poly_deg);
            for (unsigned int i=0; i<nobs; i++) {
              At.col(i) = poly.eval(times[i]);
              b.col(i) = raw_obs[i];
            }

            RansacLinAns rans = linear_ransac(At.t(), b.t(), params.rans_conf);
            if (rans.inliers.size() < params.min_support) {
              _ready = false;
              return;
            }

            BOOST_LOG_TRIVIAL(trace) << "Creating a new Kalman Filter";
            _state = ball_model->new_traj();
            double t = times[0];
            for (unsigned int i=0; i<nobs; i++) {
              double deltaT = times[i] - t;
              if (fabs(deltaT) > 1e-3) {
                _state = ball_model->advance(_state, deltaT);
                t = times[i];
              }
              if (rans.is_inlier[i]) {
                _state = ball_model->observe(_state, raw_obs[i]);
              }
            }
            BOOST_LOG_TRIVIAL(debug) << "Ransac considers that there is a good ball trajectory";
            _ready = true;
          }

          void reset() {
            _ready = false;
            _updated = true;
            raw_obs.clear();
            times.clear();
          }

          void add_obs(unsigned int sensor, double time, const double* ball_obs) {
            (void)sensor;
            double elap_time;
            if (!times.empty() && (elap_time=time - times.back()) > params.max_time_gap) {
              BOOST_LOG_TRIVIAL(debug) << "Resetting the ball filter state because " <<
                elap_time << " passed between consecutive ball obs";
              reset();
            }
            vec obs(3);
            std::copy(ball_obs, ball_obs+3, obs.begin());
            raw_obs.push_back(std::move(obs));
            times.push_back(time);
            if (times.size() > params.max_buff_size) {
              times.pop_front();
              raw_obs.pop_front();
            }
            _updated = false;
          }

          LSSGaussState state() {
            if (! _updated ) run_ransac();
            return _state;
          }
          
          double last_time() const {
            return times.back();
          }

          bool ready() const {
            if (! _updated ) run_ransac();
            return _ready;
          }

      };

      FilterRansac3D::FilterRansac3D(std::shared_ptr<BallModel> ball_model) {
        _impl = unique_ptr<Impl>( new Impl(ball_model) );
      }
          
      FilterRansac3D::~FilterRansac3D() = default;

      void FilterRansac3D::set_params(const Params& params) {
        _impl->params = params;
      }

      FilterRansac3D::Params& FilterRansac3D::params() {
        return _impl->params;
      }

      void FilterRansac3D::reset() {
        _impl->reset();
      }

      void FilterRansac3D::add_obs(unsigned int sensor, double time, const double* ball_obs) {
        _impl->add_obs(sensor, time, ball_obs);
      }

          
      LSSGaussState FilterRansac3D::state() const {
        return _impl->state();
      }

      double FilterRansac3D::last_time() const {
        return _impl->last_time();
      }

      bool FilterRansac3D::ready() const {
        return _impl->ready();
      }
    };

  };
};
