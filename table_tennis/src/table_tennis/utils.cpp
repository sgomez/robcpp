
#include "robotics/table_tennis/utils.hpp"
#include <robotics/utils.hpp>
#include <armadillo>
#include <fstream>
#include <streambuf>

using namespace std;

namespace robotics {
  namespace table_tennis {
    using json = nlohmann::json;

    std::string load_text_file(const std::string& f_name) {
      ifstream in{f_name};
      string ans {istreambuf_iterator<char>(in), istreambuf_iterator<char>()};
      return ans;
    }

    nlohmann::json loadJSON(const std::string& f_name) {
      ifstream in{f_name};
      json obj;
      in >> obj;
      return obj;
    }

    /**
     * Converts a JSON object containing the properties lenght, width and center and net_height
     * to a Table object.
     */
    Table json2table(const nlohmann::json& obj) {
      double width = obj["width"], lenght = obj["lenght"];
      arma::vec center = json2vec(obj["center"]);
      return Table(width, lenght, center, obj["net_height"]);
    }

    /**
     * Load a Table object from a file in JSON format.
     */
    Table loadTable(const std::string& f_name) {
      return json2table(loadJSON(f_name));
    }

    /**
     * Load a BallModel object from a file in JSON format.
     */
    shared_ptr<BallModel> loadBallModel(const Table& table, const std::string& f_name) {
      json ball_model_conf = json::parse(load_text_file(f_name));
      return load_ball_model(table, ball_model_conf);
    }

  };
};

