
#include "robotics/table_tennis/table.hpp"

using namespace std;
using namespace arma;

namespace robotics {

  namespace table_tennis {

    class Table::Impl {
      public:
        double width, length;
        vec center;
        double net_height;
        vec normal;

        Impl() = default;

        Impl(const Impl& b) = default;

        Impl(double width, double length, const vec& center, double net_height) : width(width), 
        length(length), center(center), net_height(net_height) {
          normal = vec{0,0,1};
        }

        ~Impl() = default;

        bool segm_intersect_table(const vec& from, const vec& to) const {
          if ((from.n_elem != to.n_elem) && (from.n_elem != center.n_elem)) 
            throw std::logic_error("The input points for the table must be 3 dimensional");
          vec u = to - from;
          vec w = from - center; //plane represented by a point(center) and a normal vector
          double den = -dot(normal, u);
          if (fabs(den)<1e-5) return false; //line and plane are parallel
          double num = dot(normal, w);
          double t = num / den;
          if (num < 0) return false; //intersection going the wrong way
          if (t<0 || t>1) return false; //intersect outside the segment
          vec int_pt = from + t*u; //the intersection point
          vec cent_pt = int_pt - center; //intersection point in coordinates respect to the center
          return (fabs(cent_pt[0]) < 0.5*width) && (fabs(cent_pt[1]) < 0.5*length);
        }
    };

    Table::Table() {
      _impl = nullptr;
    }

    Table::Table(double width, double len, const arma::vec& center, double net_height) {
      _impl = unique_ptr<Impl>( new Impl{width, len, center, net_height} );
    }
        
    Table::~Table() = default;

    Table::Table(const Table& b) {
      _impl = unique_ptr<Impl>(new Impl(*b._impl));
    }
        
    Table::Table(Table&& b) = default;

    Table& Table::operator=(const Table& b) {
      if (&b != this) {
        _impl = unique_ptr<Impl>(new Impl(*b._impl));
      }
      return *this;
    }

    Table& Table::operator=(Table&& b) = default;

    bool Table::segm_intersect_table(const vec& from, const vec& to) const {
      return _impl->segm_intersect_table(from, to);
    }

    /**
     * @brief Returns the coordinates of the center of the table
     */
    const arma::vec& Table::center() const {
      return _impl->center;
    }

  };

};
