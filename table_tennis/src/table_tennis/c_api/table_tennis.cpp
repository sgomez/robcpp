
#include "robotics/table_tennis/table_tennis.h"
#include "robotics/table_tennis/table.hpp"
#include "robotics/table_tennis/utils.hpp"
#include "robotics/table_tennis/ball.hpp"
#include <robotics.hpp>
#include <string>

using namespace std;
using namespace robotics;
using namespace robotics::table_tennis;

extern "C" {

  /* Code of the Table Object */

  /**
   * Construct a new table tennis Table object with parameters loaded from the given file. The 
   * constructed object must be manually deleted.
   */
  void* rob_tt_load_table(const char* fname) {
    const string& s = fname;
    Table* t = new Table(loadTable(s));
    return t;
  }

  /**
   * Destructor for the table tennis Table object.
   */
  void rob_tt_free_table(void *table) {
    Table* t = (Table*)table;
    delete t;
  }

  /* Code of the ball model object */

  /**
   * Construct a ball model with parameters loaded from the given file.
   */
  void* rob_tt_load_ball_model(const void *table, const char* fname) {
    const Table& t = *((Table*)table);
    const string& s = fname;
    BallModel* m = new BallModel( *loadBallModel(t, s) );
    return m;
  }

  /**
   * Destructor for the ball model object
   */
  void rob_tt_free_ball_model(void* ball_model) {
    BallModel* m = (BallModel*)ball_model;
    delete m;
  }

  /**
   * Creates a new ball trajectory
   */
  void* rob_tt_new_ball_traj(const void* ball_model) {
    const BallModel* m = (const BallModel*)ball_model;
    LSSGaussState* ans = new LSSGaussState( m->new_traj() );
    return (void*)ans;
  }

  /**
   * Advances the ball state according to the ball model dynamics a time deltaT 
   */
  void rob_tt_ball_advance_traj(void* ball_state, const void* ball_model, double deltaT) {
    const BallModel* m = (const BallModel*)ball_model;
    LSSGaussState* me = (LSSGaussState*)ball_state;
    *me = m->advance(*me, deltaT);
  }

  /**
   * Adds an observation (3 dimensional C vector) of the ball to the current ball state
   */
  void rob_tt_ball_observe(void* ball_state, const void* ball_model, const double* obs) {
    const BallModel* m = (const BallModel*)ball_model;
    LSSGaussState* me = (LSSGaussState*)ball_state;
    arma::vec v_obs{obs[0], obs[1], obs[2]};
    *me = m->observe(*me, v_obs);
  }

  /**
   * Computes the Mahalanobis distance between the current ball estimated position and the given
   * ball observation.
   */
  double rob_tt_ball_mah_dist(const void* ball_state, const void* ball_model, const double* obs) {
    const BallModel* m = (const BallModel*)ball_model;
    const LSSGaussState* me = (LSSGaussState*)ball_state;
    arma::vec v_obs{obs[0], obs[1], obs[2]};
    return m->obs_mah_dist(*me, v_obs);
  }

  /**
   * Returns the estimated ball position from the given ball state.
   */
  void rob_tt_ball_position(double* position, const void* ball_state, const void* ball_model) {
    const BallModel* m = (const BallModel*)ball_model;
    const LSSGaussState* me = (LSSGaussState*)ball_state;
    arma::vec tmp = m->mean_position(*me);
    for (int i=0; i<3; i++) position[i] = tmp[i];
  }

}; 
