
/**
 * @file
 * The implementation of the classes of the namespace ball_filters was done in a subfolder called
 * ball filters instead of this file. This goes against the standard practice but helps to keep
 * the logic of multiple ball filters separated in multiple source files.
 */

#include <robotics/table_tennis/ball_filter.hpp>

using namespace std;

namespace robotics {
  namespace table_tennis {
    using json = nlohmann::json;

    namespace ball_filters {
      
      FilterRansac3D::Params load_filter_ransac_params(const json& conf) {
        RansacConf ransac_conf{ conf.at("tol"), conf.at("group_size"), conf.at("num_iter") };
        FilterRansac3D::Params ans{ conf.at("min_support"), conf.at("max_buff_size"),
          conf.at("max_time_gap"), ransac_conf };
        return ans;
      }

      KalmanOutlier3D::Params load_kalman_outlier_3d_params(const json& conf) {
        auto ransac_params = load_filter_ransac_params(conf.at("ransac"));
        return KalmanOutlier3D::Params{ransac_params, conf.at("max_mah_dist")};
      }

      std::shared_ptr<KalmanOutlier3D> load_kalman_outlier_3d(std::shared_ptr<BallModel> ball_model, 
          const json& conf) {
        auto ans = make_shared<KalmanOutlier3D>(ball_model);
        ans->set_params( load_kalman_outlier_3d_params(conf) );
        return ans;
      }
    };

  };
};
