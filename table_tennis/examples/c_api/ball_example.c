
#include <stdio.h>
#include <robotics/table_tennis/table_tennis.h>

int main() {
  char opt[10];
  /* Allocate the resources and load configurations */
  void *table = rob_tt_load_table("models/table.txt");
  void *ball_model = rob_tt_load_ball_model(table, "models/trained_kf.txt");
  double deltaT = 0.016; /* The model in the file trained_kf.txt was trained with this deltaT */
  double obs[3], position[3];
  void *ball_state;
  double time = 0.0;

  printf("Now the models are loaded. Suppose we see a new ball trajectory (Initial time = %.2lf s)\n", time);
  ball_state = rob_tt_new_ball_traj(ball_model);
  while(1) {
    printf("Insert the 3D location of the ball observation at t = %.3lf s \
(Each coordinate separated by space)\n", time);
    scanf("%lf %lf %lf", &obs[0], &obs[1], &obs[2]);

    printf("The Mahalanobis distance of the given observation to the current estimate is %.4lf\n",
        rob_tt_ball_mah_dist(ball_state, ball_model, obs));
    printf("Do you accept this ball as correct (No means outlier)? (Y/N) ");
    scanf("%s", opt);
    /* Take anything different from No as yes. In case Yes add ball observation to the state */
    if (opt[0] != 'N' && opt[0]!='n') 
      rob_tt_ball_observe(ball_state, ball_model, obs);
    /* Now compute the ball estimated (filtered) position */
    rob_tt_ball_position(position, ball_state, ball_model);
    printf("The current ball filtered position (Position estimate) is (%.3lf, %.3lf, %.3lf)\n", position[0], 
        position[1], position[2]);

    printf("Do you want to exit the demo? (Y/N) ");
    scanf("%s", opt);
    /* Take anything different from yes as no */
    if (opt[0] == 'Y' || opt[0]=='y') 
      break;

    /* Advancing time by deltaT */
    rob_tt_ball_advance_traj(ball_state, ball_model, deltaT);
    time += deltaT;
  }
  /* Now, make sure you release all the resources you allocated */
  rob_tt_free_ball_model(ball_model);
  rob_tt_free_table(table);
}
