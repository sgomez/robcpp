
#include <json.hpp>
#include <fstream>
#include <string>
#include <unordered_map>
#include <robotics/table_tennis/ball_filter.hpp>
#include <robotics/table_tennis/utils.hpp>
#include <robotics/table_tennis/table.hpp>
#include <robotics/table_tennis/raw_ball_models.hpp>

using namespace robotics;
using namespace robotics::table_tennis;
using namespace robotics::table_tennis::ball_filters;
using namespace std;
using namespace arma;
using json = nlohmann::json;

void BOOST_CHECK_MESSAGE(bool ok, const string& msg) {
  if (!ok) {
    cout << "Assertion failed: " << msg << endl;
  }
}

int test() {
  ifstream ball_model_file("examples/models/ball_model.json");
  json ball_model_conf;
  ball_model_file >> ball_model_conf;
  auto tmp = load_ball_model(ball_model_conf);
  auto traj = tmp->sample_ball_traj(tmp->new_traj(), 100, 0.016);
  cout << traj.size()<< endl;
  auto obs = tmp->sample_ball_obs(traj);


  Table table = loadTable("examples/models/table.txt");
  ifstream f_in("test/data/ball_traj.json");
  json data;
  f_in >> data;
  mat bounce_gamma = json2mat(data["Gamma"]);
  //bounce_gamma(4,4) += 4e-3; //some extra error due to bounce
  //bounce_gamma(5,5) += 1e-2;

  LGBM1Params ball_model_params;
  ball_model_params.air_drag = json2vec(data["air_drag"]);
  ball_model_params.gravety = -9.8;
  ball_model_params.bounce_fac = json2vec(data["bounce_coef"]);
  ball_model_params.bounce_trans_noise = bounce_gamma;
  ball_model_params.fly_trans_noise = json2mat(data["Gamma"]);
  ball_model_params.fly_obs_noise = json2mat(data["Sigma"]);
  ball_model_params.bounce_obs_noise = json2mat(data["Sigma"]);

  LSSGaussState init_st{ json2vec(data["mu0"]), json2mat(data["P0"]) };
  auto dyn = make_shared<LGBM1>(ball_model_params, table);

  auto ball_model = make_shared<BallModel>( init_st, dyn );
  KalmanOutlier3D ball_filter {ball_model};

  RansacConf ransac_conf;
  ransac_conf.tol = 0.05; //5cm tolerance (otherwise outlier)
  ransac_conf.num_iter = 50; //Do 50 iterations of RANSAC
  ransac_conf.group_size = 6; //Pick groups of this many elements
  FilterRansac3D::Params ransac_pars;
  ransac_pars.max_buff_size = 12; //Keep these last ball observations only (at most)
  ransac_pars.min_support = 8; //If we have less than this number of inliers do not proceed
  ransac_pars.rans_conf = ransac_conf;
  KalmanOutlier3D::Params kalman_params; //5 mah distances is outlier
  kalman_params.max_mah_dist = 5.0; //More than this Mahalanobis dist means outlier
  kalman_params.ransac_params = ransac_pars;

  ball_filter.set_params(kalman_params);
  
  auto trajs = data["trajectories"];
  double deltaT = data["deltaT"];
  unsigned int n = 0;
  for (const auto& traj : trajs) {
    if (ball_filter.ready()) {
      cout << "Ransac ball filter should not be ready at the start. On trajectory " << 
        n << endl;
      return -1;
    }
    mat Xn = json2mat( traj.at("X") );
    mat Un = json2mat( traj.at("U") );
    mat Zn = json2mat( traj.at("Z") );
    //cout << Zn << endl;
    for (unsigned int t=0; t<Xn.n_rows; t++) {
      double obs[3] = {Xn(t,0), Xn(t,1), Xn(t,2)};
      ball_filter.add_obs(0, t*deltaT, obs);
      if (t>ransac_pars.max_buff_size) {
        if (!ball_filter.ready()) { 
          cout << "The ball filter should be ready now, but it is not. t=" << t << " n=" << n << endl;
          return -1;
        }
      }
      if (ball_filter.ready()) {
        LSSGaussState state = ball_filter.state();
        for (unsigned int tn=t; tn<Xn.n_rows; tn++) {
          vec diff = Zn.row(tn).t() - state.mean();
          mat C = dyn->obs_mat(state, &deltaT);
          vec mean_obs = C*state.mean();
          vec obs_truth = C*Zn.row(tn).t();
          vec obs_diff = mean_obs - obs_truth;
          mat obs_cov = dyn->obs_noise(state, &deltaT) + C*state.cov()*C.t();
          double mah_dist = sqrt(dot(diff, inv(state.cov())*diff));
          double obs_mah_dist = sqrt(dot(obs_diff, inv(obs_cov)*obs_diff));
          if (obs_mah_dist > 3.6) {
            //mah_dist>3.6 accounts for 99.5% of the probability mass
            cout << "The Mahalanobis distance between the ground truth " << "Zn=" << Zn.row(tn) << 
              " and the model prediction (mean=" << state.mean().t() << ", cov=" << state.cov() << 
              " is " << obs_mah_dist << ". On trajectory " << n << " t=" << t << " and tn=" << tn << endl;
            cout << "Dim std: " << sqrt(state.cov().diag()).t() << " diff=" << diff.t() << endl;
            return -1;
          } else if (mah_dist > 5.0) {
            //If the difference with velocity is too much, it is because the predicted bounce. There
            //is no point in continuing with the rest of the test case
            break;
          }
          if (t > 30) {
            if (arma::norm(obs_truth - mean_obs) > 0.05) {
              cout << "Error between ball trajectory " << obs_truth.t() << 
                " and ball prediction " << mean_obs.t() << " larger that threshold with n=" <<
                n << " t=" << t << " and tn=" << tn << endl;
              return -1;
            }
          }
          state = state.next(*dyn, Un.row(tn).t(), &deltaT);
        }
      }      
    }
    n++;
    ball_filter.reset();
  }
  return 0;
}

int main() {
  if (test() == 0) {
    cout << "Test passed!!!" << endl;
  }
}
