/**
 * @file A small server to process trials
 */

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <memory>

#include <json.hpp>
#include <armadillo>
#include <zmq.h>
#include <boost/program_options.hpp>

#include <robotics/table_tennis/time_series.hpp>
#include <robotics/table_tennis/segment.hpp>
#include <robotics/utils.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>

using namespace std;
using json = nlohmann::json;
using namespace arma;
namespace tt = robotics::table_tennis;
namespace logging = boost::log;

void log_init() {
  logging::add_file_log(
      logging::keywords::file_name = "/tmp/trial_clean_srv.log", 
      logging::keywords::auto_flush = true,
      logging::keywords::format = "%Message%"
      );
  logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::trace);
}


void run(const string& uri, unsigned int sock_buff_size, shared_ptr<tt::Segmenter> s) {
  unique_ptr<void, function<int(void*)>> context { zmq_ctx_new(), zmq_ctx_destroy };
  unique_ptr<void, function<int(void*)>> srv { zmq_socket(context.get(), ZMQ_REP), zmq_close };
  if ( zmq_bind(srv.get(), uri.c_str()) != 0 ) {
    BOOST_LOG_TRIVIAL(error) << "The ZMQ socket could not be opened, probably because"
      " the port is already busy by other process";
    throw std::logic_error("The ZMQ socket could not be opened.");
  }
  unique_ptr<char[]> buff{ new char[sock_buff_size] };

  string message;
  bool running = true;
  BOOST_LOG_TRIVIAL(info) << "Running trial cleanup server with uri " << uri;
  while (running) {
    BOOST_LOG_TRIVIAL(trace) << "Waiting for a TCP request";
    int num_bytes = zmq_recv(srv.get(), buff.get(), sock_buff_size, 0);
    buff[num_bytes] = '\0';
    message = buff.get();
    string debug_mgs;
    if (message.size() > 100) {
      debug_mgs = message.substr(0,100);
    } else {
      debug_mgs = message;
    }
    BOOST_LOG_TRIVIAL(trace) << "Received message: " << debug_mgs;
    json query = json::parse(message);
    json answer;
    auto method = query.find("method");
    auto body = query.find("body");
    if (method != query.end() && body != query.end()) {
      vector<string> parsed;
      string s_method = *method;
      json j_body = *body;
      if (s_method == "config") {
        BOOST_LOG_TRIVIAL(info) << "Updating config";
        s->update_config(j_body);
      } else if (s_method == "process_trial") {
        BOOST_LOG_TRIVIAL(info) << "Reciving trial. Saving to file";
        /*ofstream tmp("/tmp/trial.json");
        tmp << j_body;
        tmp.close();*/
        BOOST_LOG_TRIVIAL(info) << "Processing received trial";
        answer = s->segm_meta_data(j_body);
      } else if (s_method == "close") {
        running = false;
      } else {
        answer["error"] = "Unrecognized method";
      }
    } else {
      answer["error"] = "Malformed query. Both method and body properties are expected";
    }
    message = answer.dump();
    if (message.size() > 100) {
      debug_mgs = message.substr(0,100);
    } else {
      debug_mgs = message;
    }
    BOOST_LOG_TRIVIAL(trace) << "Answer: " << debug_mgs;
    zmq_send(srv.get(), message.c_str(), message.size(), 0);
  }
}

int main(int argc, char** argv) {
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help", "Produce help message")
      ("bs", po::value<unsigned int>(), "Buffer size for the socket server")
      ("type,t", po::value<string>(), "Type of segmenter (old | play)")
      ("uri,u", po::value<string>(), "URI of the zmq server socket");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }

    log_init();
    string uri = "tcp://*:7640";
    unsigned int sock_buff_size = 1 << 14;
    if (vm.count("uri")) uri = vm["uri"].as<string>();
    if (vm.count("bs")) sock_buff_size = vm["bs"].as<unsigned int>();

    shared_ptr<tt::Segmenter> s;
    string type = vm.count("type") ? vm["type"].as<string>() : "old";
    if (type == "old") s = shared_ptr<tt::Segmenter>( new tt::Segment() );
    else if (type == "play") s = shared_ptr<tt::Segmenter>( new tt::PlaySegmenter() );
    else {
      cerr << "Unrecognized segmenter type " << type;
      return 2;
    }

    run(uri, sock_buff_size, s);
    return 0;
  } catch (std::exception& ex) {
    BOOST_LOG_TRIVIAL(error) << "Exception: " << ex.what();
    cerr << "Error: " << ex.what() << endl;
    return 1;
  }
}
