/**
 * @file Script to segment data collected in JSON format
 */

#include <fstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <sstream>
#include <cassert>

#include <json.hpp>
#include <armadillo>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include <robotics/table_tennis/time_series.hpp>
#include <robotics/table_tennis/segment.hpp>
#include <robotics/utils.hpp>

#include <robotics/table_tennis/time_series.hpp>
#include <robotics/table_tennis/segment.hpp>
#include <robotics/utils.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>

using namespace std;
using json = nlohmann::json;
using namespace arma;
namespace tt = robotics::table_tennis;
namespace fs = boost::filesystem;
namespace logging = boost::log;

void log_init() {
  logging::add_file_log(
      logging::keywords::file_name = "/tmp/segmenter.log", 
      logging::keywords::auto_flush = true,
      logging::keywords::format = "%Message%"
      );
  logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::trace);
}

void write_segments(const json& segments, const json& options) {
  const string& folder = options.at("folder");
  const string& prefix = options.at("prefix");
  fs::path dir(folder);
  unsigned int file_counter = 0;
  for (auto segm : segments) {
    stringstream fname;
    fname << prefix << setw(3) << setfill('0') << ++file_counter << ".json";
    fs::path file(fname.str());
    fs::path full_path = dir / file;
    ofstream out(full_path.string());
    out << segm << endl;
    out.close();
  }
}

int main(int argc, char** argv) {
  try {
    shared_ptr<tt::Segmenter> s;
    //tt::Segment s;
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help", "Produce help message")
      ("input,i", po::value<string>(), "path to the input json file")
      ("segment,s", po::value<string>(), "path to the folder "
       "where the multiple segmented json files are stored")
      ("prefix", po::value<string>(), "prefix of the segmented files")
      ("output,o", po::value<string>(), "path to an "
       "output JSON file where the segmentation meta-data is stored")
      ("conf,c", po::value<string>(), "path to JSON segmentation "
       "configuration file")
      ("type,t", po::value<string>(), "Type of segmenter (old | play)");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }
    log_init();

    string type = vm.count("type") ? vm["type"].as<string>() : "old";
    if (type == "old") s = shared_ptr<tt::Segmenter>( new tt::Segment() );
    else if (type == "play") s = shared_ptr<tt::Segmenter>( new tt::PlaySegmenter() );
    else {
      cerr << "Unrecognized segmenter type " << type;
      return 2;
    }

    json in;
    if (vm.count("input")) {
      ifstream input(vm["input"].as<string>());
      input >> in;
    } else {
      cin >> in;
    }
    if (vm.count("conf")) {
      ifstream conf_file(vm["conf"].as<string>());
      json conf;
      conf_file >> conf;
      s->update_config(conf);
    }

    json out = s->segm_meta_data(in);
    if (vm.count("segment")) {
      json options {
        {"no_outliers", true},
        {"prefix", "s"}, 
        {"folder", vm["segment"].as<string>()}
      };
      if (vm.count("prefix")) options["prefix"] = vm["prefix"].as<string>();
      json segments = s->get_segments(in, out, options);
      write_segments(segments, options);
    }
    if (vm.count("output")) {
      ofstream output(vm["output"].as<string>());
      output << out << endl;
    } else {
      cout << out << endl;
    }
  } catch (std::exception& ex) {
    cerr << "Error: " << ex.what() << endl;
    return 1;
  }
}
